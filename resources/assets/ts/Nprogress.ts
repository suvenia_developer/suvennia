import NProgress from "nprogress";

class Nprogress {
    constructor() {
            NProgress.start();
            NProgress.inc(0.2);
            window.onload = function () { NProgress.done(); }
            jQuery( document ).ajaxStart(function() {
                NProgress.start();
              });
              
              jQuery( document ).ajaxStop(function() {
                NProgress.done();
              });
    }

}