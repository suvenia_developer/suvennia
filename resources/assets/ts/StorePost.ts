class StorePost{
    public constructor(private form: String){
        this.sendAjax();
    }

    private beforeSend(xhr: any, currentForm: JQuery): void{
        currentForm.find(':input').prop('disabled', true);
        currentForm.find(':input[type="submit"]').text('Please wait...');
            $("#StoreFrame").LoadingOverlay("show", {
            background  : "rgba(45, 45, 45, 0.3)",
            zIndex                  : 100 
            });
    }

    private sendAjax(){
        let form = $(this.form);
        const cls = this;
        $(document).on('submit', this.form, function(e){
            let currentForm: any = $(e.currentTarget);
            let btnText: any = currentForm.find(':input[type="submit"]').text()
            e.preventDefault();
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: currentForm.serializeArray(),
                beforeSend: function(xhr){
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
            .done(function(data, textStatus, jqXHR){
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    }

    private success(data: any, textStatus: any, jqXHR: any, curForm: JQuery, btnText: any){
        let cls = this;
        if(jqXHR.status == 200){
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
                if(data.message){
                toastr.success(data.message);
                }
               
                curForm.find(':input[type="submit"]').text(btnText);
                curForm.find(':input').prop('disabled', false);
                cls.reload_message_frame('StoreFrame');
                $("#StoreFrame").LoadingOverlay("hide");
                
        }
    }

    private errorHandler(jqXHR: any, textStatus: any, errorThrown: any, btnText: any, curForm: JQuery){
       // console.log(jqXHR.status);
        if(jqXHR.status == 500 || jqXHR.status == 422){
            var data = JSON.parse(jqXHR.responseText);
                if(typeof data.errors === 'string' || data.errors instanceof String){
                    toastr.error(data.errors, "Error");
                    curForm.find(':input').prop('disabled', false);
                curForm.find(':input[type="submit"]').text(btnText);
                    
                }else{
                    
                    curForm.find('.has-error').removeClass('has-error');
                    curForm.find('.help-block').text('');
                    $.each(data.errors, function(ind: any, val: any){
                        curForm.find('.'+ind).addClass('has-error');
                        curForm.find('.'+ind+' .help-block').text(val[0]);
                    });
                    curForm.find(':input').prop('disabled', false);
                    curForm.find(':input[type="submit"]').text(btnText);
                }
        }else if(jqXHR.statusText == "timeout"){
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            toastr.error('Request Timeout! Please try again.');
            curForm.find(':input').prop('disabled', false);
            curForm.find(':input[type="submit"]').text(btnText);
        }
    }


    public reload_message_frame(id: any) {
        var frame_id = id;
        if((<any>window.document.getElementById(frame_id)).location ) {  
            (<any>window.document.getElementById(frame_id)).location.reload(true);
        } else if ((<any>window.document.getElementById(frame_id)).contentWindow.location ) {
            (<any>window.document.getElementById(frame_id)).contentWindow.location.reload(true);
        } else if ((<any>window.document.getElementById(frame_id)).src){
            (<any>window.document.getElementById(frame_id)).src = (<any>window.document.getElementById(frame_id)).src;
        } else {
            // fail condition, respond as appropriate, or do nothing
            alert("Sorry, unable to reload that frame!");
        }
    }

}

$(function(){
    const __initPost = new StorePost("[store-post]");
});
