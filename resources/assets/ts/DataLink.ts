class DataLink {
    constructor() {
        if($('[data-link]').length){
            $(document).on('click', '[data-link]', (e)=>{
                let url: any = $(e.currentTarget).attr('data-link');
                e.preventDefault();
                window.location.href = url;
            })
        }
    }
}

$(function(){
    const __dataLink = new DataLink();
})