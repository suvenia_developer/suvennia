import * as _ from "lodash";
import NProgress from "nprogress";
import Dropzone = require("dropzone");
import Modernizr from "modernizr";

Dropzone.options.FileChatHolder = false;
//declare var Modernizr: any;

class Chat {

    data: any = {
		lastID 		: 0,
		noActivity	: 0
    }

    working: boolean = false;

    chatPane: JQuery;

    chats: any = []; 

    currentUser: any = [];
    
    constructor() {
        this.chatPane =  $('#AppChartPane');
    }

    public init() {
        this.setDownloadAttribute();
        this.getCurrentUser();
            this.LoadChats();
            this.addLineChat();
            this.addFileChat();
    }

    public populateChats(){
        let cls = this;
        this.chatAjax(this.chatPane.attr('url-chats'), {user_type:  $('#AppChartPane').attr("sender-type")}, (res: any)=>{
            return res.data;
        });
    } 

    /**
     * Loads all chats on page reload
     */

    LoadChats(){
        let cls = this;
        this.chatAjax(this.chatPane.attr('url-chats'), {user_type:  $('#AppChartPane').attr("sender-type")}, (res: any)=>{
            this.chats = res.data;
            if(this.chats.length){
                $.each(this.chats, (ind, val)=>{
                    let Template = cls.getTemplate(ind, val);
                    cls.chatPane.append(Template);
                });
                this.scrollBottom();
                (function getChatsTimeoutFunction(){
                    cls.updateChat(getChatsTimeoutFunction);
                })();

            }else{
                cls.chatPane.append('<li class="ChatPlaceholder" style="text-align: center; font-weight: 600;">There no messages for this order</li>');
            }
        });
    }

    public addLineChat(){
        let cls = this;
        $('#__AppchatSubmit').on('click', function(e){
            e.preventDefault();
            let elem = $(e.currentTarget);
            let params = {
                "avatar" : cls.currentUser.avatar,
                "message" : $('#__appChatInput').val(),
                "time" : "",
                "id" : "",
                "user_type": $('#AppChartPane').attr("sender-type"),
                "line_class": "replies",
                "chat_type" :'TEXT',
                "attachment_url": ""
              }
              let ind = cls.chats.length < 1 ? 0 : cls.chats.length + 1;
              let Template = cls.getTemplate(ind, params);
              if($('.ChatPlaceholder').length){
                  $('.ChatPlaceholder').remove();
              }
              cls.chatPane.append(Template);
              $('#__appChatInput').val(" ");
              cls.scrollBottom();
              cls.chatAjax(cls.chatPane.attr('url-new-chat'), params, (res: any)=>{
                    cls.chats.push(res.data);
                    if($('#LineChat'+ind).length){
                        $('#LineChat'+ind).remove();
                    }
                   // console.log('created:'+cls.chats);
                    let NewTemplate = cls.getTemplate(cls.chats.length, res.data);
                    cls.chatPane.append(NewTemplate);
                    cls.scrollBottom();
                    UIkit.modal('#FileChatModal').hide();
                   
                    //get chats count and append to the last one
              })
        });
    }

    public addFileChat(){
        let cls = this;
        let params = {
            "avatar" : this.currentUser.avatar,
            "message" : $('#__appChatInput').val(),
            "time" : "",
            "id" : "",
            "user_type": $('#AppChartPane').attr("sender-type"),
            "line_class": "replies",
            "chat_type" :'MEDIA',
            "attachment_url": "",
            _token: $('meta[name="csrf-token"]').attr('content')
          }

        let chatUpload = new Dropzone('div#FileChatUploader', {
            url: $('#FileChatUploader').attr('data-url'),
            uploadMultiple: false,
            acceptedFiles: 'image/*,application/pdf',
            maxFilesize: 1000,
            paramName: "file",
			method: 'post',
			maxFiles: 5, 
			addRemoveLinks: true,
			//autoProcessQueue: false,
			params: params,
			dictDefaultMessage: '<span class="uk-text-middle app-text-upload">Drag File Here or </span><span class="uk-link">Select File</span>',
        })
        chatUpload.on('success', function (file: any, res: any) {
                cls.chats.push(res.data);
               /* if($('#LineChat'+ind).length){
                $('#LineChat'+ind).remove();
                }*/
                // console.log('created:'+cls.chats);
                let NewTemplate = cls.getTemplate(cls.chats.length, res.data);
                cls.chatPane.append(NewTemplate);
                cls.scrollBottom();
		});
    }

    public updateChat(callback: any){
        let cls = this;
       
        //setTimeout(callback,1000);
        let last_chat = _.last(this.chats);
        let last_chat_key: any = _.findLastKey(this.chats);
        
        let params = {
            last_id: !last_chat_key ? "" : this.chats[last_chat_key]["id"],
            "user_type": $('#AppChartPane').attr("sender-type"),
        }
        this.chatAjax($(this.chatPane).attr('url-update-chat'), params, (res: any)=>{
           
            if(!_.isEmpty(res.data)){
              
               $.each(res.data, (ind, val)=>{
                cls.chats.push(val);
              //  cls.chats = _.uniqBy(cls.chats, "id");
               // console.log('updated:'+cls.chats);
                let newLastKey: any = cls.chats.length - 1;
                   let Template = cls.getTemplate(cls.chats.length, val);
                   let lastChatDiv = $('#LineChat'+newLastKey);
                  
                   //if(!lastChatDiv.length){
                        cls.chatPane.append(Template);
                        cls.scrollBottom();
                  // }
                   //lastChatDiv.after(Template);
                   
               });
               cls.data.noActivity = 0;
               
               this.scrollBottom();
            }else{
               // If no chats were received, increment
               // the noActivity counter.
               
               cls.data.noActivity++;
           }
           // Setting a timeout for the next request,
           // depending on the chat activity:
           
           var nextRequest = 1000;
           
           // 2 seconds
           if(cls.data.noActivity > 3){
               nextRequest = 2000;
           }
           
           if(cls.data.noActivity > 10){
               nextRequest = 5000;
           }
           
           // 15 seconds
           if(cls.data.noActivity > 20){
               nextRequest = 15000;
           }
          // console.log(cls.data.noActivity);
          setTimeout(callback,nextRequest);
       });

    }

    public getCurrentUser(){
        let params = this.chatPane.attr('current-user');
        this.currentUser = JSON.parse(<any>params);
    }

    public scrollBottom(){
        $("#chartFrame").animate({ scrollTop: $("#chartFrame")[0].scrollHeight - $("#chartFrame")[0].clientHeight}, "fast");
    }

    public getTemplate(index: any, data: any){
        let content = "";
        if(data.chat_type === "TEXT"){
            content = '<p class="text-msg">'+data.message+'</p>';
        }else if(data.chat_type === "MEDIA"){
            content = '<div class="uk-card uk-card-secondary p-0 chat-media">'+
            '<div class="uk-inline-clip uk-transition-toggle" tabindex="0">'+
            '<img src="'+data.attachment_url+'" alt="" width="150" uk-responsive>'+
            '<div class="uk-transition-slide-bottom uk-overlay-primary uk-position-cover uk-overlay">'+
            '<a href="'+data.attachment_url+'" class="uk-position-center" download><i class="icon icon-cloud-download icon-chat"></i></a>'+
            '</div>'+
            '</div>'+
            '</div>';
        }
        let template = [
            '<li class="'+data.line_class+'" id="LineChat'+index+'">',
            '<img class="text-img" src="'+data.avatar+'" alt="" />',
             content,
            '</li>'
        ];
        return template.join(" ");
    }

    public chatAjax(url: any, ChatPostdata: any, callback: any){
        $.ajax({
            url:url,
            type: "POST",
            data: ChatPostdata,
            beforeSend: function(xhr){
                NProgress.done();
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            timeout: 20000
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            toastr.error(jqXHR.responseText);
        })
        .done(function(data, textStatus, jqXHR){
             callback(data);
        });
    }

    public setDownloadAttribute(){
        if ( ! Modernizr.adownload ) {
            var $link = $('a');
         
            $link.each(function() {
                var $download: any = $(this).attr('download');
             
                if (typeof $download !== typeof undefined && $download !== false) {
              var $el = $('<div>').addClass('download-instruction').text('Right-click and select "Download Linked File"');
              $el.insertAfter($(this));
                }
         
            });
        }
    }

   
}


$(function(){
    if($('#chartFrame').length){
        let Appchatw = new Chat();
        Appchatw.init();
    }
    
});

