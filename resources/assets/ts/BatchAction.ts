class BatchAction{
 
    childrenCheckBox: any;

    public constructor(){
        this.triggerCheckBoxes();
        this.childrenCheckBox = '[data-children-checkbox]';
        this.submitSelected();
        this.confirm_delete();
    }

    public triggerCheckBoxes(){
        let cls = this;
        $('[data-master-checkbox]').on('change', function(e){
                if($(e.currentTarget).is(':checked')){
                    $(cls.childrenCheckBox).prop('checked', true);
                }else{
                    $(cls.childrenCheckBox).prop('checked', false);
                }
        });
    }

    public submitSelected(){
        let cls = this;
        $(document).on('click', '[data-checkbox-trigger]',  (e)=>{
            e.preventDefault();
            var data: any = [];
            $(cls.childrenCheckBox+':checked').each(function(){
                data.push($(this).val());
            });
            let form = cls.setForm($(e.currentTarget).attr('href'), data);

            $('body').prepend(
                '<div id="__delete_prompt_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center">'+
                '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                '<h3 class="uk-modal-title mb-2 border-bottom" style="color: #000; font-size: 16px; font-weight: 600;">DELETE ITEM(S)</h2>'+
                '<p class="m-1" style="color: #333; font-size: 14px;">Do you really want to delete this Item(s)?</p>'+
                form.join(" ")+
                '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>'+
                '<button class="btn btn-info prompt-delete-link">YES</button>'+
                '</div></div>'
            );

            UIkit.modal("#__delete_prompt_modal").show();

            /*var data: any = [];
            $(cls.childrenCheckBox+':checked').each(function(){
                data.push($(this).val());
            });
            let form = cls.setForm($(e.currentTarget).attr('href'), data);
            $(e.currentTarget).append(form.join(" "));
            $('#BatchDeleteForm').submit();*/
        });
    }

    public setForm(href: any, data: any){
        var csrf = $('meta[name="csrf-token"]').attr('content');
        var template = [
            '<form id="BatchDeleteForm" action="'+href+'" method="POST" style="display: none;">',
            '<input type="hidden" name="_token" value="'+csrf+'">',
            '<textarea name="deleteIds">'+JSON.stringify(data)+'</textarea>',
            '</form>'
        ];

        return template;
    }

    public confirm_delete(){
        $(document).on('click', '.prompt-delete-link', function(){
            $('#BatchDeleteForm').submit();
        });
    }
}

const __batchAction = new BatchAction();