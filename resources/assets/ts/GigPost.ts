declare const ClassicEditor: any;
declare const instance: any;

class GigPost{
    editor1: any;
    editor2: any;

    public constructor(private form: String){
        if($('#appEditor2').length || $('#appEditor2').length){
        ClassicEditor
        .create( document.querySelector( '#appEditor1' ) )
        .then((edi: any)=>{
            this.editor1 = edi;
        })
        .catch((error: any) => {
            console.error( error );
        } );

        ClassicEditor
        .create( document.querySelector( '#appEditor2' ) )
        .then((edi: any)=>{
            this.editor2 = edi;
        })
        .catch((error: any) => {
            console.error( error );
        } );
        }
        this.sendAjax();
    }

    private beforeSend(xhr: any, currentForm: JQuery): void{
       
        const Spinner : any = $('body').attr("data-sinner");
        currentForm.find(':input').prop('disabled', true);
        //currentForm.find(':input[type="submit"]').html('<img src="'+Spinner+'" width="20" uk-responsive>');
    }

    private sendAjax(){
        let form = $(this.form);
        const cls = this;
        $(document).on('submit', this.form, function(e){
            let currentForm: any = $(e.currentTarget);
            let btnText: any = currentForm.find(':input[type="submit"]').text();
            e.preventDefault();
            let dataL = currentForm.serializeArray();
           
            dataL.push({
                name: 'gig_description',
                value: cls.editor1.getData()
            });
            
           
             dataL.push({
                    name: 'gig_requirement',
                    value: cls.editor2.getData()
            });
             
            $.ajax({
                url: currentForm.attr('action'),
                type: currentForm.attr('method'),
                data: dataL,
                beforeSend: function(xhr){
                    cls.beforeSend(xhr, currentForm);
                },
                timeout: 20000
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                cls.errorHandler(jqXHR, textStatus, errorThrown, btnText, currentForm);
            })
            .done(function(data, textStatus, jqXHR){
                cls.success(data, textStatus, jqXHR, currentForm, btnText);
            });
        });
    }

    private success(data: any, textStatus: any, jqXHR: any, curForm: JQuery, btnText: any){
        if(jqXHR.status == 200){
            curForm.find('.has-error').removeClass('has-error');
            curForm.find('.help-block').text('');
            curForm.find(':input[type="submit"]').text(btnText);
                if(data.message){
                toastr.success(data.message);
                }
                curForm.find(':input[type="submit"]').text(btnText);
                if(!data.isPaused){
                    var wait = setTimeout(function() {
                        window.location.href = data.redirect_url;
                    }, 1000);
                }else{
                    window.location.href = data.redirect_url;
                }
                
        }
    }

    private errorHandler(jqXHR: any, textStatus: any, errorThrown: any, btnText: any, curForm: JQuery){
       // console.log(jqXHR.status);
        if(jqXHR.status == 500 || jqXHR.status == 422){
            var data = JSON.parse(jqXHR.responseText);
            curForm.find(':input').prop('disabled', false);
            toastr.error(data.error, "Error");
        }
    }

}

$(function(){
    const __initGigPost = new GigPost("[gig-post]");
});
