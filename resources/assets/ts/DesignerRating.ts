class DesignerRating{
    public constructor(){
        this.init();
    }

    public init(){
        if($('#rate-designer-bar').length){
            $('#rate-designer-bar').barrating({
                theme: 'css-stars',
                allowEmpty: true,
                initialRating:null,
                showSelectedRating: true,
                deselectable: true,
                emptyValue: 0,
                onSelect: (value: any, text: any, event: any)=>{
                   let url =  $('#rate-designer-bar').attr('data-url');
                   $.ajax({
                    url: url,
                    type: 'POST',
                    data: {rate: value},
                    beforeSend: function(xhr){
                        
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        Accept: "application/json; charset=utf-8",  
                    },
                    timeout: 20000
                    })
                    .fail((jqXHR: any, textStatus: any, errorThrown: any)=>{
                        toastr.error(textStatus)
                    })
                    .done((data: any)=>{
                        $('#rateMsg').text('Your rating was saved!');
                    });
                }
        });
      }
    }
}  

$(function(){
    const ___DesignerRating = new DesignerRating();
})