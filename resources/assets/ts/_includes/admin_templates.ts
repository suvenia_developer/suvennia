import fabric from 'fabric';
import "spectrum-colorpicker";
import 'fabric-customise-controls';


class Templates{

    canvas: any;

    options: any = {};

    isDirty: boolean = false;

    addedGrid: any = [];

    public constructor(){
        this.canvas = new fabric.fabric.Canvas('template_canvas');

        this.options = {
            distance: 10,
            width: this.canvas.width,
            height: this.canvas.height,
            param: {
              stroke: '#ebebeb',
              strokeWidth: 1,
              selectable: false
            }
         };

         this.setToolBarItem();
         this.setGrid();
         this.set_controls();
         this.changeFont();
         this.setToolBarItem();
         this.setToolBar();
         this.addText();
         this.saveTemplate();

         if($('#EditPage').length){
         this.setEditData();
         }

    }

    setEditData(){
      const canvas_data = JSON.parse(<any>$('#templateData').val());
      this.canvas.loadFromJSON(canvas_data,this.canvas.renderAll.bind(this.canvas));
      this.canvas.renderAll();
    }

    setGrid(){
        let cls = this;
        var gridLen = cls.options.width / cls.options.distance;
        for (var i = 0; i < gridLen; i++) {
        var distance   = i * cls.options.distance,
        horizontal = new fabric.fabric.Line([ distance, 0, distance, cls.options.width], cls.options.param),
        vertical   = new fabric.fabric.Line([ 0, distance, cls.options.width, distance], cls.options.param);
        cls.canvas.add(horizontal);
        cls.canvas.add(vertical);
        cls.addedGrid.push(horizontal);
        cls.addedGrid.push(vertical);
        if(i%5 === 0){
        horizontal.set({stroke: '#cccccc'});
        vertical.set({stroke: '#cccccc'});
        };
        };
        cls.canvas.renderAll();
    }

    set_controls(){
        let cls = this;

        var BaseUrl = $('#InfoDiv').attr('asset-base');
        (<any>fabric).fabric.Canvas.prototype.customiseControls({
          tr: {
              action: 'rotate',
              cursor: 'default'
          },
          tl: {
              action: 'moveUp',
              cursor: 'default'
          },
          bl: {
              action: 'remove',
              cursor: 'default'
          },
          br: {
              action: 'scale',
              cursor: 'nwse-resize'
          }
      }, function () {
          cls.canvas.renderAll();
      });
      (<any>fabric).fabric.Object.prototype.customiseCornerIcons({
          settings: {
              borderColor: '#4B4B4B',
              cornerSize: 25,
              cornerShape: 'circle',
              cornerBackgroundColor: '#AFAEAE'
          },
          tl: {
              icon: BaseUrl + 'customize-icons/move-up.svg'
          },
          tr: {
              icon: BaseUrl + 'customize-icons/rotate-left.svg'
          },
          bl: {
              icon: BaseUrl + 'customize-icons/delete.svg'
          },
          br: {
              icon: BaseUrl + 'customize-icons/resize-d.svg'
          }
      }, function () {
          cls.canvas.renderAll();
      });
      (<any>fabric).Object.prototype.setControlsVisibility({
          mt: false,
          mb: false,
          mr: false,
          ml: false,
          mtr: false
      });
      
      }
    
      
    changeFont() {
    var fontSelected = <any>$('#FontSelect').val();
    var cls = this;
    $('#FontSelect').css('font-family', fontSelected);
    $('#FontSelect').on('change', function () {
        var cl = <any>$(this).val();
        $(this).css('font-family', cl);
        var activeObject = cls.canvas.getActiveObject();
        if (activeObject && activeObject.type === 'i-text') {
            activeObject.fontFamily = (activeObject.fontFamily == cl ? '' : cl);
            activeObject.dirty = true;
            cls.canvas.renderAll();
            cls.isDirty = true;
        }
    });
    };
      
      
    addText() {
        var cls = this;
        $('#AddText').on('click', function () {
            var Text = new fabric.fabric.IText(<any>$('#TextField').val(), {
                fontFamily: <any>$('#FontSelect').val(),
                fill: '#333',
                fontSize: 40
            });
            cls.isDirty = true;
            cls.canvas.centerObject(Text);
            cls.canvas.add(Text);
            cls.canvas.setActiveObject(Text);
            cls.canvas.renderAll();
        });
      };
      
    setColorToggle() {
        (<any>$(".toggleColorPallete")).spectrum({
            showPaletteOnly: true,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            color: 'blanchedalmond',
            palette: [
                ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
                ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
                ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
                ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
                ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
            ]
        });
    };
      
      
      
    setToolBar(){
        var tools = {};
        var cls = this;
        cls.canvas.on('object:selected', function (evt: any) {
            var activeObject = evt.target.get('type');
            $('.ToolBar').removeClass('hide');
            if (activeObject == 'image' || activeObject == 'path-group') {
                $('#OtherColorCont').hide();
                $('#outlineColorCont').hide();
            }
            else {
                $('#OtherColorCont').show();
                $('#outlineColorCont').show();
            }
        });
        cls.canvas.on('selection:cleared', function (evt: any) {
            $('.ToolBar').addClass('hide');
        });
        cls.canvas.on('object:removed', function (evt: any) {
            var activeObject = evt.target;
            $('.ToolBar').addClass('hide');
        });
      };
      
    setToolBarItem() {
        var cls = this;
        $('#DuplicateApp').on('click', function (e: any) {
            e.preventDefault();
            var activeObject = cls.canvas.getActiveObject();
            activeObject.clone(function (cloned: any) {
                cls.canvas.discardActiveObject();
                cloned.set({
                    top: cloned.top + 20,
                    evented: true
                });
                if (cloned.type === 'activeSelection') {
                    cloned.canvas = cls.canvas;
                    cloned.forEachObject(function (obj: any) {
                        cls.canvas.add(obj);
                    });
                    cloned.setCoords();
                }
                else {
                    cls.canvas.add(cloned);
                }
                cls.canvas.setActiveObject(cloned);
                cls.canvas.renderAll();
            });
        });
        $('#FlipVertical').on('click', function (e) {
            e.preventDefault();
            var activeObject = cls.canvas.getActiveObject();
            if (activeObject) {
                if (!activeObject.flipY) {
                    activeObject.set('flipY', true);
                }
                else {
                    activeObject.set('flipY', false);
                }
                cls.canvas.centerObject(activeObject);
                cls.canvas.renderAll();
            }
        });
        $('#FlipHorizintal').on('click', function (e) {
            e.preventDefault();
            var activeObject = cls.canvas.getActiveObject();
            if (activeObject) {
                if (!activeObject.flipX) {
                    activeObject.set('flipX', true);
                }
                else {
                    activeObject.set('flipX', false);
                }
                cls.canvas.centerObject(activeObject);
                cls.canvas.renderAll();
            }
        });
        $('#AppWideColor').on('change', function () {
            var activeObject = cls.canvas.getActiveObject();
            if (activeObject && activeObject.type == 'i-text') {
                activeObject.fill = $(this).val();
                activeObject.dirty = true;
                cls.canvas.renderAll();
            }
            else if (activeObject && activeObject.type == 'path-group') {
                activeObject.set('fill', $(this).val());
                activeObject.dirty = true;
                cls.canvas.renderAll();
            }
        });
        $('#AppWideOutlineColor').on('change', function (e) {
            var activeObject = cls.canvas.getActiveObject();
            if (activeObject && activeObject.type === 'i-text') {
                activeObject.stroke = $(this).val();
                activeObject.dirty = true;
                cls.canvas.renderAll();
            }
        });
    }

    saveTemplate(){
        let cls = this;
        $('#SaveTemplate').on('click', function(){
  
            $.each(cls.addedGrid, function(ind, val){
              cls.canvas.remove(val);
            });
          
            //console.log(canvas.toDataURL());
              //window.open(canvas.toDataURL());
              //PostTemplate
              $('#templateData').val(JSON.stringify(cls.canvas.toJSON()));
              //$('#templatePhoto').val(canvas.toSVG());
              $('#templatePhoto').val(cls.canvas.toDataURL());
              $('#PostTemplate').submit();
              $.each(cls.addedGrid, function(ind, val){
                cls.canvas.add(val);
              });
          });
    }
    

}

let __Template__ = new Templates();