//import "spectrum-colorpicker";
//import "jquery-cropbox";
import "./StorePost";

class StoreEdit {

    $uploadCrop: any;

    constructor() {
        this.setAjax();
        this.setColor();
        this.setSingleCheckBox();
        this.setSwitch();
        this.sendUpload();
        this.$uploadCrop = (<any>$('#upload-demo')).croppie({
			viewport: {
				width: 200,
				height: 100,
                type: 'square'
			},
            enableExif: true,
            enforceBoundary: false
		});
    }

    public setColor(){
        $('.colorpickers').each(function(){
            var el = this;
            var inp : any = $(el).attr('data-target');
            (<any>$(el)).spectrum({
            color: $(inp).val(),
            change: function(color: any) {
                $(inp).val(color.toHexString());
            }
             });

        });

    }

    public setSwitch(){
        var radio = $('[app-switcher]').find('input[type="radio"]');
        var radio2 = $('[app-switcher]').find('input[type="radio"]:checked');
        $(radio2).each(function(){
            var def_cont = $(this).attr('show-content');
             var def_hide_cont : any = $(this).attr('hide-content');
            $(def_hide_cont).addClass('app-hide');
            //$(def_cont).show();
        });
        
        $(document).on('change', radio, (e)=>{
            var cont : any = $(e.target).attr('show-content');
            var hide_cont : any = $(e.target).attr('hide-content');
            if($(e.target).is(':checked')){
                $(hide_cont).addClass('app-hide');
            $(cont).removeClass('app-hide');
            }
           
        });
    }

    setAjax(){
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    setSingleCheckBox(){
        $(document).on("click", '.single_select_checkbox', function(){
        let thisRadio : any  = $(this);
        
        if (thisRadio.hasClass("imChecked")) {
            $('.single_select_checkbox').removeClass('imChecked');
        thisRadio.removeClass("imChecked");
        thisRadio.prop('checked', false);
        } else { 
        thisRadio.prop('checked', true);
        thisRadio.addClass("imChecked");
        };
        })
    }

  /* public cropImage(div: any, input: any){
        let IMG : any;
        var crop = (<any>$(div)).cropbox({
        width: 200,
        height: 60,
        }, ()=>{
        //console.log('Url: ' + this.getDataURL());
       
         $('#CropImage').removeClass('app-hide');
           
            IMG = this;

        }).on('cropbox', function(e: any, data: any) {
        ///console.log('crop window: ' + JSON.stringify(data));
        //$(input).val(this.getDataURL());
        //console.log(e);

        });
        $('#CropImage').on('click', function(){
            IMG.update();
           $(input).val(IMG.getDataURL());
           //console.log(IMG);
        });

        
    }*/

    public cropImage(div: any, input: any, cropBox: any, cropBtn: any){
        let cls = this;
        $(cropBtn).on('click', function(e){
            cls.$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp: any) {
                $(input).val(resp);
                $(div).attr('src', resp);
                $(cropBox).addClass('app-hide');
                $(div).removeClass('app-hide');
                $(e.currentTarget).addClass('app-hide');
			});
        });
    }

    public sendUpload(){
        var cls = this;

        $('.js-upload').each(function(ind: any, val: any){
            
            var bar : any = document.getElementById(<any>$(val).attr('progress'));
            (<any>UIkit).upload(val, {

                url: (<any>$('.js-upload')).attr('data-url'),
                //mime: true,
                //allow:"*.png,*.jpg",
                loadStart: function (e : any) {
                   // console.log('loadStart', arguments);

                    bar.removeAttribute('hidden');
                    bar.max = e.total;
                    bar.value = e.loaded;
                },
                beforeSend: function (e: any) {
                    e.headers = {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    };
        
                },
                progress: function (e: any) {
                   // console.log('progress', arguments);

                    bar.max = e.total;
                    bar.value = e.loaded;
                },

                loadEnd: function (e : any) {
                   // console.log('loadEnd', arguments);

                    bar.max = e.total;
                    bar.value = e.loaded;
                },

                completeAll: function () {
                    var ImgUrl = JSON.parse(arguments[0].response);
                    var previewFrame: any = $(val).attr('preview');
                    var ImageInput: any = $(val).attr('input');
                    var cropBox : any = $(val).attr('cropBox');
                    var cropBtn : any = $(val).attr('cropBtn');
                    $(previewFrame).attr('src', ImgUrl.img);
                    $(ImageInput).val(ImgUrl.img);
                    /*if($(val).attr('cropable') !== undefined){
                        cls.cropImage(previewFrame, ImageInput);
                    }*/
                    //console.log(ImgUrl);
                    $(cropBox).removeClass('app-hide');
                    $(previewFrame).addClass('app-hide');
                         $(cropBtn).removeClass('app-hide');
                         $(cropBox).addClass('ready');
                    
                    cls.$uploadCrop.croppie('bind', {
	            		url: ImgUrl.img
	            	}).then(function(){
                         
                    });

                    cls.cropImage(previewFrame, ImageInput, cropBox, cropBtn);
                    setTimeout(function () {
                        bar.setAttribute('hidden', 'hidden');
                    }, 1000);
                }

            });
        });
        

    }

}
$(function(){
    const __setStoreObj = new StoreEdit();
});
