class Text{
    public constructor(public Canva: any){
        this.addText();
        this.addTextOutline(); 
    }

    public addText() {
		const cls = this;
		$('#AddText').on('click', (e) => {
			let Text = new cls.Canva.fab.fabric.IText(<any>'Add Text', {
				fontFamily: < any > $('#FontSelect').val(),
				fill: '#333',
				fontSize: 40,
            });
			cls.Canva.isDirty = true;
			cls.Canva.activeCanva.centerObject(Text);
			cls.Canva.activeCanva.add(Text);
			cls.Canva.activeCanva.setActiveObject(Text);
			cls.Canva.activeCanva.renderAll();
			let thisObject = cls.Canva.activeCanva.getObjects().indexOf(Text);
			cls.Canva.setObjectLayers({ index: thisObject, icon: 'add-text', text: 'Text' });
		});
	}

	public addTextOutline(){
		    const cls = this;
			$('#TextOutline').on('change', function(e){
				let inp = $(e.currentTarget);
				var activeObject = cls.Canva.activeCanva.getActiveObject();
				if (activeObject && activeObject.type === 'i-text') {
					activeObject.stroke = < any > $('#AppWideOutlineColor').val();
					activeObject.strokeWidth = parseInt(<any>inp.val());
					cls.Canva.activeCanva.renderAll();
				}
			});
	}
    
}

export default Text;