import 'fabric-customise-controls';

class Controls{
    public constructor(public Canva: any){
        this.setControls();
    }

    public setControls() {
		let cls = this;
		let BaseUrl = $('body').attr('BaseAppUrl');
		//(<any>$('#AppWideOutlineColor') )
		(<any>cls.Canva.fab).fabric.Canvas.prototype.customiseControls({
			tr: {
				action: 'rotate',
				cursor: 'default'
			},
			tl: {
				action: 'moveUp',
				cursor: 'default'
			},
			bl: {
				action: 'remove',
				cursor: 'default'
			},
			br: {
				action: 'scale',
				cursor: 'nwse-resize'
			},

		}, function () {
			cls.Canva.activeCanva.renderAll();
		});

		// basic settings
		(<any>cls.Canva.fab).fabric.Object.prototype.customiseCornerIcons({
			settings: {
				borderColor: '#4B4B4B',
				cornerSize: 20,
				cornerShape: '',
				cornerBackgroundColor: '#AFAEAE',
				cornerPadding: 10
			},
			tl: {
				icon: BaseUrl + '/customize-icons/move-up.svg',
			},
			tr: {
				icon: BaseUrl + '/customize-icons/rotate-left.svg',
			},

			/*bl: {
				icon: BaseUrl + '/customize-icons/delete.svg',
			},*/
			br: {
				icon: BaseUrl + '/customize-icons/resize-d.svg',
			},
			/* mb:{
			     icon: BaseUrl + '/src/customizer/customize-icons/move-down.svg',
			 }*/
		}, function () {
			cls.Canva.activeCanva.renderAll();
		});

		cls.Canva.fab.fabric.Object.prototype.setControlsVisibility({
			mt: false,
			mb: false,
			mr: false,
			ml: false,
			mtr: false,
			bl: false
		});
	}


}

export default Controls;