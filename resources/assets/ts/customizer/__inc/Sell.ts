import * as _ from "lodash";
import "bootstrap-slider";
import "bootstrap-tagsinput";


class Sell{

    public constructor(public Canva: any, public $route: any){}


    public setPriceSlider() {
		let cls = this;

		let PriceSlider = $('#PriceSlider').slider({
			formatter: function (value: any) {
				return value + '%';
			}
		}).on('slide change', function () {
			//console.log(PriceSlider.getValue());
			setInputs(PriceSlider.getValue());

		}).data('slider'); 

		let setEarning = function () {
			$('#data_earning').on('keydown keyup', function () {
				var base_price = parseInt(cls.Canva.activeData.price);
				var percentage = <any>$(this).val() / base_price * 100;

				if (percentage < 200) {
					$('#PriceSlider').slider('setValue', percentage);
					setInputs(percentage, true);
				} else {
					toastr.remove()
					toastr.warning('Maximum percentage exceeded!');
				}

			});
		}
		setEarning();

		let setQuantity = function () {
			$('#data_quantity').on('keydown keyup', function () {
				setInputs(PriceSlider.getValue());
			});
		}
		setQuantity();

		let setInputs = function (percentage: any, noEarning: boolean = false) {
			var qty = parseInt(<any>$('#data_quantity').val());
			var base_price = parseInt(cls.Canva.activeData.price);
			var earning = percentage / 100 * base_price;
			var totalEarning = earning * qty;
			var totalPrice = base_price + percentage / 100 * base_price;
			var user_price = totalPrice - base_price;
			$('#data_base_price').val(base_price);
			$('#display_base_price').text(base_price);
			$('#data_price').val(totalPrice);
			$('#data_user_price').val(user_price);
			$('#display_price').text(totalPrice);
			if (!noEarning) {
				$('#data_earning').val(earning.toFixed(2));
			}
			$('#data_total_earning').val(totalEarning.toFixed(2));
			$('#display_total_earning').text(totalEarning.toFixed(2));
			$('#data_percentage').val(PriceSlider.getValue());
		}

		}

	public setTags() {
		$("#TagsInput").tagsinput({
			tagClass: 'badge badge-info'
		});
	}

	public setDesign() {
		let cls = this;
		let data = cls.Canva.activeData;

		$('.frontimageHolder').find('.ProductFacing').attr('src', cls.Canva.designData.front.img_url);
		$('.frontimageHolder').find('.drawingArea').css({
			'position': 'absolute',
			'width': data.front.width + 'px',
			'height': data.front.height + 'px',
			'left': data.front.left + 'px',
			'top': data.front.top + 'px',
		});
		cls.Canva.designData.front.left = data.front.left;
		cls.Canva.designData.front.top = data.front.top;
		cls.Canva.designData.front.height = cls.Canva.canvas_front.height;
		cls.Canva.designData.front.width = cls.Canva.canvas_front.width;
		cls.Canva.designData.front.design = cls.Canva.canvas_front.toDataURL();
		$('.frontimageHolder').find('.drawingArea').html('<img src="' + cls.Canva.canvas_front.toDataURL() + '">');

		if (cls.Canva.hasBack == true) {
			$('.backimageHolder').find('.ProductFacing').attr('src', cls.Canva.designData.back.img_url);
			$('.backimageHolder').find('.drawingArea').css({
				'position': 'absolute',
				'width': data.back.width + 'px',
				'height': data.back.height + 'px',
				'left': data.back.left + 'px',
				'top': data.back.top + 'px',
			});
			cls.Canva.designData.back.left = data.back.left;
			cls.Canva.designData.back.top = data.back.top;
			cls.Canva.designData.back.height = cls.Canva.canvas_back.height;
			cls.Canva.designData.back.width = cls.Canva.canvas_back.width;
			cls.Canva.designData.back.design = cls.Canva.canvas_back.toDataURL();
			$('.backimageHolder').find('.drawingArea').html('<img src="' + cls.Canva.canvas_back.toDataURL() + '">');
		} else {
			cls.Canva.designData.back.design = '';
			$('.backimageHolder').find('.drawingArea').html('');
			$('.backimageHolder').find('.ProductFacing').attr('src', '');
		}
		cls.Canva.backgroundSetter('.frontimageHolder', '.backimageHolder',  _.last(cls.Canva.PrimaryColor));
	}

	public postDesign() {
		let cls = this;
		if (!cls.Canva.isDirty) {
			toastr.error('You have not made any changes');
			//	window.location.href = '#/create';
			cls.$route.navigate('home');
		} else {

			cls.setDesign();
			$('#data_base_price').val(cls.Canva.activeData.price);
			$('#display_base_price').text(cls.Canva.activeData.price);
			$('#display_price').text(cls.Canva.activeData.price);

			$('#CategorySelect option[value=' + cls.Canva.activeData.category_id + ']').prop("selected", true);
			$('#CategorySelect').val(cls.Canva.activeData.category_id);
			$('#CategorySelect').prop('disabled', true);
			/*if(!cls.hasBack){
				$('#SizeInput').prop('disabled', true);
			}else{
				$('#SizeInput').prop('disabled', false);
			}*/

			cls.postForm(cls);
		}
    }
    
  public postForm(cls: any): any {
		$(document).on('submit', '[data-post-customizer]', function (e) {
			e.preventDefault();
			let productId: any = $('#ProductSelect option:selected').attr('data-id');
			var form = $(this);
			var btnText = form.find(':input[type="submit"]').text();
			//console.log(cls.designData);
			//if (e.isDefaultPrevented()) {
			var data = form.serializeArray();
			data.push({
				name: 'design_id',
				value: productId
			});
			data.push({ 
				name: 'design_data',
				value: JSON.stringify(cls.Canva.designData)
			});
			data.push({
				name: 'colors',
				value: JSON.stringify(cls.Canva.PrimaryColor)
			});
			data.push({
				name: 'has_back',
				value: cls.Canva.hasBack
			});
			data.push({ 
				name: 'sub_category',
				value: <any>$('#TagsInput').val()
			});
			data.push({
				name: 'category',
				value: cls.Canva.activeData.category
			});
			data.push({
				name: 'photos',
				value: JSON.stringify(cls.Canva.imageBucket)
			});
			data.push({
				name: 'brand_description',
				value: cls.Canva.activeData.description
			});
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: data,
				beforeSend: function (xhr) {
					//console.log(data);
					form.find(':input').prop('disabled', true);
					form.find(':input[type="submit"]').text('Working...');
				},
				timeout: 20000,
			})
				.done(function (data, textStatus, jqXHR) {
					if (jqXHR.status == 200) {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						form.find(':input[type="submit"]').text(btnText);
						if (data.message) {
							toastr.success(data.message);
						}
						var wait = setTimeout(function () {
							window.location.href = data.redirect_url;
							//window.location.replace(data.redirect_url);
						}, 1000);
						form.find(':input[type="submit"]').text(btnText);

					}
				})
				.fail(function (jqXHR, textStatus, errorThrown) {

					if (jqXHR.status == 500) {
						var data = JSON.parse(jqXHR.responseText);
						if (typeof data.error === 'string' || data.error instanceof String) {
							toastr.error(data.error, "Error");
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);

						} else {

							form.find('.has-error').removeClass('has-error');
							form.find('.help-block').text('');
							$.each(data.error, function (ind: any, val: any) {
								form.find('.' + ind).addClass('has-error');
								form.find('.' + ind + ' .help-block').text(val[0]);
							});
							form.find(':input').prop('disabled', false);
							form.find(':input[type="submit"]').text(btnText);
						}

					} else if (jqXHR.statusText == "timeout") {
						form.find('.has-error').removeClass('has-error');
						form.find('.help-block').text('');
						toastr.error('Request Timeout! Please try again.');
						form.find(':input').prop('disabled', false);
						form.find(':input[type="submit"]').text(btnText);
					}
				});


		});
	}


}

export default Sell;
