class Dialog{
    public constructor(){
        this.init();
    }

    public init(){
        $(document).on('click', '.data-prompt', function(e){
            e.preventDefault();
            let title = $(e.currentTarget).attr('title');
            let content = $(e.currentTarget).attr('content');
            let confirm = $(e.currentTarget).attr('confirm') ? $(e.currentTarget).attr('confirm') : "YES";
            let cancel = $(e.currentTarget).attr('cancel') ? $(e.currentTarget).attr('cancel') : "NO";
            $('body').prepend(
            '<div id="__dialog_item_modal" uk-modal="bg-close:false;"><div class="uk-modal-dialog uk-modal-body p-0">'+
            '<button class="uk-modal-close-default" type="button" uk-close></button>'+
            '<h3 class="uk-modal-title mb-2 border-bottom p-3" style="color: #000; font-size: 16px; font-weight: 600;">'+title+'</h2>'+
            '<div class="p-3">'+content+'</div>'+
            '<div class="p-3 uk-text-right bg-light">'+
            '<button class="btn btn-info btn-sm mr-2 uk-modal-close" href="">'+cancel+'</button>'+
            '<a class="btn btn-info btn-sm post-link" href="'+$(this).attr('data-url')+'">'+confirm+'</a>'+
            '</div>'+
            '</div></div>'
            );
            UIkit.modal("#__dialog_item_modal").show();
        });


    }
}

$(function(){
    let __dialog = new Dialog();
});