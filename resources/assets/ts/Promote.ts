class Promote {
    followers_data: any = {};
    is_shipable: boolean = true;

    constructor() {
        this.setSteps();
        this.sortShippable();
        this.sortInputs();
        this.setShippingLocation();
        this.sortPromoteFormat();
        this.setInfluencerCount();
        this.getInfo();
        $('#PromotetotalPrice').text(0);
    }

    public setData(){
        if($('#FollowSelect').length && _.isEmpty(this.followers_data)){
            this.followers_data = JSON.parse(<any>$('#FollowSelect option:selected').attr('data'));
        }
    }

    public setSteps(){
        $(document).on('click', '[wgo]', (e)=>{
            e.preventDefault();
            let target : any = $(e.currentTarget).attr('wgo');
            let parent: any = $(e.currentTarget).attr('wparent');
            $(target).removeClass('wizardry-hide');
            $(parent).addClass('wizardry-hide');
        });
    }
  
    public sortShippable(){
        $(document).on('change', '#isPromoteShippable', (e)=>{
            let value = $(e.currentTarget).val();
            if(value == 0){
                $('#PromoteContentLink').removeClass('wizardry-hide');
                $('#promoteShippingLocationDiv').addClass('wizardry-hide');
                this.is_shipable = false;
                this.TotalFilterResult();
                
            }else if(value == 1){
                $('#PromoteContentLink').addClass('wizardry-hide');
                $('#promoteShippingLocationDiv').removeClass('wizardry-hide');
                this.is_shipable = true;
                this.TotalFilterResult();
            }
        });
    }
    
    public sortInputs(){
        $(document).on('keyup change', '#FollowSelect', (e)=>{
            this.followers_data = JSON.parse(<any>$('#FollowSelect option:selected').attr('data'));
            this.TotalFilterResult();
            $(e.currentTarget).trigger('PromoteContentUpdated');
        });
    }

    public setShippingLocation(){
        $(document).on('change', '#promoteShippingLocationInput', (e)=>{
             let price = $('#promoteShippingLocationInput option:selected').attr('data-price');
             $('#promoteShippingLocationDiv').text(<any>price);
             this.TotalFilterResult();
             $(e.currentTarget).trigger('PromoteContentUpdated');
        });
    }

    public sortPromoteFormat(){
        $(document).on('change', '#promoteFormatInput', (e)=>{
            this.TotalFilterResult();
            $(e.currentTarget).trigger('PromoteContentUpdated');
        });
    }

    public setInfluencerCount(){
        $(document).on('change', '#InfluencerCount', (e)=>{
            $(e.currentTarget).trigger('PromoteContentUpdated');
            this.TotalFilterResult();
        });
    }

    public TotalFilterResult(){
        this.setData();
        let shipping_price: any = this.is_shipable ? $('#promoteShippingLocationInput option:selected').attr('data-price') : 0;
        if(!this.is_shipable){
            $('.is_shippable_divs').addClass('wizardry-hide');
        }else{
            $('.is_shippable_divs').addClass('wizardry-hide');
        }
        
        let follower_price = this.followers_data[<any>$("#promoteFormatInput").val()];
        let total = parseInt(shipping_price) + parseInt(follower_price);
        let influencer_count = (<any>$('#InfluencerCount').val()) < 1 ? 1 : (<any>$('#InfluencerCount').val());
        $('#PromotetotalPrice').text(total * parseInt(influencer_count));
        $('#PromotetotalPriceInput').val(total * parseInt(influencer_count));
    }

    public getInfo(){
        $(document).on('PromoteContentUpdated', (e)=>{
            let params = {
                format: $("#promoteFormatInput").val(),
                number_of_influencers: $("#numberOfInfluencers").val(),
                duration: $('#totalPromoteDuration').val(),
                range_of_followers: $('#FollowSelect').val(),
                audience: $('#promoteShippingLocationInput').val()
            };
            $.ajax({
                url: (<any>$('#promoteFilterUrl').val()),
                type: 'POST',
                data: params,
                beforeSend: function(xhr){
                    
                },
                timeout: 20000
            })
            .fail((error: any)=>{
    
            })
            .done((data: any)=>{
               let dd = data.data;
               $('#influencerRangeDisplay').text(dd.influencers_range);
               $('#influencerCountDisplay').text(dd.influencers_count);
               $('#PotentialReach').text(dd.potential_reach);
               $('#EstimatedEngagements').text(dd.estimated_engagement);
               if(parseInt(dd.influencers_count) < 1){
                   $('#PromoteSubmitButton').prop('disabled', true);
               }else{
                   $('#PromoteSubmitButton').prop('disabled', false);
               }
            });

        });
    }
} 

$(function(){
    const ____promote = new Promote();
}) 