class Prompt{

    public constructor(){
        
        this.trigger_item();
        
        
    }

    public trigger_item(): any{
            $(document).on('click', '[data-prompt]', function(e){
                e.preventDefault();
                let title = $(e.currentTarget).attr("title");
                let message = $(e.currentTarget).attr("message");
                let url = $(e.currentTarget).attr("url");
                $('body').prepend(
                    '<div id="__global_prompt_modal" uk-modal><div class="uk-modal-dialog uk-modal-body uk-text-center p-0">'+
                    '<button class="uk-modal-close-default" type="button" uk-close></button>'+
                    '<h3 class="uk-modal-title mb-2 border-bottom p-3" style="color: #000; font-size: 16px; font-weight: 600;">'+title+'</h2>'+
                    '<div class="p-3"><p class="m-1 mb-2" style="color: #333; font-size: 14px;">'+message+'</p>'+
                    '<button class="btn btn-info mr-2 uk-modal-close" href="">NO</button>'+
                    '<a class="btn btn-info post-link" href="'+url+'">YES</a></div>'+
                    '</div></div>'
                ); 
                UIkit.modal("#__global_prompt_modal").show();
            });
    }

}

$(function(){
    let ___prompt = new Prompt();
})

