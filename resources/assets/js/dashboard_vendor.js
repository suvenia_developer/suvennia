require("modernizr"); 
require('jquery');
var NProgress = require("nprogress");
require('popper.js');
require("jquery-bar-rating");
require("gasparesganga-jquery-loading-overlay");
require("uikit/dist/js/uikit");
require("uikit/dist/js/uikit-icons");
require('bootstrap');
require("toastr");
require('chartjs');
require('./carbon');
require("spectrum-colorpicker");
require("croppie");
require("jquery-steps/build/jquery.steps");
require("@ckeditor/ckeditor5-build-classic/build/ckeditor.js");


