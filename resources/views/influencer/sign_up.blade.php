@extends('layouts.layout')

@section('title', 'Become An Influencer' )

@section('content')

<div class="uk-container mt-3 mb-3">
    <div class="row justify-content-center">
   
   <div class="col-md-4">
   
     <div class="card app-box">
       <div class="box-header">
         <p class="title">SIGN UP AS INFLUENCER</p>
         <p class="sub-title">Please fill the form to create your account</p>
       </div>
       <div class="box-body"> 
           <form class="app-form" action="{{ route('app:influencer:sign_up') }}" method="POST" >
               @csrf 
               <div class="form-group {{ $errors->has('invite_code') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="Invite Code" name="invite_code"  value="{{ old('invite_code') }}">
                <p class="form-text help-block">{{ $errors->first('invite_code') }}</p>
              </div>

              <div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="firstname" name="firstname"  value="{{ old('firstname') }}">
                <p class="form-text help-block">{{ $errors->first('firstname') }}</p>
              </div>
               
              <div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="lastname" name="lastname"  value="{{ old('lastname') }}">
                <p class="form-text help-block">{{ $errors->first('lastname') }}</p>
              </div>

              <div class="form-group {{ $errors->has('bio') ? 'has-error' : ''}}">
                <textarea class="form-control form-control-lg" placeholder="Bio" name="bio" ></textarea>
                <p class="form-text help-block">{{ $errors->first('bio') }}</p>
              </div>
              
              <div class="form-group {{ $errors->has('accepted_payment') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="accepted_payment">
                      <option value="">Accept Payment</option>
                      <option value="1">Yes</option>
                      <option value="0">No</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('accepted_payment') }}</p>
              </div>

              
              <div class="form-group {{ $errors->has('minimum_amount') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="minimum_amount">
                      <option value="">Minimum Amount</option>
                      <option value="5000">5000</option>
                      <option value="10000">10000</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('minimum_amount') }}</p>
              </div>

              
              <div class="form-group {{ $errors->has('specialties') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="specialties">
                      <option value="">Specialties</option>
                      <option value="products">Products</option>
                      <option value="movies">Movies</option>
                      <option value="religion">Religion</option>
                      <option value="trends">Trends</option>
                      <option value="music">Music</option>
                      <option value="food">Food</option>
                      <option value="food">Events</option>
                      <option value="lifestyle">Lifestyle</option>
                      <option value="tech">Tech</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('specialties') }}</p>
              </div>

              
              <div class="form-group {{ $errors->has('platform') ? 'has-error' : ''}}">
                <select class="form-control form-control-lg form-control-select" name="platform">
                      <option value="">Choose Preferred Platform</option>
                      <option value="instagram">Instagram</option>
                      <option value="facebook">Facebook</option>
                      <option value="twitter">Twitter</option>
                </select>
                <p class="form-text help-block">{{ $errors->first('platform') }}</p>
              </div>


              <div class="form-group {{ $errors->has('cellphone') ? 'has-error' : ''}}">
                <input type="text" class="form-control form-control-lg" placeholder="cellphone" name="cellphone"  value="{{ old('cellphone') }}">
                <p class="form-text help-block">{{ $errors->first('cellphone') }}</p>
              </div>


                  <button type="submit" class="btn btn-info  btn-block rounded">COMPLETE</button>
                </form>

       </div>
       
     </div>
       
   </div>
   
    </div>
</div>

@endsection