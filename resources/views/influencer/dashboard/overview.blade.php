@extends('layouts.dashboard_influencer')

@section('title', 'Overviews' )

@section('content')


<div class="container-fluid">
  
    <div class="row"> 
        <div class="col-md-12">
            <div class="card uk-animation-slide-top-small">
                   
                    <div class="card-body">
                        <div class="row dime_row">
                          @foreach($dimers as $dimer)
                            <div class="col-md-3 mb-2">
                                <div class="d-flex justify-content-between">
                                    <div>
                                    <img src="{{ asset($dimer->icon) }}" width="100" uk-responsive>
                                    </div>
                                    <div class="uk-text-center ml-2">
                                    <p class="dime_counter m-0">{{ $dimer->value }}</p>
                                    <p class="dime_desc">{{ $dimer->title }}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                           
                            <div class="col-md-6 mb-2 uk-text-right">
                                <div class="d-flex justify-content-between">
                                    <div></div>
                                    <div>
                                            <div class="uk-text-center">
                                                    <p class="m-1">Post Something New</p>
                                                        <a href="{{ route('app:influencer:dashboard:my_posts')}}" class="btn btn-info">POST</a>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-9">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Overview</h4>
                    </div>

                    <div class="card-body p-0">
                            
                        <div class="p-4">
                            <canvas id="bar-chart" width="100%" height="50"  data-months="{{ json_encode($month_names) }}" data-stats="{{ json_encode($month_stats) }}"></canvas>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                    <div class="card mb-4 p-4 uk-text-center">
                        <p class="balance_dimer">{{ Auth::guard('influencers')->user()->balance }}</p>
                        <p class="dimer_desc_grey">Account Balance</p>
                        <p><a href="{{ route('app:influencer:dashboard:withdraw') }}" class="btn btn-info">WITHDRAW</a></p>
                    </div>
            </div>
    </div>

    <div class="row">
            <div class="col-md-12">
                <div class="card uk-animation-slide-top-small">
                    <div class="card-header card-header-lite">
                        <h4 class="header_text m-0">Gigs Completed</h4>
                    </div>

                    <div class="card-body">
                        <div class="mb-2">

                        </div>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Order Date</th>
                                    <th>Customer</th>
                                    <th>Transaction Ref</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                        @foreach ($seller_orders as $order)
                                    
                                        <tr>
                                        <td>{{ \Carbon\Carbon::parse($order->created_at)->toDayDateTimeString() }}</td>
                                            <td>{{ $order->customer->username }}</td>
                                            <td>{{ $order->reference }}</td>
                                            <td>{{ $order->amount }}</td>
                                            <td><span class="_delivery_status">{{ isset($order->status) ? $utils->set_shipping_status($order->status) : '' }} </span></td>
                                        
                                           
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>

                        
                    </div>
                </div>
            </div>
    </div>

</div>

@endsection 