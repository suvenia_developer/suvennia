@extends('layouts.dashboard_influencer')

@section('title', 'My Orders' )

@section('content')
<div class="">
    <h4>My Orders</h4>
</div>
<div class="bg-white uk-box-shadow-small p-2">
   <div class="row justify-content-center">
       @forelse($orders as $gig)
       <div class="col-md-3">
            <div class="uk-card ">
                @if($gig->promotion_format == "VIDEO")
                <div class="bg bg-success p-1 uk-text-center w-25 text-white font-weight-bold uk-position uk-position-top-right " style="background: #1f7100;">Video</div>
                @else
                <div class="bg p-1 uk-text-center w-25 text-white font-weight-bold uk-position uk-position-top-right " style="background: #a36a3d;">Photo</div>
                @endif
                    <div class="uk-card-media-top">
                        <img src="{{ asset($gig->product->photos->first()->public_url) }}" alt="">
                    </div>
                    <div class="uk-card-body p-3 bg-light">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p class="font-weight-bold m-0 text-dark" style="font-size:14px;">{{ title_case($gig->product->name) }}</p>
                                    <p class="text-muted m-0" style="font-size:14px;">{{ title_case($gig->product->name) }}</p>
                                </div>
                                <div>
                                    <p class="text-muted m-0" style="font-size:14px;">Amount</p>
                                    <p class="font-weight-bold m-0 text-dark" style="font-size:14px;">&#8358;{{ title_case($gig->product->price) }}</p>
                                </div>
                            </div> 
                            <div class="mt-1">
                                <p class="text-muted m-0" style="font-size:14px;">{{ str_limit($gig->description, 30) }}</p>
                            </div>
                            @if(!$gig->is_closed)
                            <div class="d-flex justify-content-between mt-2">
                                    <div>
                                        <a href="" class="text-primary font-weight-bold">Reject</a>
                                    </div>
                                    <div>
                                        @php
                                           $modal_title = $gig->promotion_format == "VIDEO" ? "Video Promotion" : "Photo Promotion"; 
                                        @endphp
                                       <a href="javascript:;" class="btn btn-info rounded-pill" modal-form data-title="{{ $modal_title }}" data-url="{{ route('app:influencer:dashboard:gig_modal', ['id'=> $gig->id]) }}">Accept</a>
                                    </div>
                            </div>
                            @else
                                <div class="d-flex justify-content-between mt-2">
                                        <div>
                                        <small class="font-weight-bold text-dark">{{ \Carbon\Carbon::parse($gig->closed_at)->diffForHumans() }}</small>
                                        </div>
                                        <div>
                                        <div class="p-2 rounded-pill text-white" style="background: #9b9b9b; font-size:13px;">closed</div>
                                        </div>
                                </div>
                            @endif
                    </div>
                </div>
        </div>
        @empty
        <div class="col-md-12 uk-text-center">
            <p>Ooops.. no jobs at this time!</p>
        </div>
       @endforelse
   </div>
</div>
@endsection