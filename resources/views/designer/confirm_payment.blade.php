@extends('layouts.designer')

@section('title', 'Confrm Payment' )

@section('content')

<div class="uk-container uk-container-small mb-3 mt-3">
    <div class="row justify-content-center">
        <div class="col-md-5">
                <div class="bg-white mb-3 shadowed-box">
                        <div class="p-3 cart_shipping_zone_header_bottom">
                                <p class="cart_shipping_zone_header m-0">Payment Method</p>
                           
                        </div> 
                        
                        <div class="row no-gutters border-bottom-pay p-4">
                                <div class="col-md-12"><img src="{{ asset('img/paystack.png') }}" width="150"></div>
                        </div> 
                        <div class="row p-3">
                            <div class="col-6">
                                <p class="pay_with_text mb-2 ml-0">Pay With</p>
                                <div class="mt-0 d-inlin">
                                        <img src="{{ asset('img/master_card.png') }}" width="42" height="24" class="d-inlin">
                                        <img src="{{ asset('img/visa.png') }}" width="42" height="24" class="d-inlin">
                                        <img src="{{ asset('img/verve.png') }}" width="42" height="24" class="d-inlin">
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        <div class="col-md-5">
            <div class="confirm_pay_card p-4">
                    <div class="row no-gutters">
                            <div class="col-md-2">
                                <img src="{{ asset($plan->gig->photos->first()->public_url) }}" uk-responsive width="100">
                            </div>
                            <div class="col-md-7 pl-3">
                                <p class="m-0 title">{{ $plan->gig->title }}</p>
                                <p class="m-0 user">{{ $plan->gig->user->username }}</p>
                                 
                            </div> 
                            <div class="col-md-3"></div>
                    </div>
                    <div class="row mt-3 border-bottom">
                        <div class="col-md-12">
                        <h2 class="plan_title m-1">{{ $plan->name }}</h2>
                            <div class="plan_details">
                                    <ul class="list-unstyled">
                                            <li class="d-block feature"><i class="icon-checked mr-2 color-check"></i><span> {{ $plan->duration }} Days</span></li>
                                            <li class="d-block feature">
                                                    @if($plan->is_printable)
                                                    <i class="icon-checked mr-2 color-check"></i>
                                                    @else
                                                    <i class="icon-cancel mr-2 color-cancel"></i>
                                                    @endif
                                                    <span>Print-Ready</span>
                                            </li>
                                            <li class="d-block feature">
                                                @if($plan->has_source_file)
                                                <i class="icon-checked mr-2 color-check"></i>
                                                @else
                                                <i class="icon-cancel mr-2 color-cancel"></i>
                                                @endif
                                                <span> Files</span></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between sub_total_box border-bottom mb-3 pb-3 pt-3">
                            <div class="title">
                                Sub total
                            </div>
                            <div class="amount">
                                &#8358;{{ $plan->price }}
                            </div>
                    </div>
                    <div class="d-flex justify-content-between sub_total_box mb-5">
                            <div class="title_bold">
                                Total
                            </div>
                            <div class="amount_bold">
                                &#8358;{{ $plan->price }}
                            </div>
                    </div>
                    <form action="{{ route('app:designer:process_payment') }}" method="POST">
                        <div class="row no-gutters  p-4">
                            <div class="col-md-12 form-group">
                                <div class="row" uk-switcher="toggle:div > div > label; connect: #authContainer">
                                    <div class="col-md-6"> 
                                            <div class="duration_button uk-text-center">
                                                <input type="radio" name="user_type" value="BUYER" id="buyer_inp_div" checked>
                                                <label for="buyer_inp_div" class="w-100" onclick="switchTab('#buyer_inp_div')">I AM A BUYER</label>
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="duration_button uk-text-center">
                                                <input type="radio" name="user_type" value="SELLER" id="seller_inp_div">
                                                <label for="seller_inp_div" class="w-100" onclick="switchTab2('#seller_inp_div')">I AM A SELLER</label>
                                            </div>
                                    </div>
                                    
                                </div>
                                <script>
                                   function switchTab(id){
                                            $(id).prop('checked', true);
                                            let val = $(id).val();
                                            if(val == 'BUYER' && $('#buyerLogged').length < 1){
                                                $('#postRequestButton').prop('disabled', true);
                                            }else{
                                                $('#postRequestButton').prop('disabled', false);
                                            }
                                    };
                                   function switchTab2(id){
                                            $(id).prop('checked', true);
                                            let val = $(id).val();
                                            if(val == 'SELLER' && $('#sellerLogged').length < 1){
                                                $('#postRequestButton').prop('disabled', true);
                                            }else{
                                                $('#postRequestButton').prop('disabled', false);
                                            }
                                    };
                                </script>

                                <div class="row">
                                        <ul id="authContainer" class="uk-switcher col-md-12">
                                            <li>
                                                @auth
                                                <div class="bg-light p-3" id="buyerLogged">
                                                        <div class="media">
                                                        <img src="{{ Auth::user()->photo_url }}" class="align-self-center mr-3 uk-border-circle" alt="{{ Auth::user()->username }}" width="50" uk-responsive>
                                                        <div class="media-body">
                                                            <p class="mt-0">{{ Auth::user()->username }}</p>
                                                        </div>
                                                        </div>
                                                </div>
                                                @else
                                                <p class="text-muted mt-0 mb-1" style="font-size: 14px;">You are currently not signed in</p>
                                                    <a href="{{ route('app:user:login') }}" class="btn btn-info btn-sm">Sign In or Register</a>
                                                @endif
                                            </li>
                                            <li>
                                                @auth('sellers')
                                                <div class="bg-light p-3" id="sellerLogged">
                                                        <div class="media">
                                                        <img src="{{ Auth::guard('sellers')->user()->photo_url }}" class="align-self-center mr-3 uk-border-circle" alt="{{ Auth::guard('sellers')->user()->username }}" width="50" uk-responsive>
                                                        <div class="media-body">
                                                            <p class="mt-0">{{ Auth::guard('sellers')->user()->username }}</p>
                                                        </div>
                                                        </div>
                                                </div>
                                                @else
                                                <div class="bg-light p-3">
                                                    <p class="text-muted mt-0 mb-1" style="font-size: 14px;">You are currently not signed in as a seller</p>
                                                    <a href="{{ route('app:seller:login') }}" class="btn btn-info btn-sm">Sign In or Register</a>
                                                </div>
                                                @endif
                                            </li>
                                            
                                        </ul>
                                       
                                </div>
                            </div>
                        </div> 
                        @csrf
                        <div class="mt-2 uk-text-center">
                        <input type="hidden" name="plan" value="{{ $plan->id }}">
                            <button class="btn btn-info-dark" type="submit" @auth @else disabled @endauth id="postRequestButton">PROCEED</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection