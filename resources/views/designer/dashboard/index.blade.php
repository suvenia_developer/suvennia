@extends('layouts.dashboard_designer')

@section('title', 'Overviews' )

@section('content')


<div class="container-fluid">

    <div class="row"> 
        <div class="col-md-12">
            <div class="card uk-animation-slide-top-small">
                    <div class="card-body">
                        <div class="row dime_row">
                                @foreach(json_decode(json_encode($dimers), FALSE) as $dimer)
                                <div class="col-md-3 mb-2">
                                        <div class="uk-text-center">
                                        <p class="dime_desc">{{ $dimer->title }}</p>
                                        <p class="dime_counter m-0">{{ $dimer->value }}</p>
                                        </div>
                                </div>
                                @endforeach
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row"> 
            <div class="col-md-6">
                <div class="p-3 bg-white">
                    <p class="active_order_title m-0">Active Order - {{ $active_orders->count() }}</p> 
                </div>
                @foreach($active_orders as $order)
                <div class="mt-2 p-2 bg-white">
                    <div class="row">
                        <div class="col-md-2">
                        <img src="{{ asset($order->designer->photo_url) }}" uk-responsive width="100">
                        </div>
                        <div class="col-md-4">
                        <p class="m-0 text-bold-order">{{ $order->reference }}</p>
                        </div>
                        <div class="col-md-3">
                            <p class="m-0 text-bold-order">&#8358; {{ $order->amount }}</p>
                        </div>
                        <div class="col-md-2">
                            <a href="{{ route('app:designer:dashboard:view_order', ['id'=> $order->id]) }}" class="btn btn-app-border btn-sm">View Order</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
    </div>

</div>

@endsection 