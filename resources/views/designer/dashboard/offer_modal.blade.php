<form action="{{ route('app:designer:dashboard:send_offer', ['id'=> $request->id ]) }}" method="post" class="row mt-0">
    @csrf
    <div class="col-md-12 form-group">
        <label class="app-label">Choose A Gig For This Offer</label>
        <select class="form-control" name="gig">
            @foreach ($gigs as $gig)
                <option value="{{ $gig->id }}">{{ title_case($gig->title) }}</option>
            @endforeach
        </select>
</div>
<div class="col-md-12 form-group uk-text-center">
    <button class="btn btn-info" type="submit">Send Offer</button>
</div>
</form>