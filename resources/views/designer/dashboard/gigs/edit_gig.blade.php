@extends('layouts.dashboard_designer')

@section('title', 'Edit ' . $gig->title )

@section('content')
<form action="{{ route('app:designer:dashboard:edit_gig', ['id'=> $gig->id]) }}" method="POST" id="newGigForm" gig-post>
  @csrf
<div id="GigCreate"></div>

<div id="add-edit-gig">
@php
$tabs = [ 
    [
        'name'=> 'Overview',
        'id'=> '_overview'
    ],
    [
        'name'=> 'Pricing',
        'id'=> '_pricing'
    ],
    [
        'name'=> 'Description',
        'id'=> '_description'
    ],
    [ 
        'name'=> 'Requirement',
        'id'=> '_requirement'
    ],
    [
        'name'=> 'Gallery', 
        'id'=> '_gallery'
    ],
    [
        'name'=> 'Finish',
        'id'=> '_finish'
    ]
];
@endphp

  
<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[0]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-lite">
                    Basic Settings
                </div>
                <div class="card-body p-3">

                    <div class="row justify-content-left mt-3">
                    <div class="col-md-12">
                            <form action="" method="post" data-post>
                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">GIG TITLE</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="Make You Title Catchy" class="form-control" name="gig_title" value="{{ $gig->title }}"> 
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">CATEGORY</label>
                                        </div>
                                        <div class="col-md-9">
                                             <select class="form-control" name="gig_category">
                                                 @foreach($categories as $category)
                                                 @php $mark_selected = $category->id == $gig->category_id ? 'selected' : ''; @endphp
                                                 <option value="{{ $category->id }}" {{ $mark_selected }}>{{ title_case($category->title) }}</option>
                                                 @endforeach
                                             </select>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">SERVICE TYPE</label>
                                            <p class="label-sub m-0">Input the type of services you provide</p>
                                        </div>
                                        <div class="col-md-9">
                                             <input type="text" class="form-control" placeholder="" name="service_type" value="{{ $gig->service_type}}">
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row no-gutters">
                                        <div class="col-md-3">
                                            <label for="" class="label-title">SEARCH TAGS</label>
                                            <p class="label-sub m-0">Enter search terms, which you feel buyers will use when looking for your service.</p>
                                        </div>
                                        <div class="col-md-9">
                                        <textarea placeholder="Up to 5 search terms" class="form-control" name="tags">{{ $gig->tags }}</textarea>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                            </form>
                    </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
</section>
  
<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[1]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
                <div class="card mt-3 plan-card">
        
                         @php
                        $plan_compare_data = [
                            ['name'=> '', 'key'=> 'name'   ],
                            ['name'=> '', 'key'=> 'description' ],
                            ['name'=> 'Print-Ready', 'key'=> 'is_printable' ],
                            ['name'=> 'Source File', 'key'=> 'source_file'  ],
                            ['name'=> 'Design Concepts', 'key'=> 'design_concepts'  ],
                            ['name'=> 'Revision', 'key'=> 'revisions'   ],
                            ['name'=> 'Delivery Time', 'key'=> 'delivery_time'  ],
                            ['name'=> 'Price', 'key'=> 'price'  ],
                           // ['name'=> '', 'key'=> 'button'    ],
                        ];
                        $__compare_data = json_decode(json_encode($plan_compare_data));
                    @endphp
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered compare_table mb-0">
                        
                                    <tbody>
                                        @foreach($__compare_data as $compare_data)
                                        <tr>
                                            <th class="compare_title">{{ $compare_data->name }} </th>
                                            @foreach($gig->plans as $gigE) 
                                            @php $kkey = $compare_data->key; $gigEIndex = $loop->index; @endphp 
                                            @if($compare_data->key == 'name')
                                            <td class="cell_price tab-title">
                                                <!--<div class="cell_gig_title">{{ $gigE->$kkey }}</div>-->
                                                <textarea placeholder="Name your package" class="form-control tab_form" name="plans[{{$gigEIndex}}][plan_name]">{{ $gigE->$kkey }}</textarea>
                                            </td>
                                            @elseif($compare_data->key == 'description')
                                            <td>
                                                <textarea placeholder="Describe your package" class="form-control tab_form" name="plans[{{$gigEIndex}}][plan_description]">{{ $gigE->$kkey }}</textarea>
                                            </td>
                        
                                            @elseif($compare_data->key == 'revisions')
                                            @php $revisions = [4,6,10,12,'unlimited'];  @endphp
                                            <td>
                                                <select class="form-control tab_form" name="plans[{{$gigEIndex}}][plan_revisions]">
                                                    @foreach ($revisions as $revI)
                                                    @php $checkRev = $revI == $gigE->revisions ? 'selected' : ''; @endphp
                                                      <option value="{{$revI}}" {{ $checkRev }}>{{$revI}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            @elseif($compare_data->key == 'design_concepts')
                                            @php $design_conceptsArr = ['1-3', '4-5', '6-8', 'unlimited']; @endphp
                                            <td>
                                                <select class="form-control tab_form" name="plans[{{$gigEIndex}}][plan_design_concepts]">
                                                    @foreach($design_conceptsArr as $concept)
                                                    @php $conceptCheck = $concept == $gigE->design_concepts ? 'selected' : ''; @endphp
                                                    <option value="{{ $concept }}" {{ $conceptCheck }}>{{ $concept }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            @elseif($compare_data->key == 'is_printable') 
                                            @php 
                                              $check1 = str_random(5);
                                              $check_is_printable = $gigE->is_printable == true ? 'checked': '';
                                            @endphp
                                            <td class="uk-text-center">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1{{$check1}}" name="plans[{{$gigEIndex}}][plan_printable]" {{ $check_is_printable }}>
                                                    <label class="custom-control-label" for="customCheck1{{$check1}}"></label>
                                                </div>
                                            </td>
                        
                                            @elseif($compare_data->key == 'source_file') 
                                            @php
                                                $check2 = str_random(5);
                                                $check_has_source = $gigE->has_source_file == true ? 'checked': ''; 
                                             @endphp
                                            <td class="uk-text-center">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2{{$check2}}" name="plans[{{$gigEIndex}}][source_file]" {{ $check_has_source}}>
                                                    <label class="custom-control-label" for="customCheck2{{$check2}}"></label>
                                                </div>
                                            </td>
                        
                                            @elseif($compare_data->key == 'delivery_time')
                        
                                            <!--<td class="cell_con">{{ $gigE->$kkey }} Days</td>-->
                                            <td>
                                                <input type="datetime" class="tab_form" name="plans[{{$gigEIndex}}][delivery_time]" value="{{ $gigE->duration }}">
                                            </td>
                        
                                            @elseif($compare_data->key == 'price')
                                            <td>
                                                <input type="number" class="form-control tab_form" placeholder="Enter A Price" name="plans[{{$gigEIndex}}][price]" value="{{ $gigE->$kkey }}">
                                            </td>
                        
                                            @endif @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
        </div>
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[2]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="m-0 tab_header_text mb-2">Description</h4>
            <textarea class="form-control" id="appEditor1" name="gig_description">{!! $gig->description !!}</textarea>
        </div>
        
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[3]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-3">
        <div class="row justify-content-center">
        <div class="col-md-12">
            
            <h4 class="m-0 tab_header_text mb-2">Requirements</h4>
            <textarea class="form-control appEditor" id="appEditor2" name="gig_requirements">{!! $gig->reqirements !!}</textarea>
        </div>
        
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[4]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-2">
    
        <div class="row justify-content-center">
        <div class="col-md-12">
            <h4 class="m-0 tab_header_text mb-1">Previous Work</h4>
            <p class="m-0 tab_header_sub mb-3">Add photos of your past work related to this Gigs to make you stand out</p>
            <div class="card">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="header_file border-bottom p-3">
                        ADD FILE
                    </div>
                    <p class="text_desc_file p-3">Accepted File Types (Max file size: 10MB)
                            We accept the following file types: png, jpg, gif</p>
                    <div class="mt-3 p-3">
                            <form action=""
                            class="dropzone"
                            id="drop-area" upl-url="{{ route('app:designer:dashboard:upload_gallery') }}"></form>
                    </div>
                </div>
                <div class="col-md-6 border-left p-2">
                    <ul id="fileLister" class="list-unstyled">
                        @foreach($gig->photos as $photo)
                    <li class="d-block border-bottom p-3" id="imageEdit{{$photo->id}}"><img width="150" src="{{ asset($photo->public_url) }}" uk-responsive><p class="m-0" style="font-size: 12px; color: red; cursor: pointer;" data-url="{{ route('app:designer:dashboard:designer_delete_single_image', ['id'=>$photo->id]) }}" ajax-delete-item title="Delete Image" message="Do you want to delete this Image?" data-callback-type="DELETE" data-target-element="#imageEdit{{$photo->id}}">remove</p></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        </div>
</section>

<h6 class="col-md-2 col-6 mb-1 app-hide"><div class="check-ball d-inline-block mr-2"><i uk-icon="check"></i></div>{{ $tabs[5]['name'] }}</h6>
<section class="uk-animation-slide-top-small mb-3">
        <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card uk-text-center p-5">
                <h1 class="m-1 complete_title_text">YOU ARE READY</h1>
                <p class="text_complete_sub">Let’s publish your Gig so buyers can see .</p>
            </div>
        </div>
       
        </div>
    </div>
</section>

</form>
@endsection