@extends('layouts.designer2')

@section('title', 'Become A Designer' )

@section('content')
@php $redirectUrl = url()->current(); @endphp

<div class="uk-cover-container uk-height-large">
        <img src="{{ $utils->get_image('site.hpcreatedesign') }}" alt="" uk-cover>
       <!-- <div class="uk-overlay-primary uk-position-cover"></div>-->
        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center p-2"> 
                <div class="col-md-12">
                    
                    <div class="uk-text-center">
                    <div class="n-centered-hero">
                        <b>GET AMAZING DESIGN WORK DONE FASTER!</b> 
                        <p class="mt-1 n-centered-sub-hero">Earn Online with your Creative Skill.</p>
                    </div>
                    <div class="mt-4"> 
                    <a class="btn btn-info" style="color: #fff;" href="{{ route('app:designer:sign_up') }}">GET STARTED</a>
                    </div>
                    </div> 
 
                </div>
            </div>
        </div>
  
</div> 

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="borde" style="position: relative; height:100px; background: #fff;">
        <div class="float-slider" style="padding: 20px 60px;" uk-slider>
                <div class="row">
                    <div class="col-12 uk-text-center">
                    <h2 class="m-0 header">Kickass designs you’ll love</h2>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >
        
                                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                                    @foreach($categories as $category)
                                    @php $params = $category->slug . '-' . $category->ref; @endphp
                                        <li class="uk-text-center">
                                            <a href="{{ route('app:designer:catalogue', ['params'=> $params]) }}">
                                                <div class="uk-panel">
                                                    <img src="{{ asset($category->photo_url) }}" alt="" width="150" uk-responsive>
                                                
                                                </div>
                                            </a>
                                        <a href="{{ route('app:designer:catalogue', ['params'=> $params]) }}" class="_prod_title mt-2">{{ title_case($category->title) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                
                        </div>
                </div>
                <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
                <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
        </div>
</div>


<div class="uk-section bg-grey p-4">
        <div class="uk-container uk-container-small" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <p class="m-0 after_cat_slide_desc">Suvenia makes it easy for you to request for amazing designs for all of your personal or business needs, from our community of designers. Fast, affordable and guranteed.</p>
                </div>
            </div>
        </div>
</div>
@endif


<div class="uk-section" style="background: #fff;">
        <div class="uk-container uk-container-small">
                <div class="lite-bg-header uk-text-center mb-2">
                    <h1 class="m-0 header-main">TWO WAYS TO GET YOUR PROJECT DONE!</h1>
                 </div>
                
                <div class="row justify-content-center mt-3" uk-grid uk-height-match="target: > div > .card_earn">
                   <div class="col-md-4">
                       <div class="card_earn p-3 uk-text-center">
                           <h2 class="m-0 title">POST A REQUEST</h2>
                           @php 
                                $category = $categories->first();
                                $paramSingle = $category->slug . '-' . $category->ref;
                            @endphp
                           <p class="mt-2 content">Set up your design request by filling a short form that contain all the necessary details needed for a designer to work on your project. </p>
                           <p class="mt-1"><a href="{{ route('app:designer:post_request') }}" class="btn btn-info-dark">Post a Request</a></p>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card_earn p-3 uk-text-center">
                           <h2 class="m-0 title">FIND DESIGNER</h2>
                           <p class="mt-2 content">Explore and search for designer. Choose from pool of professional designers to help with your project.</p>
                           <p class="mt-1"><a href="{{ route('app:designer:catalogue', ['params'=>  $paramSingle]) }}" class="btn btn-info-dark">Explore</a></p>
                       </div>
                   </div>
                </div>

        </div>
</div>

<div class="uk-section bg-app">
        <div class="uk-container uk-container-small">
                <div class="bg-banner-header uk-text-center mb-5">
                        <h1 class="m-0">FAQ</h1>
                    </div>
            <div class="row justify-content-center">
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content1" aria-expanded="false" aria-controls="faq-collapse-content1">
                            <div class="d-flex justify-content-between">
                                <div>How much does it cost to be a seller</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content1">
                        Nothing!.. Just give your life to christ
                    </div>
    
                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content2" aria-expanded="false" aria-controls="faq-collapse-content2">
                            <div class="d-flex justify-content-between">
                                <div>What can I sell?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content2">
                        Nothing! freely yea recieve, freely yea shall give
                    </div>
    
                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content3" aria-expanded="false" aria-controls="faq-collapse-content3">
                            <div class="d-flex justify-content-between">
                                <div>How do I price my service?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content3">
                        Simple! by how many souls you have won for christ
                    </div>
    
                </div>
                <div class="col-md-6 mb-3">
                    <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content4" aria-expanded="false" aria-controls="faq-collapse-content4">
                            <div class="d-flex justify-content-between">
                                <div>How do I get paid?</div>
                                <div><span class="icon-caret-down ml-2"></span></div>
                            </div>
                    </button>
                    <div class="collapse p-3 bg-white border-top" id="faq-collapse-content4">
                        Seriously?! Your reward is in heaven
                    </div>
    
                </div>
    
            </div>
        </div>
</div>


@endsection 