@extends('layouts.designer')

@section('title', 'Success')

@section('content')
<div class="uk-container uk-container-small mt-3 mb-3">
        
        <div class="row justify-content-center">
                <div class="col-8 uk-text-center">
                        <img src="{{ asset('img/pay_success.svg') }}" uk-responsive width="100">
                        <h3 class="m-2 _pay_status_title">Payment was successfull</h3>
                        <p class="m-1 _pay_status_text">Thank you for your Order</p>
                        <p class="m-1 _pay_status_text">Your Order number is <a href="">{{ $order }}</a></p>
                        <p class="m-1 _pay_status_text">We will email your order details and information.</p>
                        <p class="m-2"><a href="{{ route('app:designer:landing_page_hire') }}" class="btn btn-info-dark">EXPLORE DESIGNERS</a></p>
                        
                </div>
        </div>
</div>

@endsection