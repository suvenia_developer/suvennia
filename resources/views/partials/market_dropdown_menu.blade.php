@php
  $basic_menu = collect([
      [
          'title'=> 'Overview',
          'route'=> 'app:dashboard:index'
      ],
      [
          'title'=> 'My Orders', 
          'route'=> 'app:dashboard:my_orders'
      ],
      [
          'title'=> 'Account',
          'route'=> 'app:dashboard:profile-settings'
      ],
      [
          'title'=> 'Shipping',
          'route'=> 'app:dashboard:shipping'
      ],
      [
          'title'=> 'Reviews',
          'route'=> 'app:dashboard:reviews'
      ],
      [
          'title'=> 'Saved Items',
          'route'=> 'app:dashboard:saved_items'
      ],
      [
          'title'=> 'Transactions',
          'route'=> 'app:dashboard:payment_history'
      ],
  ]);  
@endphp

@foreach ($basic_menu->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
