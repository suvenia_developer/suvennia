<div class="uk-animation-toggle" tabindex="0">
<div class="uk-card uk-card-default uk-animation-scale-up" data-link="{{ route('app:designer:view_gig', ['ref'=> $gig->ref]) }}">
    <div class="uk-card-media-top">

        
    <img src="{{ $gig->photos->count() > 0 ? asset($gig->photos->first()->public_url) : asset('storage/designers/plh.jpg')}}" alt="">
    </div>
    <div class="uk-card-body p-0">
    <div class="p-2">
        <ul class="m-0 p-0">
            <li class="d-inline-block">
            <img src="{{ isset($gig->user->photo_url) ? asset($gig->user->photo_url) : asset('user/default.png')}}" class="uk-border-circle" uk-responsive width="36">
            <span class="designer_card_catch_phrase">{{ title_case($gig->user->username) }}</span>
            </li>
            <li class="d-block mt-2">
            <a href="javascript:;" class="designer_card_title mt-2"><p class="m-0">{{ title_case($gig->title) }}</p></a>
            </li>
        </ul>
    </div>
    <div class="pr-2 pl-2 pb-2">
    <p class="m-0 designer_card_catch_phrase">{{ $gig->service_type }}</p>
    </div>
    <div class="border-top">
    <div class="row">
        <div class="col-6">
            <p class="m-0 p-2 designer_card_price_pane">STARTING PRICE</p>
        </div>
        <div class="col-6">
        <p class="m-0 uk-text-right p-2 designer_card_price_pane">&#x20A6; {{ $gig->plans->first()['price']}}</p>
        </div>
    </div>
    </div>

    </div>
</div> 
</div> 