<div class="row">
@forelse ($products as $product)
@php $__cat = $product->categories->first(); @endphp
@php 
    $photo = $product->photos->first();
    $design_colors = json_decode(json_decode($product->design->colors));
    $get_color = collect($design_colors)->random();
@endphp
<div class="col-12 border-bottom border-gray">
<div class="media text-muted pt-3">
        <img src="{{ asset($photo->public_url) }}" uk-responsive width="50" style="background: {{ $get_color }};">
    
        @if($product->categories->count() > 0)
        <a class="media-body pb-3 mb-0" href="{{ route('app:ecommerce:single_product', ['category'=> $__cat->slug, 'param'=> $product->slug . '_' . $product->ref]) }}">
        @else
        <a class="media-body pb-3 mb-0" href="javascript:;">
        @endif
          {{ $product->name }}
        </a>
</div>
</div>
@empty
<div class="col-12 border-bottom border-gray">
    <p class="uk-text-center">No result found..</p>
</div>
@endforelse
@if($products->count() > 0)
<div class="col-12 uk-text-right mt-2">
    <p><a href="{{ route('app:ecommerce:product_search', ['q'=> request()->q ]) }}">Sell All Results</a></p>
</div>
@endif

</div>