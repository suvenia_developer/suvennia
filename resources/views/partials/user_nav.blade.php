
<ul class="nav nav-fill">
        <li class="nav-item">
          <a class="nav-link {{ $utils->isActive(['app:user:basic_settings']) ? 'active' : '' }}" href="{{ route('app:user:basic_settings') }}">MY INFORMATION</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ $utils->isActive(['app:user:orders']) ? 'active' : '' }}" href="{{ route('app:user:orders') }}">MY ORDERS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ $utils->isActive(['app:user:shipping_address']) ? 'active' : '' }}" href="{{ route('app:user:shipping_address') }}">SHIPPING ADDRESS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ $utils->isActive(['app:user:saved_items']) ? 'active' : '' }}" href="{{ route('app:user:saved_items') }}">SAVED ITEMS</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ $utils->isActive(['app:user:review_product']) ? 'active' : '' }}" href="{{ route('app:user:review_product') }}">REVIEWS</a>
        </li>

</ul>