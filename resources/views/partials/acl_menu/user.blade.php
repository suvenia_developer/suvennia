@php
  $menus = collect([
     /* [
          'title'=> 'My Dashbooard',
          'route'=> 'app:dashboard:index',
          'icon'=> 'icon-speedometer'
      ],*/
      [
          'title'=> 'My Orders',
          'route'=> 'app:dashboard:my_orders',
          'icon'=> 'icon-basket-loaded'
      ], 
      [
          'title'=> 'Profile Settings',
          'route'=> 'app:dashboard:profile_settings',
          'icon'=> 'icon-user'
      ],
      [
          'title'=> 'Payment History',
          'route'=> 'app:dashboard:payment_history',
          'icon'=> 'icon-credit-card'
      ],
      [
          'title'=> 'Saved Items',
          'route'=> 'app:dashboard:saved_items',
          'icon'=> 'icon-plus'
      ],
      [ 
          'title'=> 'Reviews',
          'route'=> 'app:dashboard:reviews',
          'icon'=> 'icon-like'
      ],
      [
          'title'=> 'Shipping Information',
          'route'=> 'app:dashboard:shipping',
          'icon'=> 'icon-plane'
      ],
      
  ]);   
@endphp

@if(isset($type) && $type == 'admin_dropdown')

@foreach ($menus->all() as $item)
    <a href="{{ route($item['route']) }}" class="dropdown-item">
        <i class="icon {{ $item['icon']}}"></i> {{ $item['title']}}
    </a>
@endforeach

@endif

@if(isset($type) && $type == 'admin_sidebar_header')

<li class="nav-item super-header">
    <a href="" class="nav-link active">
        My Account <i class="icon icon-minus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
@endif

@if(isset($type) && $type == 'admin_sidebar_content')
@foreach ($menus->all() as $item)
<li class="nav-item">
        <a href="{{ route($item['route']) }}" class="nav-link">
            <i class="icon {{ $item['icon']}}" style="font-size: 14px;"></i> {{ $item['title']}}
        </a>
</li> 
@endforeach

@endif

@if(isset($type) && $type == 'front_end')
@foreach ($menus->all() as $item)
<li class=""><a href="{{ route($item['route']) }}" target="_blank">{{ $item['title'] }}</a></li>
@endforeach
@endif

@if(isset($type) && $type == 'admin_sidebar_footer')
 
<li class="nav-item super-header">
    <a href="{{ route('app:dashboard:design_services') }}" class="nav-link active">
        Design Services <i class="icon icon-cheveron-right" style="font-size: 14px; float:right;"></i>
    </a> 
</li> 

<!--<li class="nav-item super-header">
    <a href="{{ route('app:seller:dashboard:index') }}" class="nav-link active">
        Seller <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<li class="nav-item super-header">
    <a href="{{ route('app:influencer:dashboard:overview') }}" class="nav-link active">
        Influencer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>
<li class="nav-item super-header">
    <a href="{{ route('app:designer:dashboard:index') }}" class="nav-link active">
        Designer <i class="icon icon-plus" style="font-size: 14px; float:right;"></i>
    </a>
</li>-->
@endif