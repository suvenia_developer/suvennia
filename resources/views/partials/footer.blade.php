<div class="uk-section p-4 bg-app">
        <div class="uk-container">
            <div class="row">
                <div class="col-md-9">
                    <ul class="list-unstyled menu-footer">
                        <li><a href="">FAQ</a></li>
                        <li><a href="">return policy</a></li>
                        <li><a href="">seller terms</a></li>
                        <li><a href="">terms</a></li>
                        <li><a href="">shipping</a></li>
                        <li><a href="">why suvenia</a></li>
                        <li><a href="">contact us</a></li>
                        <li><a href="">blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="">
                            <a href="" class="uk-icon-button uk-margin-small-right app-list-icon" uk-icon="twitter"></a>
                            <a href="" class="uk-icon-button  uk-margin-small-right app-list-icon" uk-icon="facebook"></a>
                            <a href="" class="uk-icon-button app-list-icon" uk-icon="instagram"></a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <p class="app-copyright">&copy; 2018 Suvenia.com</p>
                </div>
            </div>

        </div>
    </div>
