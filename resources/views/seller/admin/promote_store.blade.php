<div class="row">
        <div class="col-md-7">
            <form action="{{ route('app:seller:dashboard:promote_store') }}" method="post" data-post>
                @csrf
                <input type="hidden" name="store_id" value="{{ $id }}">
            <div class="row">
                <div class="col-md-12 form-group title">
                        <label class="app-label">Title</label>
                        <input type="text" class="form-control app-form" name="title" value="" placeholder="Title">
                        <span class="help-block"></span>
                </div>
                <div class="col-md-12 form-group gig_content">
                        <label class="app-label">Description</label>
                        <textarea class="form-control app-form" name="gig_content" value="" placeholder="Content"></textarea>
                        <span class="help-block"></span>
                </div>
                <div class="col-md-12 form-group gig_type">
                        <label class="app-label">Type</label>
                        <select class="form-control app-form" name="gig_type">
                            <option value="video">Video</option>
                            <option value="photos">Photos</option>
                        </select>
                        <span class="help-block"></span>
                </div>
                <div class="col-md-12 form-group duration">
                        <label class="app-label">Duration</label>
                        <input type="number" class="form-control app-form" name="duration" value="" placeholder="E.G 2, 3">
                        <span class="help-block"></span>
                </div>
                <div class="col-md-12 form-group budget">
                        <label class="app-label">Budget</label>
                        <input type="number" class="form-control app-form" name="budget" value="" placeholder="Budget">
                        <span class="help-block"></span>
                </div>
                <div class="col-md-12 form-group">
                    <button type="submit" class="btn app-btn-info btn-sm">Promote</button>
                </div>
             </div>
           </form>
        </div>
        <div class="col-md-5">

        </div>
    </div>