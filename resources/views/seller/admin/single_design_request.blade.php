@extends('layouts.dashboard_seller')

@section('title', 'Design Offers' )

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card uk-animation-slide-top-small">
                <div class="card-header card-header-lite">
                    <div class="row">
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_services') }}" class="card_head_nav_item {{  $utils->makeActive(['app:seller:dashboard:design_services'], 'active') }}">My Orders</a>
                        </div>
                        <div class="col-md-4 uk-text-center">
                        <a href="{{ route('app:seller:dashboard:design_request') }}" class="card_head_nav_item active">My Design Requests</a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-5">
                    <div class="row">
                        @foreach ($offers as $offer)
                        @php $gig = $offer->gig; @endphp
                            <div class="uk-card uk-card-default col-md-6 p-0">
                                <div class="uk-card-header border-bottom-0">
                                    <div class="uk-grid-small uk-flex-middle" uk-grid>
                                        <div class="uk-width-auto">
                                             <img class="uk-border-circle" width="40" height="40" src="{{ $gig->user->photo_url }}">
                                        </div>
                                        <div class="uk-width-expand">
                                            <h3 class="uk-card-title uk-margin-remove-bottom" style="font-size:14px;">{{ title_case($gig->user->username) }}</h3>
                                        <p class="uk-text-meta uk-margin-remove-top"><time datetime="{{ $offer->created_at }}">{{  $offer->created_at->toDayDateTimeString()}}</time></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-card-body p-0">
                                        <div class="gig-tab"> 
                                                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                                    @foreach ($gig->plans as $plan)
                                                        <li class="nav-item">
                                                        <a class="nav-link {{ $loop->first ? 'active' : ''}}" id="gig_tab_ar{{ $plan->id }}" data-toggle="tab" href="#gig_tab{{ $plan->id }}" role="tab" aria-controls="{{ $plan->title }}" aria-selected="true">{{ $plan->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                <div class="tab-content border-left-0 border-right-0 border-bottom-0" id="myTabContent">
                                                    @foreach ($gig->plans as $plan)
                                                    <div class="tab-pane fade {{ $loop->first ? 'show active' : ''}}" id="gig_tab{{ $plan->id }}" role="tabpanel" aria-labelledby="gig_tab_ar{{ $plan->id }}">
                                                        <div class="p-3">
                                                            <div class="tab-gig-price text-primary">&#8358;{{ $plan->price }}</div>
                                                            <div class="tab-gig-desc">{{ title_case($plan->description) }}</div>
                                                            <div class="tab-gig-desc mb-0">
                                                                <ul class="list-unstyled mb-0 mt-1">
                                                                    <li class="d-inline-block"><i class="icon-clock mr-2"></i>Time: {{ $plan->duration }} Days</li>
                                                                <li class="d-inline-block ml-2"><i class="icon-refresh mr-2"></i>Revisions: {{ $plan->revisions }}</li>
                                                                </ul>
                                                            </div>
                                                            <div class="tab-gig-desc mt-2">
                                                                <ul class="list-unstyled">
                                                                    <li class="d-block"><i class="icon-checked mr-2 color-check"></i>Time: {{ $plan->duration }} Days</li>
                                                                    <li class="d-block">
                                                                            @if($plan->is_printable)
                                                                            <i class="icon icon-check mr-2 color-check"></i>
                                                                            @else
                                                                            <i class="icon icon-close mr-2 color-cancel"></i>
                                                                            @endif
                                                                            Print-Ready
                                                                    </li>
                                                                    <li class="d-block">
                                                                        @if($plan->has_source_file)
                                                                        <i class="icon icon-check mr-2 color-check"></i>
                                                                        @else
                                                                        <i class="icon-close mr-2 color-cancel"></i>
                                                                        @endif
                                                                        Source Files</li>
                                                                </ul>
                                                            </div>
                                                            <div class="mt-1">
                                                                <a href="{{ route('app:designer:buy_plan', ['id'=>$plan->id]) }}" class="btn btn-info btn-sm">HIRE</a>
                                                            </div>
            
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                        </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
        </div>
   
</div>


@endsection

 