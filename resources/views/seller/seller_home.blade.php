@extends('layouts.seller')

@section('title', 'Welcome To Seller Portal')

@section('content')

<div class="uk-cover-container" uk-height-viewport>
        <img src="{{ $utils->get_image('site.seller_home_bg') }}" alt="" uk-cover>
        <!--<div class="uk-overlay-primary uk-position-cover"></div>-->
        <div class="uk-section uk-position-center mt-4">
            <div class="row uk-justify-center"> 
                <div class="col-md-12">
                    
                    <div class="uk-text-center">
                    <div class="n-centered-hero">
                        <b style="color: #fff;">MAKE MONEY ON SUVENIA</b> 
                        <p class="mt-1 n-centered-sub-hero" style="color: #fff;">Start your online business today</p>
                    </div>
                    <div class="mt-4"> 
                    <a class="btn-border-whit">GET STARTED</a>
                    </div>
                    </div>
 
                </div>
            </div> 
        </div>

</div>

<div class="borde" style="position: relative; height:100px; background: #fff;">
        <div class="float-slider" style="padding: 20px 60px;" uk-slider>
                <div class="row">
                    <div class="col-12 uk-text-center">
                    <h2 class="m-0 header">Sell your designs on products without hassle!</h2>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >
        
                                <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                                    @foreach($showcase_categories as $category) 
                                        <li class="uk-text-center">
                                            <a href="">
                                                <div class="uk-panel">
                                                    <img src="{{ asset('storage/' . $category->show_case_product) }}" alt="">
                                                
                                                </div>
                                            </a>
                                        <a href="" class="_prod_title">{{ title_case($category->name) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                
                        </div>
                </div>
                <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
                <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
        </div>
</div>


<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <p class="m-0 after_cat_slide_desc">Suvenia is a marketplace that allows you sell your designs and ideas on everyday products like apparels, drinkwares and accessories, at no hassle to you. With quality products waiting for your amazing designs, we handle everything from printing and shipping through to ongoing customer service, making earning easy for you.</p>
                </div>
            </div>
        </div>
</div>

<div class="uk-section bg-app">
<div class="uk-container uk-container-small">
    <div class="bg-banner-header uk-text-center mb-5">
        <h1 class="m-0">HOW IT WORKS</h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-6 col-md-4 uk-text-center mb-2">
            <div class="li-icon-ball m-auto"><span class="icon-enter"></span></div>
            <h1 class="mt-2 mr-0 mb-0 __how_to_pane_header">REGISTER</h1>
            <p class="m-1 __how_to_pane_text">Register by filling short details.</p>
        </div>
        <div class="col-6 col-md-4 uk-text-center mb-2">
            <div class="li-icon-ball m-auto"><span class="icon-listing"></span></div>
            <h1 class="mt-2 mr-0 mb-0 __how_to_pane_header">CREATE AND LIST</h1>
            <p class="m-1 __how_to_pane_text">Design your first product and list to activate your account</p>
        </div>
        <div class="col-6 col-md-4 uk-text-center mb-2">
            <div class="li-icon-ball m-auto"><span class="icon-bulb"></span></div>
            <h1 class="mt-2 mr-0 mb-0 __how_to_pane_header">CUSTOMER BUYS</h1>
            <p class="m-1 __how_to_pane_text">Customer likes your design and buys it.</p>
        </div>
        <div class="col-6 col-md-4 uk-text-center mt-4">
            <div class="li-icon-ball m-auto"><span class="icon-delivery-truck"></span></div>
            <h1 class="mt-2 mr-0 mb-0 __how_to_pane_header">WE PROCESS</h1>
            <p class="m-1 __how_to_pane_text">We produce, package and ship to customers</p>
        </div>
        <div class="col-6 col-md-4 uk-text-center mt-4">
            <div class="li-icon-ball m-auto"><span class="icon-checklist"></span></div>
            <h1 class="mt-2 mr-0 mb-0 __how_to_pane_header">YOU GET PAID</h1>
            <p class="m-1 __how_to_pane_text">Get you earnings paid directly to your preffered bank on all sales made</p>
        </div>

    </div>
</div>
</div>

<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small">
                <div class="app-sec-header sec-center mb-5">
                        <h1 class="title">OUR PRICING SYSTEM</h1>
                        <p class="sub-title">Suvenia gives you total control over how you sell your designs on products. Current Suvenia sellers actually earn an average margin of 17% of the retail price, but whether it is 10% or 30%, you get to decide.</p>
                    </div>
            <div class="row justify-content-center" uk-height-match="target: .card_tag">
                <div class="col-md-3">
                    <div class="card_tag p-3 uk-text-center">
                        <div class="">
                            <img src="{{ asset("img/naira_tag.svg") }}" uk-responsive width="150" class="m-auto">
                        </div>
                        <div class="">
                            <p class="tag_title m-0">BASE PRICE</p>
                            <p class="tag_desc m-0">The base price covers our service and the operational costs involved in making each product</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 borde"><img src="{{ asset("img/plus.svg") }}" uk-responsive width="24" class="uk-position-center"></div>
                <div class="col-md-3">
                    <div class="card_tag p-3 uk-text-center">
                        <div class="">
                            <img src="{{ asset("img/percentage_tag.svg") }}" uk-responsive width="150" class="m-auto">
                        </div>
                        <div class="">
                            <p class="tag_title m-0">YOUR MARGIN</p>
                            <p class="tag_desc m-0">You have complete control when setting the margin on your products. You decide what you will earn from each sale.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 borde"><img src="{{ asset("img/equals.svg") }}" uk-responsive width="24" class="uk-position-center"></div>
                <div class="col-md-3">
                    <div class="card_tag p-3 uk-text-center">
                        <div class="">
                            <img src="{{ asset("img/sale_tag.svg") }}" uk-responsive width="150" class="m-auto">
                        </div>
                        <div class="">
                            <p class="tag_title m-0">RETAIL PRICE</p>
                            <p class="tag_desc m-0">This is the final price your customer sees</p>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
</div>

<div class="uk-section bg-app">
    <div class="uk-container uk-container-small">
            <div class="bg-banner-header uk-text-center mb-5">
                    <h1 class="m-0">FAQ</h1>
                </div>
        <div class="row justify-content-center">
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content1" aria-expanded="false" aria-controls="faq-collapse-content1">
                        <div class="d-flex justify-content-between">
                            <div>How much does it cost to be a seller</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content1">
                    Nothing!.. Just give your life to christ
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content2" aria-expanded="false" aria-controls="faq-collapse-content2">
                        <div class="d-flex justify-content-between">
                            <div>What can I sell?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content2">
                    Nothing! freely yea recieve, freely yea shall give
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content3" aria-expanded="false" aria-controls="faq-collapse-content3">
                        <div class="d-flex justify-content-between">
                            <div>How do I price my service?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content3">
                    Simple! by how many souls you have won for christ
                </div>

            </div>
            <div class="col-md-6 mb-3">
                <button class="btn faq-collapse-btn btn-block" type="button" data-toggle="collapse" data-target="#faq-collapse-content4" aria-expanded="false" aria-controls="faq-collapse-content4">
                        <div class="d-flex justify-content-between">
                            <div>How do I get paid?</div>
                            <div><span class="icon-caret-down ml-2"></span></div>
                        </div>
                </button>
                <div class="collapse p-3 bg-white border-top" id="faq-collapse-content4">
                    Seriously?! Your reward is in heaven
                </div>

            </div>

        </div>
    </div>
</div>

<div class="uk-section">
    <div class="uk-container uk-container-small">
            <div class="grey-bg-header uk-text-center">
                <h1 class="m-0 header-main">SOUNDS INTERESTING?</h1>
                <p class="mt-3 sub-header">Join us to create awesome products for our customers</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <a href="" class="btn btn-info-dark">GET STARTED IT’S FREE</a>
                </div>
            </div>
    </div>
</div>

<div class="uk-section bg-white">
    <div class="uk-container uk-container-small">
            <div class="grey-bg-header uk-text-center">
                    <h1 class="m-0 header-main">HAPPY SELLERS</h1>
            </div>
            <div class="row">
                    <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slider>

                            <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-2@m uk-grid p-4" uk-height-match="target: .uk-card">
                                <li>
                                        <div class="uk-card uk-card-default uk-card-body">
                                                <div class="uk-text-center">
                                                    <img src="{{asset("img/dodo_gang.png")}}" class="m-auto" uk-responsive>
                                                </div>
                                                <p class="happy_cus_text">Suvenia.com is an amazing platform for creating and marketing my products, they always deliver on time, and I have heard positive feedback from customers concerning the quality of the shirts. I recommend Suvenia those out there who are looking for a platform to create and sell their designs.
                                                </p>
                                                <h3 class="uk-card-title m-0 happy_cus_name">--- Dodo Gang</h3>
                                        </div>
                                </li>
                                <li>
                                        <div class="uk-card uk-card-default uk-card-body">
                                                <div class="uk-text-center">
                                                    <img src="{{asset("img/cancer.png")}}" class="m-auto" uk-responsive width="150">
                                                </div>
                                                <p class="happy_cus_text">Breast cancer is the most common female cancer in Nigeria. Every day, about 40 women die from it in the country. It is closely followed by cervical cancer which is almost 100% preventable yet, it accounts for about 26 deaths every day in Nigeria. During our breast campaign we decided to use Suvenia.com to create shirts for awareness purposes.
                                                </p>
                                                <h3 class="uk-card-title m-0 happy_cus_name">--- Cancer Aware</h3>
                                        </div>
                                </li>
                               
                            </ul>
                        
                            <a class="uk-position-center-left uk-position-small uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next"></a>

                        
                        </div>
            </div>
    </div>

</div>

@endsection