@extends('layouts.store_view')
@php 
$store_meta = json_decode($store->meta, true); 
$store_products = isset($store_meta['products']) ? $store_meta['products'] : [] ;
@endphp

@section('content') 
<div class="uk-container uk-container-expand" style="padding:0;">
        <div style="position: relative;">
        <img src="{{ (isset($store_meta['default_banner']) && $store_meta['default_banner'] == 'SYSTEM') ? asset($store_meta['store_banner_system']) : asset('store_img/' . $store_meta['store_upload_banner']) }}" class="uk-width-1-1" alt="" uk-responsive height="400">
        <div class="banner-desc">
        <p>{{ str_limit($store->description, 200, '...') }}</p>
        <p><a href="{{ route('app:base:store_view', ['slug'=> $store->slug]) }}" class="btn btn-secondary">SHOP NOW</a></p>
        </div> 
        
        </div>
    </div>
    
    <div class="uk-container uk-container-small mt-3">
        <div class="row">
            @php $products = \App\Product::whereIn('id', $store_products)->paginate(10); @endphp
            @forelse($products as $product)
            <div class="col-md-3">
                    @include('partials.single_product', ['product'=> $product])
                </div>
            @empty
                <p>You do not have any products</p>
            @endforelse
        </div>
    </div>

@endsection