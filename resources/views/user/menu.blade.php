<li class=" nav-item {{ $utils->isActive(['app:admin:user:index', 'app:admin:user:add', 'app:admin:user:edit', 'app:admin:user:role:index', 'app:admin:user:role:add', 'app:admin:user:role:edit']) ? 'open has-sub': ''}}"><a href="javascript:;"><i class="ft-users"></i><span class="menu-title" data-i18n="">Users</span><span class="badge badge badge-primary badge-pill float-right mr-2">3</span></a>
<ul class="menu-content">
<li class="{{ $utils->isActive(['app:admin:user:index', 'app:admin:user:add', 'app:admin:user:edit']) ? 'active' : ''}}"><a class="menu-item" href="{{ route('app:admin:user:index') }}">User</a>
</li>
<li class="{{ $utils->isActive(['app:admin:user:role:index', 'app:admin:user:role:add', 'app:admin:user:role:edit']) ? 'active' : ''}}"><a class="menu-item" href="{{ route('app:admin:user:role:index') }}">Roles</a>
</li>
</ul>
