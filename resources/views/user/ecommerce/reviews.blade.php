@extends('layouts.layout')

@section('title', 'Review Products' )

@section('content')

<div class="uk-container mt-2 mb-3">
  <div class="row justify-content-center">
    <div class="col-8">
        @includeIf('partials.user_nav')
    </div>
  </div>
  
  <div class="row justify-content-center">
        <div class="col-9">
           
            <table class="uk-table uk-table-small uk-table-divide _saved_item_table mt-4">
                <thead>
                    <tr>
                        <th>REVIEWS</th>
                        <th></th>
                    </tr> 
                </thead>
                <tbody class="bg-white">
                   
                    @foreach($products as $product)
                  
                    <tr class="ml-4 border-bottom">
                        <td>
                                <div class="media">
                                        <img src="https://suvenia.com/p-upl/prod-Xv2BKWXbRY.png" alt="" class="mr-2" width="100" height="130">
                                        <div class="media-body">
                                            <p class="saved_item_prod_name m-0 mb-0 uk-text-capitalize">{{ $product->name }}</p>
                                        <p class="saved_item_prod_seller m-0">{{ $product->order_item->order_ref }}</p>
                                        <div class="mt-1">
                                       
                                        @php $rateValues = [1, 2, 3, 4, 5] ;@endphp
                                                <select class="bar-rated" >
                                                        @foreach ($rateValues as $item)
                                                        <option value="{{ $item }}" {{ $product->avgRating == $item ? 'selected' : ''}}>{{ $item }}</option>
                                                        @endforeach
                                                </select>
                                        </div>
                                        <div class="mt-1">
                                            <p class="m-0 saved_item_prod_name">How do you feel about this product? Tell us what you like or dislike</p>
                                        </div>
                                           
                                        </div>
                                    </div>
    
                        </td>
                       
                        <td>
                                @php $__cat = $product->categories->first(); @endphp
                            <a href="{{ route('app:user:add_product_review', ['id'=> $product->id]) }}" class="btn btn-info btn-sm appReviewProduct">REVIEW</a>
                        </td>
                    </tr>
                   @endforeach
                </tbody>
            </table>
    
        </div>
      </div>


</div>

@endsection

@push('saved_item_page')

<script src="{{ asset('suv/s/pg_reviews.bundle.js') }}"></script>
    
@endpush