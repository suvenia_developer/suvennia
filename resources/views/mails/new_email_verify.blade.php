Welcome! {{$user->username}};
<p>Please follow the link below to verify your new email address: </p>
<a href="{{ route('app:user:verify_email', ['token'=> $user->email_token]) }}">Verify</a>