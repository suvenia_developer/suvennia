Hi! {{$user->username}};
<p>Please follow this link below to reset your password: </p>
<a href="{{ route('app:user:reset_password', ['token'=> $user->password_token]) }}">Reset Password</a>