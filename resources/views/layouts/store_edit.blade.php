<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
   <link rel="stylesheet" href="{{ mix('css/dashboard_vendor.css') }}">
    <link rel="stylesheet" href="{{ mix('css/dashboard_main.css') }}"> 
    @stack('PAGE_STYLES')
    
    <script src="{{ mix('js/dashboard_vendor.js') }}"></script>
    <script src="{{ mix('js/dashboard_main.js') }}"></script>
    @stack('PAGE_SCRIPTS')
 
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">  
    
        <nav class="navbar page-header">
                <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none">
                    <!--<i class="fa fa-bars"></i>-->
                    <i class="icon-menu" style="font-size:20px;"></i>
                </a>
         
                <a class="navbar-brand d-md-down-none" href="{{ route('app:base:index') }}">
                    <img src="{{ $utils->get_image('site.logo') }}" alt="logo" width="100">
                </a>
                <a class="navbar-brand navbar-brand-mobile d-md-none" href="{{ route('app:base:index') }}">
                    <img src="{{ asset('img/logo-icon.png') }}" alt="logo" width="36">
                </a>
        
                <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
                    <i class="icon-menu" style="font-size:20px;"></i>
                </a>
        
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="">
                            <i class="icon-bell" style="font-size: 14px;"></i>
                            @if($utils->get_notifications()->count() > 0)
                            <span class="badge badge-pill badge-danger">{{ $utils->get_notifications()->count() }}</span>
                            @endif
                        </a>
                    </li>
        
                    <li class="nav-item ">
                        <a href="#">
                            <i class="icon-envelope" style="font-size: 14px;"></i>
                            <span class="badge badge-pill badge-danger">5</span>
                        </a>
                        <div uk-dropdown="mode: click" class="p-0">
                            <ul class="list-unstyled">
                                <li class="d-inline-block border-bottom p-3">
                                    <div class="d-flex justify-content-between">
                                        <div >
                                            <i class="icon-envelope" style="font-size:20px;"></i>
                                        </div>
                                        <div class="ml-3">
                                                Set the direction of flex items in a flex container with direction utilities
                                        </div>
                                    </div>
                                </li>
                                <li class="d-inline-block border-bottom p-3">
                                    <div class="d-flex justify-content-between">
                                        <div >
                                            <i class="icon-envelope" style="font-size:20px;"></i>
                                        </div>
                                        <div class="ml-3">
                                                Set the direction of flex items in a flex container with direction utilities
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>
        
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="avatar avatar-sm generalProfilePic" alt="{{ Auth::guard('sellers')->user()->username }}" src="{{ Auth::guard('sellers')->user()->photo_url ? asset(Auth::guard('sellers')->user()->photo_url) : asset('user/default.png') }}" alt="" width="100">
                            <span class="small ml-1 d-md-down-none capitalize">{{ Auth::guard('sellers')->user()->username }}</span>
                        </a>
        
                        <div class="dropdown-menu dropdown-menu-right">
                            @include('partials.acl_menu.seller', ['type'=>'admin_dropdown'])
                            <a href="{{ route('app:seller:logout') }}" class="dropdown-item post-link">
                                    <i class="fa fa-lock"></i> Logout
                            </a>
                        </div>
                    </li>
                    
                </ul>
            </nav> 
        
    <div class="main-container">
        
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-header">
                            <i class="icon icon-layers" style="font-size: 14px;"></i> Store Header
                        </a>
                    </li>
                    <li class="nav-item">
                            <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-banner">
                                <i class="icon icon-picture" style="font-size: 14px;"></i> Store Banner
                            </a>
                        </li>
                   
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-products">
                            <i class="icon icon-handbag" style="font-size: 14px;"></i> My Product
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link active" uk-toggle="target: #offcanvas-store-about">
                            <i class="icon icon-info" style="font-size: 14px;"></i> About
                        </a>
                    </li>
    

                 </ul>
            </nav>
        </div> 

        <div class="content">
            
           @yield('content')
        
        </div>
        
    </div>
</div>

</body>
</html>
