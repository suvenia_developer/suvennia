@extends('layouts.layout')

@section('title', 'Home' )

@section('content')

@if(!$utils->device()->isMobile() || $utils->device()->isTablet())

<div class="uk-cover-container uk-height-larg" style="" uk-height-viewport>
        <img src="{{ asset($utils->get_image('site.hpbanner')) }}" alt="" uk-cover>
        <!---<div class="uk-overlay-primary uk-position-cover"></div>-->
       
        <div class="d-flex justify-content-between uk-position-center">
            <div></div>
            <div class="w-100 p-3 mr-5" style="" uk-scrollspy="cls:uk-animation-scale-up;repeat:true;">
                    <div class="home-hero uk-text-center">
                        <b>Discover</b> amazing designs
                    </div>
                    <div class="home-hero-sub uk-text-center">
                            <p class="m-0">MADE FOR YOU</p>
                        </div> 
                    <div class="mt-2 uk-text-center m-auto" style="width: 70%;"> 
                            <form action="{{ route('app:ecommerce:product_search') }}" method="post" autocomplete="off" class="m-auto">
                                    @csrf
                    <div class="input-group">
                    <input type="search" class="form-control banner-search" placeholder="Find your desired product" name="q" >
                    <div class="input-group-append">
                    <button class="btn btn-info" type="submit" style="width:85px;"><span class="icon-search" ></span></button>
                    </div> 
                    </div> 
                    </form>
                    </div>
            </div>
        </div>

        
</div>


<div class="borde" style="position: relative; height:100px; background: #fff;">
<div class="float-slider" style="padding: 20px 60px;" uk-slider>
        <div class="row">
            <div class="col-12 uk-text-center">
            <h2 class="m-0 header">Everyday products made uniquely for you</h2>
            </div>
        </div>
        <div class="row mt-3">
                <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" >

                        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-5@m uk-grid">
                        @foreach($showcase_categories as $category) 
                            <li class="uk-text-center" uk-scrollspy="cls:uk-animation-fade;repeat:true;">
                                <a href="{{ route('app:ecommerce:product_catlogue', ['category'=> $category->slug]) }}">
                                    <div class="uk-panel">
                                        <img src="{{ asset('storage/' . $category->show_case_product) }}" alt="">
                                    
                                    </div>
                                </a>
                            <a href="{{ route('app:ecommerce:product_catlogue', ['category'=> $category->slug]) }}" class="_prod_title">{{ title_case($category->name) }}</a>
                            </li>
                        @endforeach
                        </ul>
                        
                </div>
        </div>
        <a class="uk-position-center-left uk-position-smal uk-hidden-hove" href="#" uk-slidenav-previous uk-slider-item="previous" style="font-weight: bold;"></a>
        <a class="uk-position-center-right uk-position-smal uk-hidden-hove" href="#" uk-slidenav-next uk-slider-item="next" style="font-weight: bold;"></a>
</div>
</div>


<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small" style="margin-top:100px;">
            <div class="row">
                <div class="col-12">
                    <p class="m-0 after_cat_slide_desc">You are unique, so why shouldn’t everything else about you be? From Apparels to Accessories, Suvenia makes it easy for you to find something made just for you.</p>
                </div>
            </div>
        </div>
</div>
@else
<div class="uk-cover-container">
        <img src="{{ asset($utils->get_image('site.hpbanner')) }}" alt="">
        <div class="uk-section uk-position-center">
            <div class="row uk-justify-center p-2"> 
                <div class="col-md-12">
                    
                    <div class="uk-text-center">
                    <div class="n-centered-her">
                        <b class="text-white" style="font-size:16px;">Discover Amazing Designs</b> 
                        <p class="mt-0 n-centered-sub-hero text-white" style="font-size:13px;">Made For You</p>
                    </div>
                    <div class="m-0"> 
                    @php 
                      $catDefaultLink = $showcase_categories->first()->name; 
                    @endphp
                    <a class="btn btn-outline-light btn-sm rounded-pill" style="font-size:12px;" href="{{ route('app:ecommerce:product_catlogue', ['category'=> $catDefaultLink]) }}" target="_blank">Explore</a>
                    </div>
                    </div>
 
                </div>
            </div>
        </div>
  
</div>

@endif



<div class="uk-section bg-white p-4">
<div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center mb-5">
                <h1 class="title">COLLECTIONS FOR YOU</h1>
            </div>
@if(!$utils->device()->isMobile() || $utils->device()->isTablet())
       <!-- <div class="uk-child-width-1-2@s uk-child-width-1-2">-->
        <div class="row" uk-height-match="target: > div > .uk-card">
            @foreach($banner_grids as $grid)
            @php $faker = \Faker\Factory::create(); @endphp
                <div class="col-12 col-md-6 mb-2">
                <!--<div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle" style="">-->
                <div class="uk-card uk-card-default" style="cursor: pointer;" uk-scrollspy="cls:uk-animation-slide-bottom-medium;repeat:true;" data-link="{{ $grid->link }}">
                            <div class="uk-card uk-card-default">
                                <div class="uk-card-media-top">
                                    <img src="{{ asset($grid->block_key) }}" class="app-lazy" alt="" uk-responsive>
                                </div>
                            <div class="uk-card-body p-1 uk-text-center" style="background: {{ $faker->hexcolor }};">
                                    <p class="cat_banner_home_text">{{ $grid->block_value }}</p>
                                </div>
                            </div>
                    </div>
                </div>
            @endforeach
        </div>
@else
        <div class="uk-position-relative uk-light" tabindex="-1" uk-slider="autoplay: true">

                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@m uk-grid">
                    @foreach($banner_grids as $grid)
                    @php $faker = \Faker\Factory::create(); @endphp
                    <li>
                            <div class="uk-card uk-card-default" style="cursor: pointer;" uk-scrollspy="cls:uk-animation-slide-bottom-medium;repeat:true;" data-link="{{ $grid->link }}">
                                    <div class="uk-card uk-card-default">
                                        <div class="uk-card-media-top">
                                            <img src="{{ asset($grid->block_key) }}" class="app-lazy" alt="" uk-responsive>
                                        </div>
                                    <div class="uk-card-body p-1 uk-text-center" style="background: {{ $faker->hexcolor }};">
                                            <p class="cat_banner_home_text">{{ $grid->block_value }}</p>
                                        </div>
                                    </div>
                            </div>
                    </li>
                    @endforeach
                </ul>
            
                <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover" href="javascript:;" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover" href="javascript:;" uk-slidenav-next uk-slider-item="next"></a>
            
        </div>
@endif
          
</div>
</div>



<div class="uk-margin-to mt-2">
        <div class="uk-container uk-container-smal">
    <a href="" target="_blank">
            <img src="{{ asset('img/home_banner.png') }}" class="uk-width-1-1" alt="" uk-responsive>
        </a> 
    </div>
</div>

<div class="uk-section bg-white p-4">
<div class="uk-container uk-container-small">
    <div class="row">
        @foreach($showcase_categories as $category)
        <div class="col-md-3 col-6 uk-text-center">
        </div>
        @endforeach
    </div>
</div>
</div>



 
<div class="uk-section bg-white p-4">
    @php $_in_season = \App\Product::with(['user','categories'=>function($q){
        $q->where('parent_id', '!=', null);
    }, 'photos'])->get(); @endphp

    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center">
            <h1 class="title">recommended for you</h1>
            <!--<div class="underline"></div>-->
        </div>

        <div class="row mt-4">

            @foreach($_in_season->take(4) as $product)
            
            <div class="col-md-3">
                @include('partials.single_product', ['product'=> $product])
            </div>
            @endforeach

        </div>

    </div>

</div>

<div class="uk-section bg-white p-4">
    @php $_in_season = \App\Product::with(['user','categories'=>function($q){
        $q->where('parent_id', '!=', null);
    }, 'photos'])->get(); @endphp

    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center">
            <h1 class="title">based on trends</h1>
            <!--<div class="underline"></div>-->
        </div> 

        <div class="row mt-4">

            @foreach($_in_season->take(4) as $product)
            <div class="col-md-3">
                @include('partials.single_product', ['product'=> $product])
            </div>
            @endforeach

        </div>

    </div>

</div>


<div class="uk-section bg-white p-4">
    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center mb-3">
            <h1 class="title">more for you</h1>
            <!--<div class="underline"></div>-->
            <div style="height:5px"></div>
        </div>

        
                 <div uk-slideshow="animation: push;min-height: 100; max-height: 80">

                    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                
                        <ul class="uk-slideshow-items">
                            <li>
                                <img src="{{ asset('img/aa.png') }}" alt="">
                            </li>
                            <li>
                                <img src="{{ asset('img/aa.png') }}" alt="">
                            </li>
                        </ul>
                
                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                
                    </div>
                
                    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
                
                </div>

    </div>

</div>

<div class="uk-section bg-white p-4">
    <div class="uk-container uk-container-small">
        <div class="app-sec-header sec-center mb-3">
            <h1 class="title">stores we like</h1>
            <!--<div class="underline"></div>-->
            <div style="height:5px"></div>
        </div>

        <div class="row mt-4">
                <div class="uk-position-relative uk-light" tabindex="-1" uk-slider="autoplay: true">

                        <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@m uk-grid">
                        @for($i=0; $i < 10; $i++)
                            <li>
                                    <div class="uk-card uk-card-default">
                                            <div class="uk-card-media-top">
                                                <a href="">
                                                <img src="{{ asset('img/dummy_store.png') }}" alt="">
                                                </a>
                                            </div>
                                            <div class="uk-card-body p-2">
                                                    <div class="d-flex justify-content-between">
                                                        <div>
                                                                <img src="{{ asset('img/store_logo.png') }}" alt="" width="64">
                                                        </div>
                                                        <div>
                                                            <a href="" class="card__links">
                                                            <p class="mb-0 mt-0 ml-2 store_card_title">Flo Wears</p>
                                                            <p class="ml-2 mt-0 store_text_content">Flowears Lorem ipsum dolor sit amet.</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                            </li>
                        @endfor
                            
                        </ul>
                    
                        <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    
                    </div>
        </div>

    </div>

</div>



<div class="uk-section bg-white p-4">
        <div class="uk-container uk-container-small">
            <div class="app-sec-header sec-center mb-3">
                <h1 class="title">our promise to you</h1>
                <!--<div class="underline"></div>-->
                <div style="height:5px"></div>
            </div>
    
            <div class="row mt-4">
    
                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-delivery-truck"></span>
                                <div class="app-card-icon-head">FAST SHIPPING</div>
                                <small>We produce every item on-demand and quickly</small>
                        </div>
                </div>
                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-five-stars"></span>
                                <div class="app-card-icon-head">100% SATISFACTION</div>
                                <small>Exchange or money back guarantee for all orders</small>
                        </div>
                </div>
                <div class="col-md-4">
                        <div class="card app-info-card transparent">
                                <p class="m-1"><span class="card-info-icon icon-shield"></span>
                                <div class="app-card-icon-head">SECURE PAYMENT</div>
                                <small>Secure online shopping and payment.</small>
                        </div>
                </div>
    
            </div>
    
        </div>
    
</div>


<div class="uk-section bg-grey p-4">
        <div class="uk-container uk-container-small">
            <div class="div mt-4 uk-text-center">
            <p class="m-0 __subscribe_title_text">Subscribe to our newsletter now, get all news from our awesome website.</p>
            </div>
            <div class="row mt-4 justify-content-center">
            <div class="col-md-7">
                <form class="app-form">
                <div class="input-group mb-3">
                <input type="email" class="form-control app-input rounded-0" placeholder="Enter Your Email Address" aria-label="Subscribe" aria-describedby="Subscribe">
                <div class="input-group-append">
                <button class="btn btn-info-dark" type="button" id="button-addon2">SUBSCRIBE</button>
                </div>
                </div>
            </form>
                          
            </div>
            </div>
    
        </div>
    
    </div>



@endsection