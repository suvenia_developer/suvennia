<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerGig extends Model
{
    public function user()
    {
        return $this->belongsTo("App\Designer");
    }

    public function plans()
    {
        return $this->hasMany("App\DesignerPlan", "gig_id");
    }

    public function category()
    {
        return $this->belongsTo("App\DesignerCategory", "category_id");
    }

    public function orders()
    {
        return $this->hasMany("App\DesignerOrder", "gig_id");
    }

    public function photos()
    {
        return $this->hasMany("App\DesignerAttachment", "gig_id");
    }

}
