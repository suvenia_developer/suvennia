<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerNetwork extends Model
{
    protected $table = 'influencer_socials';
}
