<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerPost extends Model
{
    public function product(){
        return $this->belongsTo("App\Product", "product_id");
    }

    public function user(){
        return $this->belongsTo("App\Seller", "user_id");
    }
}
