<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Influencer extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'influencers';

    public function socials(){
        return $this->hasMany("App\InfluencerSocial");
    }
} 
