<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerRequest extends Model
{
    public function seller()
    {
        return $this->belongsTo("App\Seller", "user_id");
    }

    public function gig() 
    {
        return $this->belongsTo("App\DesignerGig", "category_id");
    }

    public function offers() 
    {
        return $this->hasMany("App\DesignerOffer", "request_id");
    }
    
}
