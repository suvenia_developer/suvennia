<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;

class OrderDetails extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $order_info = Order::with(['order_items' => function($q){
            $q->with('product')->get();
        }, 'order_shipping'])->where('id', $order->id )->first();

        $this->order = $order_info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(setting('site.email'), setting('site.title') . ' ' . setting('site.description'))
        ->subject(setting('site.title') . ' - Order Summary')
             ->view('mails.order_summary');
    }
}
