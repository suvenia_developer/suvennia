<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerOrder extends Model
{
    public function customer(){
        return $this->belongsTo("App\Seller", "customer_id");
    }

    public function gig(){
        return $this->belongsTo("App\InfluencerGig", "gig_id");
    }
}
