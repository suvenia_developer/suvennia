<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $table = 'eco_payments';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function order(){
        return $this->hasOne("App\Order");
    }
public function order_metric(){
        return $this->hasOne("App\OrderMetric");
    }
}
