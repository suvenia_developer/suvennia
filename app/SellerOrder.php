<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerOrder extends Model
{

    protected $table = 'eco_seller_orders';

    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function order()
    {
        return $this->hasOne("App\Order");
    }
}
