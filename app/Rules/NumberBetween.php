<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumberBetween implements Rule
{
	
	public $min = 0;
	
	public $max = 0;
	
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($min, $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $filter = filter_var(intval($value), FILTER_VALIDATE_INT, ['options'=> ['min_range'=> $this->min, 'max_range'=> $this->max]]);
		if(!$filter){
			return false;
		}
		return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please enter an amount between ' . $this->min . ' and ' . $this->max;
    }
}
