<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'eco_orders';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function order_items(){
        return $this->hasMany("App\OrderItem");
    }
    public function shipping(){
        return $this->hasMany("App\ShippingAddress");
    }
    public function payment(){
        return $this->hasOne("App\Payment");
    }

    public function order_shipping(){
        return $this->hasOne("App\OrderShipping");
    }

    public function order_metrics(){
        return $this->hasMany("App\OrderMetric");
    }

}
