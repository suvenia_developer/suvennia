<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerCategory extends Model
{
    public function gigs()
    {
        return $this->hasMany("App\DesignerGig", "category_id");
    }
}
