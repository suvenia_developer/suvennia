<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingZone extends Model
{

    protected $table = 'eco_shipping_zones';

    public function locations(){
        return $this->hasMany("App\ShippingLocation");
    }

    public function addresses(){
        return $this->hasMany("App\ShippingAddress", "zone_id");
    }

}
