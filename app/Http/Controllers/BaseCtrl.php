<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory as Category;
use DB;

class BaseCtrl extends Controller{

    public function index(Request $request){
       //// $url = parse_url('mysql://b7680eb9dd827b:eb42f85b@us-cdbr-iron-east-03.cleardb.net/heroku_724285d14f5f0ea?reconnect=true');
       
        $categories = Category::with(['children'=> function($q){
            $q->with('children');
        }])->where('parent_id', 0)->get();

        $showcase_categories = Category::where('is_sub', true)->get();
        $banner_grids = DB::table('display_block')->where('block_code', 'ID:MASONRY')->get();

        return view('shop.index', [
            'categories'=> $categories, 
            'showcase_categories'=> $showcase_categories,
            'banner_grids'=> $banner_grids
            ]);
    } 
 
    public function seeder(Request $request){
        /*
        $block = DB::table('display_block')->insert([
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/1.png',
                'block_value'=> 'Birthday Celebration',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/2.png',
                'block_value'=> 'Coloful Phone Cases',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/3.png',
                'block_value'=> 'Fashion',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/4.png',
                'block_value'=> 'Game Of Thrones',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/5.png',
                'block_value'=> 'Fun  Throw Pillows',
            ],
            [
                'block_code'=> 'ID:MASONRY',
                'block_key'=> 'storage/product-categories/img/6.png',
                'block_value'=> 'Foodie',
            ],
        ]);
        */
    }
}