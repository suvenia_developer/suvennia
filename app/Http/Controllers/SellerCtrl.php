<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory as Category;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Repo\User\Auth as AuthGuard;

class SellerCtrl extends Controller
{
    public $user; 

    public function __construct(){
        $this->user = new AuthGuard('@SELLER');
    }

    public function seller_home(){
        $showcase_categories = Category::where('is_sub', true)->get();
        return view('seller.seller_home', [
            'showcase_categories'=> $showcase_categories,
        ]); 
    } 

    public function login(Request $request){
       
        if ($request->isMethod('post')){
           $login = $this->user->login(); 
           $login->enter();
           if($login->status){
               return redirect()->route('app:seller:dashboard:index')->with('success', $login->message);
           }else{
               return back()->withInput()->with('error', $login->message);
           }
        }
        return view('seller.login');
    }

    public function logout(Request $request){
        if ($request->isMethod('post')){
            $this->user->logout('app:seller:index');
            return redirect()->route('app:seller:index')->with('success', 'You have logged out successfully!');
         }
    }

}
 