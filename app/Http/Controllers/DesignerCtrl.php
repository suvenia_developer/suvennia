<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Designer;
use App\DesignerGig as Gig;
use App\DesignerPlan as Plan;
use App\DesignerOrder as Order;
use App\Payment as Payment;
use App\DesignerRequest as _Request;
use App\DesignerCategory as Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Mail\VerifyEmail;
use App\Repo\User\Auth as AuthGuard;
use Illuminate\Validation\Rule;

class DesignerCtrl extends Controller
{

    public $user; 

    public function __construct(){
        $this->user = new AuthGuard('@DESIGNER');
    } 

    public function landing_page(Request $request){
        $categories = Category::get();
        return view('designer.landing_page', ['categories'=> $categories]);
    }

    public function landing_page_hire(Request $request){
        $categories = Category::get();
        return view('designer.landing_page_hire', ['categories'=> $categories]);
    }

    public function sign_up(Request $request){
        if ($request->isMethod('POST')) {
            $data = $request->all();
            $messages = [
                'tos.required' => 'You must agree to our terms and conditions',
            ]; 
            $validator = Validator::make($data, [
                'username' => 'required|max:255|min:4|unique:designers',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
                'email' => 'required|min:4|email|unique:designers',
                'portfolio_link' => 'required|url',
                'tos' => 'required'
            ], $messages);
            
            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();
                

            } else {
                $designer = new Designer();
                $data = $request->all();

                $designer->username = $data['username'];
                $designer->ref = str_random(10);
                $designer->slug = str_slug($data['username'], '-');
                $designer->email = $data['email'];
                $designer->password = bcrypt($data['password']);
                $designer->has_password = true;
                $designer->email_token = str_random(36);
                $designer->email_token_expiry = \Carbon\Carbon::now()->addDay(1);
                
                if ($designer->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($designer->email)->later($when, new VerifyEmail($designer, 'DESIGNER'));
                    return redirect()->route('app:designer:landing_page', ['pid'=> encrypt($designer->ref)]);
                }

            }

        }
        return view('designer.sign_up', []);
    }

    public function login(Request $request){
       
        if ($request->isMethod('post')){
           $login = $this->user->login();
           $login->enter();
           if($login->status){
               return redirect()->route('app:designer:dashboard:index')->with('success', $login->message);
           }else{
               return back()->withInput()->with('error', $login->message);
           }
        }
        return view('designer.login');
    } 
 
    public function logout(Request $request){
        if ($request->isMethod('post')){
            $this->user->logout('app:designer:landing_page');
            return redirect()->route('app:designer:landing_page')->with('success', 'You have logged out successfully!');
         }
    }

    public function search_services_post(Request $request){
        if($request->isMethod('POST')){
            return redirect()->route('app:designer:search', ['q'=> $request->q]);
        }
    }

    public function search_services(Request $request){
        $sort_gigs = $this->get_sorted_gigs_by_duration($request->dur);
        $category = Category::where('title', 'like', $request->q . '%')->with(['gigs'=> function($q)use($sort_gigs){
            $q->whereIn('id',$sort_gigs)->with(['plans'=>function($q){
                $q->orderBy('price')->first();
            }, 'photos']);
        }])->first();

        if(!$category){
            $category = Category::with(['gigs'=> function($q)use($sort_gigs){
                $q->whereIn('id',$sort_gigs)->with(['plans'=>function($q){
                    $q->orderBy('price')->first();
                }, 'photos']);
            }])->first();
        }

        $categories = Category::with(['gigs'])->get();
        $params = $category->slug . '-' . $category->ref;
        return view('designer.catalogue', ['categories'=> $categories, 'category'=> $category, 'cat_ref'=> $category->ref, 'currentUrlParam'=> $params]);
    }

    public function catalogue(Request $request, $params){
        $Sortref = explode('-', $params);
        $sort_gigs = $this->get_sorted_gigs_by_duration($request->dur);
        $category = Category::where('ref', end($Sortref))->with(['gigs'=> function($q)use($sort_gigs){
            $q->whereIn('id',$sort_gigs)->with(['plans'=>function($q){
                $q->orderBy('price')->first();
            }, 'photos']);
        }])->firstOrFail();
        $categories = Category::with(['gigs'])->get();
       return view('designer.catalogue', ['categories'=> $categories, 'category'=> $category, 'cat_ref'=> end($Sortref), 'currentUrlParam'=> $params]);
    } 

    public function get_sorted_gigs_by_duration($duration){
        if(!$duration || $duration < 1){
            $plans = \App\DesignerPlan::get();
        }else{
            $plans = \App\DesignerPlan::where('duration', '<=', $duration)->get();
        }
         
        return $plans->pluck('gig_id');
    }
   
    public function view_gig(Request $request, $ref){
        $gig = Gig::with('category')->where('ref', $ref)->first();
        $categories = Category::with(['gigs'])->get();
        $relatedGigs = Category::with(['gigs'=>function($q)use($gig){
            $q->whereNotIn('id', [$gig->id])->with(['plans'=>function($q){
                $q->orderBy('price')->first();
            }, 'photos']);
        }])->where('id', $gig->category_id)->first();

        $defaults = Gig::with(['plans'=>function($q){
            $q->orderBy('price')->first();
        }, 'photos'])->limit(15)->get();

        return view('designer.public_profile', ['gig' => $gig, 'related'=> $relatedGigs, 'defaults'=> $defaults, 'categories'=> $categories]);
    }

    public function buy_plan(Request $request, $id){
        $plan = Plan::with(['gig'=> function($q){
            $q->with(['photos', 'user']);
        }])->where('id', $id)->firstOrFail();
        return view('designer.buy_plan', ['plan'=> $plan]);
    }

    public function confirm_payment(Request $request, $param){
        $plan = Plan::with(['gig'=> function($q){
            $q->with(['photos', 'user']);
        }])->where('id', $param)->firstOrFail();
        return view('designer.confirm_payment', ['plan'=> $plan]);
    }

    public function process_payment(Request $request){
        if($request->isMethod('post')){
            $plan = Plan::where('id', $request->plan)->firstOrFail();
            $order_id = 'SUV-' . str_random(10);
            $role = $request->user_type == 'BUYER' ? '' : 'SELLER';

            $paystack = new \Yabacon\Paystack(config('designer_config.paystack_code'));
            try
                {
                    $tranx = $paystack->transaction->initialize([
                        'amount' => $plan->price * 100, // in kobo
                        'email' => Auth::guard($role)->user()->email, // unique to customers
                        'reference' => $order_id, // unique to transactions
                    ]);
                } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                    die($e->getMessage());
                }

            $order = new Order();
            $order->user_id = Auth::guard($role)->user()->id;
            $order->user_type = strtoupper($request->user_type);
            $order->gig_id = $plan->gig_id;
            $order->designer_id = $plan->user_id;
            $order->plan_id = $plan->id;
            $order->payment_id = 0;
            $order->amount = $plan->price;
            $order->reference = $order_id;
            if($order->save()){
                $payment = new Payment();
                $payment->user_id = Auth::guard($role)->user()->id;
                $payment->user_type = strtoupper($request->user_type);
                $payment->order_id = $order->id;
                $payment->amount = $plan->price;
                $payment->gateway = 'PAYSTACK';
                $payment->reference = $order_id;
                $payment->type = 'DESIGNER';
                $payment->save();
                return redirect($tranx->data->authorization_url);
            }
  
        }
    } 

    public function payment_callback(Request $request){
        if ($request->reference) {
            $paystack = new \Yabacon\Paystack(config('designer_config.paystack_code'));
            try {

                $tranx = $paystack->transaction->verify([
                    'reference' => $request->reference, // unique to transactions
                ]);
                if ('success' === $tranx->data->status) {
                    $payment = Payment::where('reference', $request->reference)->first();
                    $payment->reference = $request->reference;
                    $payment->status = 2;
                    $payment->save();
                    
                    $order = Order::where('reference', $request->reference)->first();
                    $order->payment_id = $payment->id;
                    $order->status = 2;
                    $order->save();
                    return redirect()->route('app:designer:complete_order', ['pid' => encrypt($order->reference)])->with('success', 'Your payment was successful! You may now provide additional information for you order.');
                }
            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                // return redirect()->route('shop:cart_payment_error');
            }
        }
    }

    public function complete_order(Request $request){
        if(!$request->pid){
            return redirect()->route('app:designer:landing_page_hire')->with('success', 'Invalid link');
        }

        $order = Order::where('reference', decrypt($request->pid))->firstOrFail();
        

        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255|min:4',
                'description' => 'required|min:20',
                'request_file' => 'file|mimes:jpeg,bmp,png,jpg',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=> $validator->errors()], 500);
            }else{

                $drequest = new _Request();
                $order = Order::with('gig')->where('reference', decrypt($request->pid))->first();
                $plan = Plan::where('id', $order->plan_id)->first(); 
                $drequest->user_id = $order->user_id;
                $drequest->user_type = strtoupper($order->user_type);
                $drequest->gig_id = $plan->gig_id;
                $drequest->plan_id = $plan->id;
                $drequest->title = $request->title;
                $drequest->category_id = $plan->gig->category_id;
                $drequest->duration = intval($plan->duration);
                $drequest->budget = $plan->price;
                $drequest->description = $request->description;
                $drequest->deadline = \Carbon\Carbon::now()->addDays(intval($plan->duration));

                if ($request->hasFile('request_file') && $request->file('request_file')->isValid()) {
                    if(Storage::exists($drequest->image_path)){
                        Storage::delete($drequest->image_path);
                    }
                    $image_path = Storage::putFile('designer_requests', $request->file('request_file'));
                    $drequest->photo_url = Storage::url($image_path);
                    $drequest->image_path = $image_path;
                }


                $drequest->save();
                
                $order->request_id = $drequest->id;
                $order->save();

                return response()->json(['message'=> 'Your Request has been submitted!', 'redirect_url'=> route('app:designer:order_notify', ['pid'=> $order->reference])], 200);

            }
            
        }
        return view('designer.complete_order', []);
    }

    public function order_notify(Request $request){
        if(!$request->pid){
            return redirect()->route('app:designer:landing_page_hire')->with('success', 'Invalid link');
        }
        return view('designer.order_notify', ['order'=> $request->pid]);
    }

    public function post_request(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'title' => 'required|max:255|min:4',
                'description' => 'required|min:20',
                'duration' => 'required|numeric',
                'request_file' => 'file|mimes:jpeg,bmp,png,jpg',
                'budget' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=> $validator->errors()], 500);
            }else{
                $role = $request->user_type == 'BUYER' ? '' : 'sellers';
                
                $drequest = new _Request();
                $drequest->user_id = Auth::guard($role)->user()->id;
                $drequest->user_type = strtoupper($request->user_type);
                $drequest->title = $request->title;
                $drequest->budget = $request->budget;
                $drequest->category_id = $request->category;
                $drequest->duration = intval($request->duration);
                $drequest->description = $request->description;
                $drequest->deadline = \Carbon\Carbon::now()->addDays(intval($request->duration));

                if ($request->hasFile('request_file') && $request->file('request_file')->isValid()) {
                    if(Storage::exists($drequest->image_path)){
                        Storage::delete($drequest->image_path);
                    }
                    $image_path = Storage::putFile('designer_requests', $request->file('request_file'));
                    $drequest->photo_url = Storage::url($image_path);
                    $drequest->image_path = $image_path;
                }

                $drequest->save();
                return response()->json(['message'=> 'Your Request has been submitted!', 'redirect_url'=> route('app:designer:post_request')], 200);

            }
        }
        $categories = Category::get();
        return view('designer.post_request', ['categories'=> $categories]);
    }

}
