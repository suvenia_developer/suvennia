<?php
namespace App\Http\Controllers\DashboardTraits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

use App\DesignerCategory as Category;
use App\DesignerGig as Gig;
use App\DesignerPlan as Plan;
use App\Helpers\Utils as Utils;
use App\DesignerAttachment;
use App\Chat;

Trait DesignerTrait{
    
    public function designer_all_gigs(Request $request){
        $gigs = Gig::where('user_id', Auth::user()->id)->paginate(20); 
        return view('designer.dashboard.gigs.all_gigs', ['gigs'=> $gigs]);
    }

    public function designer_add_gig(Request $request){
        $categories = Category::get();
        if($request->isMethod('POST')){
            $gig = new Gig();
            $gig->title = $request->gig_title;
            $gig->ref = str_random(10);
            $gig->user_id = Auth::user()->id; 
            $gig->category_id = $request->gig_category;
            $gig->service_type = $request->service_type;
            $gig->tags = $request->tags;
            $gig->description = $request->gig_descripton;
            //$gig->description = 'hello';
            $gig->reqirements = $request->gig_requirement;
            //$gig->reqirements = 'req';
            $gig->save();

            foreach($request->plans as $plan){
                $planIns = new Plan();
                $planIns->gig_id = $gig->id;
                $planIns->user_id = Auth::user()->id;
                $planIns->name = $plan['plan_name'];
                $planIns->description = $plan['plan_description'];
                $planIns->revisions = $plan['plan_revisions'];
                $planIns->design_concepts = $plan['plan_design_concepts'];
                $planIns->has_source_file = isset($plan['source_file']) ? true : false;
                //$planIns->enables_commercial_use = $plan['enables_commercial_use'] ? true : false;
                $planIns->enables_commercial_use = false;
                $planIns->duration = $plan['delivery_time'];
                $planIns->price = $plan['price'];
                $planIns->save();

            }
            foreach($request->app_files as $file){
                $utils  = new Utils();
                $upload = $utils->full_base64_upload($file, str_random(10), $gig->ref, 'designer/gigs');
                $att = new DesignerAttachment();
                $att->user_id = Auth::user()->id;
                $att->gig_id = $gig->id;
                $att->path = $upload['path'];
                $att->public_url = $upload['public_url'];
                $att->save();
            }

            return response()->json(['message'=> 'Gig created successfully!', 'redirect_url'=> route('app:dashboard:designer:all_gigs')], 200);
        }
        return view('designer.dashboard.gigs.new_gig', ['categories'=> $categories]);
    }
 
    public function designer_edit_gig(Request $request, $id){

        return view('designer.dashboard.gigs.edit_gig');
    }

    public function designer_delete_gigs(Request $request){
    }

    public function designer_all_orders(Request $request){
 
        return view('designer.dashboard.orders.all_orders');
    }

    public function designer_view_order(Request $request, $id){

        return view('designer.dashboard.orders.view_order');
    } 

    public function designer_delete_orders(Request $request){
    }  

    public function upload_gallery(Request $request){

    }

    public function designer_order_send_message(Request $request){
        if($request->isMethod('POST')){
            $chat = new Chat();
            $chat->sender = $request->sender;
            $chat->reciever = $request->reciever;
            $chat->department = 'DESIGNER';
            $chat->model_id = 2;
            $chat->content = $request->message;
            $chat->type = 'TEXT';
            $chat->save();
            return response()->json(['message'=> $chat->content], 200);
        }
    }

    public function designer_order_send_attachment(Request $request){

    }

}