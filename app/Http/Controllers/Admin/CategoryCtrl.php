<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductCategory as Category;

class CategoryCtrl extends Controller
{
    public $view_path = 'vendor.voyager.product-categories.';

    public function index(Request $request){
     
      $categories = Category::get();
      return view($this->view_path . 'index', ['categories'=> $categories]);
    }

    public function add(Request $request){
        
    }

    public function edit(Request $request, $id){

    }

    public function delete(Request $request, $id){

    }


}
