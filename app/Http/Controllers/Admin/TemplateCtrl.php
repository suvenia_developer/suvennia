<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Utils as Utils;
use App\ProductCategory as Category;
use App\Template as Template;

class TemplateCtrl extends Controller
{
    public function index(Request $request){
        $templates = Template::paginate(12);
        return view('vendor.voyager.templates.index', ['templates'=> $templates]);
    } 

    public function add(Request $request){
        if ($request->isMethod('post')) {
           // print_r($request->all());
           $utils = new Utils();
           $template = new Template();
           $template->data = $request->data;
           $template->photo_url = $utils->base64_img($request->photo_url, 'template', 'design_templates');
           $template->save();
           $red = action([TemplateCtrl::class, 'index']);
           return redirect($red);
        }
        return view('vendor.voyager.templates.add', []);
    }

    public function edit(Request $request, $id){
        $template = Template::where('id', $id)->first();
        if ($request->isMethod('post')) {
            // print_r($request->all());
            $old_photo = basename($template->photo_url);
            $link_photo = public_path('design_templates/') . $old_photo;
            if (file_exists(public_path('design_templates/') . $old_photo)) {
                unlink($link_photo);
            }
            $utils = new Utils();
            $template = Template::where('id', $id)->first();
            $template->data = $request->data;
            $template->photo_url = $utils->base64_img($request->photo_url, 'template', 'design_templates');
            $template->save();
            $red = action([TemplateCtrl::class, 'index']);
           return redirect($red);
         }
        return view('vendor.voyager.templates.edit', ['template'=> $template]);
    } 
    
    public function delete(Request $request, $id){
        $template = Template::where('id', $id)->first();
            $old_photo_front = basename($template->photo_url);
            $link_front = public_path('design_templates/') . $old_photo_front;
            if (file_exists(public_path('design_templates/') . $old_photo_front)) {
                unlink($link_front);
            }
            $template->delete();
        return back();
    }

}
