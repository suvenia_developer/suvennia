<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\PasswordExists;
use App\Rules\AmountEquals; 
use App\Rules\NumberBetween;
use App\Helpers\Utils; 
use Illuminate\Validation\Rule;
use App\Influencer;
use App\InfluencerPost as Post;
use App\InfluencerOrder as Order; 
use App\InfluencerGig as Gig;
use App\InfluencerOffer as InfluencerOffer;
use App\InfluencerWithdrawal as Withdraw;
use Auth;
use Validator;
use Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class InfluencerCtrl extends Controller
{
    public function overview(Request $request){

        $month_back = [6,5,4,3,2,1,0];
        $month_names = []; 
        $month_stats = [];
        $carbon = \Carbon\Carbon::now();
        foreach($month_back as $month){
            $month_names[] = $carbon->copy()->startOfMonth()->subMonths($month)->format('F');
            $month_stats[] = Order::whereMonth('created_at',$carbon->copy()->startOfMonth()->subMonths($month)->format('m'))->where([['influencer_id', Auth::guard('influencers')->user()->id]])->count();
        }

        $dimers = [
            [
                'icon'=> 'img/influencer/box.png',
                'title'=> 'total posts',
                'value'=> Post::count()
            ],
            [
                'icon'=> 'img/influencer/withdraw.png',
                'title'=> 'Life Balance',
                'value'=> Order::where('status', 2)->sum('amount')
            ]
        ];

        $seller_orders = Order::where('influencer_id', Auth::guard('influencers')->user()->id)->get();

        return view('influencer.dashboard.overview', ['dimers'=> json_decode(json_encode($dimers), false), 'month_names'=> $month_names, 'month_stats'=> $month_stats, 'seller_orders'=> $seller_orders]);
    }

    public function gigs(Request $request){
        $fetchIDS = Order::where('status', 2)->get();
        $offers = \App\Order::where('user_id', Auth::guard('influencers')->user()->id)->get();

        $gigs = Gig::with(['product'=>function($q){
            $q->with('photos');
        }])->where('is_closed', false)->whereIn('id', $fetchIDS->pluck('gig_id'))->OrWhereNotIn('influencer_id', $offers->pluck('user_id'))->paginate(15);
       return view('influencer.dashboard.gigs', ['gigs'=> $gigs]);
    }

    public function gig_modal(Request $request, $id){
        $gig = Gig::with(['product'=>function($q){
            $q->with('photos');
        }])->where('id', $id)->first();
        return View::make('influencer.dashboard.gig_modal', ['gig'=> $gig]);
    }

    public function accept_gig(Request $request, $id){
        if($request->isMethod('POST')){
            $offer = new InfluencerOffer();
            $offer->user_id = Auth::guard('influencers')->user()->id;
            $offer->gig_id = $id;
            $offer->response_type = 'ACCEPT';
            $offer->save();
            $gig = Gig::where('id', $id)->where('id', $id)->first();
            $gig->is_closed = true;
            $gig->influencer_id = Auth::guard('influencers')->user()->id;
            $gig->closed_at = \Carbon\Carbon::now();
            $gig->save();
            return back()->with('success', 'This Gig has been added to your Orders.');
        }
    }

    public function reject_gig(Request $request, $id){
        if($request->isMethod('POST')){
            $offer = InfluencerOffer();
            $offer->user_id = Auth::guard('influencers')->user()->id;
            $offer->gig_id = $id;
            $offer->response_type = 'REJECT';
            $offer->save();
            return back()->with('success', 'You have rejected this Job successfully!');
        }
    }

    public function my_posts(Request $request){
        $posts = Post::with(['product'=>function($q){
            $q->with('photos'); 
        }, 'user'])->where('influencer_id', Auth::guard('influencers')->user()->id)->get();
        return view('influencer.dashboard.my_posts', ['posts'=> $posts]);
    }

    public function my_orders(Request $request){
        $orders = Order::with(['gig'=> function($q){
            $q->with(['product'=>function($qq){
                $qq->with('photos');
            }, 'user'])->get();
        }])->where('influencer_id', Auth::guard('influencers')->user()->id)->get();
        
        return view('influencer.dashboard.my_orders', ['orders'=> $orders]);
    }

    public function withdraw(Request $request){
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                 'amount' => ['required', new NumberBetween(1000, 50000), new AmountEquals(Auth::guard('influencers')->user()->balance)]
             ]);
             if ($validator->fails()) {
                 return response()->json(['errors'=> $validator->errors()], 500);
             }else{
                 $withdraw = new Withdraw();
                 $withdraw->user_id = Auth::guard('influencers')->user()->id;
                 $withdraw->code = Auth::guard('influencers')->user()->transfer_code;
                 $withdraw->amount = $request->amount;
                 $withdraw->ref = 'SUV-' . str_random(20);
                 $withdraw->status = 1;
                 $withdraw->save();
                 $userQuery = \App\Influencer::where('id', Auth::guard('influencers')->user()->id)->first(); 
                 $userQuery->decrement('balance', $request->amount);
                 
                 return response()->json(['message'=> 'Your request has been recieved! After validation, your payment would be made within 5 working days.', 'redirect_url'=> route('app:influencer:dashboard:withdraw')], 200);
             }
        }
        $dimers = [
            [
                'title'=> 'Current Balance',
                'value'=> Auth::guard('influencers')->user()->balance
            ],
            [
                'title'=> 'Lifetime Balance',
                'value'=> Order::where([['influencer_id', Auth::guard('influencers')->user()->id], ['status', true]])->sum('amount')
            ],
        ];
        $withdrawals = Withdraw::where('user_id', Auth::guard('influencers')->user()->id)->paginate(10);

        return view('influencer.dashboard.withdraw', ['withdrawals'=> $withdrawals, 'dimers'=> json_decode(json_encode($dimers), false)]);
    }

    public function profile_settings(Request $request){
        if($request->isMethod('post')){
            $user = Influencer::where('id', Auth::guard('influencers')->user()->id)->first();
    
            $validator = Validator::make($request->all(), [
                'username' => 'required|max:255|min:4|unique:influencers,username,' . $user->id,
                'firstname' => 'max:255|min:2',
                'lastname' => 'max:255|min:2',
                'phone' => 'digits:11',
                'instagram' => 'nullable|url',
                'twitter' => 'nullable|url',
                'bio' => 'max:300|min:20',
                'image' => 'image',
            ]);
    
            if ($validator->fails()) {
    
                return response()->json(['errors'=> $validator->errors()], 500);
    
            } else {
                $influencer = Influencer::where('id', Auth::guard('influencers')->user()->id)->first();
                $influencer->username = $request->username;
                $influencer->slug = str_slug($request->username, '-');
                $influencer->phone = $request->phone;
                $influencer->firstname = $request->firstname;
                $influencer->lastname = $request->lastname;
                $influencer->bio = $request->bio; 

                if ($request->hasFile('image') && $request->file('image')->isValid()) {
                    if(Storage::exists($influencer->image_path)){
                        Storage::delete($influencer->image_path);
                    }
                    $image_path = Storage::putFile('influencers', $request->file('image'));
                    $influencer->photo_url = Storage::url($image_path);
                    $influencer->image_path = $image_path;
                }

                if ($influencer->save()) {
                    return response()->json(['message'=> 'Account settings updated successfully', 'redirect_url'=> route('app:influencer:dashboard:profile_settings')], 200);
                }
             }
           }

        return view('influencer.dashboard.profile_settings', []);
    } 

    public function change_password(Request $request){

        if($request->isMethod('post')){
            $influencer = Influencer::where('id', Auth::guard('influencers')->user()->id)->first();

            if ($request->isMethod('post')) {
                $validator = Validator::make($request->all(), [
                    'password' => ['max:255', 'min:4', function($attribute, $value, $fail)use($influencer) {
                        if (!Hash::check($value, $influencer->password)) {
                            return $fail('Password does not match your old password.');
                        }
                    }],
                    'confirm_password' => 'required|min:4|same:password',
                ]);
    
                if ($validator->fails()) {
                    return response()->json(['errors'=> $validator->errors()], 500);
                } else {
    
                    $influencer = Influencer::where('id', Auth::guard('influencers')->user()->id)->first();
                    $influencer->password = bcrypt($request->password);
                    if ($influencer->save()) {
                        return response()->json(['message'=> 'Password setting updated successfully', 'redirect_url'=> route('app:influencer:dashboard:change_password')], 200);
                    }
                }
    
            }
        }

        return view('influencer.dashboard.change_password', []);
    }
    public function bank_details(Request $request){
        if($request->isMethod('post')){
			$utils = new Utils();
			$influencer = Influencer::where('id', Auth::guard('influencers')->user()->id)->first();
			$validator = Validator::make($request->all(), [
				'bank' => 'required',
				'bank_name' => 'required',
				//'bank_bvn' => 'required|digits:11',
				'bank_account' => 'required|digits:10',
			]);
			
			if ($validator->fails()) {
				return response()->json(['errors'=> $validator->errors()], 500);
				
			}else{
				
            $paystack = new \Yabacon\Paystack(config('ecommerce_config.paystack_code'));
            
            try
			{
			  $tranx = $paystack->transferrecipient->create([
				'name' => $request->bank_name,
                 'description'=> 'Transfer Reciepent',
                'bank_code'=> $request->bank,
                'currency'=> 'NGN',
                'account_number'=> $request->bank_account
			  ]);
              
               if($tranx->status === true){
               $influencer->bank_sort = $request->bank;
				$influencer->bank_name = $request->bank_name;
				//$user->bank_bvn = $request->bank_bvn;
				$influencer->bank_account = $request->bank_account;
                $influencer->transfer_code = $tranx->data->recipient_code;
				if($influencer->save()){
					return response()->json(['message'=> 'Your Profile updated was successfull.', 'redirect_url'=> route('app:influencer:dashboard:bank_details')], 200);
				} 
               }
				
			}catch(\Yabacon\Paystack\Exception\ApiException $e){
				//die($e->getMessage());
                return response()->json(['errors'=> str_replace("'", " ", str_after($e->getMessage(), ':'))], 500);
			}
			
		}
      }
        return view('influencer.dashboard.bank_details', []);
    }


}
