<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyEmail;
use App\Influencer as Influencer;
use App\Repo\User\Auth as AuthGuard;

class InfluencerCtrl extends Controller
{
    public $user; 

    public function __construct(){
        $this->user = new AuthGuard('@INFLUENCER');
    } 

    public function landing_page(){
        //Auth::guard('influencers')->logout();
        $service_grids = DB::table('display_block')->where('block_code', 'ID:INFLUENCER:SERVICE')->get();
        $influencers = Influencer::with(['socials'=>function($q){
            $q->with('network')->where('is_default', true)->first();
        }])->where('is_approved', true)->limit(4)->get();
       
        return view('influencer.landing_page', ['service_grids'=>$service_grids, 'influencers'=> $influencers]);
    }
 
    public function apply(Request $request){
        if ($request->isMethod('post')) {
            $data = $request->all();
            $messages = [
                'tos.required' => 'You must agree to our terms and conditions',
            ]; 
            $validator = Validator::make($data, [
                'username' => 'required|max:255|min:4|unique:influencers',
                'password' => 'required|min:4',
                'confirm_password' => 'required|min:4|same:password',
                'email' => 'required|min:4|email|unique:influencers',
                'socials.instagram' => 'required|min:4|url',
                'socials.twitter' => 'required|min:4|url',
                'tos' => 'required|boolean',
            ], $messages);
            $niceNames = [
                'socials.instagram'=> 'Instagram',
                'socials.twitter'=> 'Twitter',
            ]; 
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();
                

            } else {

                $influencer = new Influencer();
                $data = $request->all();

                $influencer->username = $data['username'];
                $influencer->ref = str_random(10);
                $influencer->slug = str_slug($data['username'], '-');
                $influencer->email = $data['email'];
                $influencer->password = bcrypt($data['password']);
                $influencer->has_password = true;
                $influencer->email_token = str_random(36);
                $influencer->invite_code = 'SV' . str_random(4);
                $influencer->email_token_expiry = \Carbon\Carbon::now()->addDay(1);
                $influencer->temp_socials = json_encode($data['socials']);
                if ($influencer->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    Mail::to($influencer->email)->later($when, new VerifyEmail($influencer, 'INFLUENCER'));
                    return redirect()->route('app:influencer:apply_success', ['pid'=> encrypt($influencer->ref)]);
                }

            }

        }

        return view('influencer.apply', []);
    }

    public function apply_success(Request $request){
        if(!$request->pid){
            return abort(404, 'Missing success token!');
        }
        $influencer = Influencer::where('ref', decrypt($request->pid))->first();
        if(!$influencer){
            return abort(404, 'Missing success token!');
        }
        return view('influencer.apply_success', []);
    }

    public function sign_up(Request $request, $param){
        //$invite_code = 
        if ($request->isMethod('post')) {
            $role = DB::table('roles')->where('name', 'user')->first();

            $data = $request->all();
            $messages = [
                'invite_code.required' => 'You must Provide an invite code',
                'invite_code.exists' => 'Your invite code does not exists on our system',
            ];
 
            $validator = Validator::make($data, [
                'invite_code' => 'required|exists:influencer',
                'firstname' => 'required|max:255|min:4',
                'lastname' => 'required|max:255|min:4',
                'cellphone' => 'required|digit:11',
                'accepted_payment' => 'required|min:4',
                'minimum_amount' => 'required',
                'specialties' => 'required',
                'platform' => 'required',
                'bio' => 'required|min:4|max:300',
            ], $messages);

            if ($validator->fails()) {

                return back()
                    ->withErrors($validator)
                    ->withInput();

            } else {

                $influencer = Influencer::where('invite_code', $data['invite_code'])->first();
                $data = $request->all();
                $influencer->invite_code = null;
                $influencer->firstname = $data['firstname'];
                $influencer->lastname = $data['lastname'];
                $influencer->cellphone = $data['cellphone'];
                $influencer->accepted_payment = $data['accepted_payment'];
                $influencer->minimum_amount = $data['minimum_amount'];
                $influencer->platform = $data['platform'];
                $influencer->is_approved = $data['is_approved'];
                if ($influencer->save()) {
                    $when = \Carbon\Carbon::now()->addSeconds(2);
                    return redirect()->route('app:influencer:apply_success');
                }

            }

        }

        return view('influencer.sign_up', []);
    }

    public function login(Request $request){
       
        if ($request->isMethod('post')){
           $login = $this->user->login();
           $login->enter();
           if($login->status){
               return redirect()->route('app:influencer:landing_page')->with('success', $login->message);
           }else{
               return back()->withInput()->with('error', $login->message);
           }
        }
        return view('influencer.login');
    }

    public function logout(Request $request){
        if ($request->isMethod('post')){
            $this->user->logout('app:influencer:landing_page');
            return redirect()->route('app:influencer:landing_page')->with('success', 'You have logged out successfully!');
         }
    }


}
 