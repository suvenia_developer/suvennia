<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AssignGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $redirectTo = 'app:user:login')
    {
        if (!Auth::guard($guard)->check()) {
            return redirect()->route($redirectTo)->with('error', 'You do not have access to the requested page!.');
        }
        Auth::shouldUse($guard);
        return $next($request);
    }
}
 