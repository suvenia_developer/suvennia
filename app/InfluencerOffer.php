<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerOffer extends Model
{
    protected $table = "influencer_offers";
}
