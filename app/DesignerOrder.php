<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignerOrder extends Model
{

    public function seller()
    {
        return $this->belongsTo("App\Seller", "user_id");
    }

    public function plan()
    {
        return $this->belongsTo("App\DesignerPlan");
    }

    public function designer()
    {
        return $this->belongsTo("App\Designer");
    }

    public function gig()
    {
        return $this->belongsTo("App\DesignerGig");
    }

    public function order_request()
    {
        return $this->belongsTo("App\DesignerRequest", "request_id");
    }
}
