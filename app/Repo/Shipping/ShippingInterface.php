<?php
namespace App\Repo\Shipping;

interface ShippingInterface{

    /**
     * fetchs all ahipping address
     *
     * @param mixed $id
     * @return void
     */
    public function get_address($id);

    /**
     * add new address
     *
     * @param [type] $data
     * @param [type] $id
     * @return void
     */
    public function add_address($data, $zone_id);

    /**
     * edit shipping address
     *
     * @param [type] $id
     * @return void
     */
    public function edit_address($data, $zone_id, $address_id);

    /**
     * selects an address
     *
     * @param [type] $id
     * @return void
     */
    public function select_address($id);

    /**
     * selects an address
     *
     * @param [type] $id
     * @return void
     */
    public function select_zone($id);

    /**
     * delete an address
     *
     * @param [type] $id
     * @return void
     */
    public function delete_address($id);

    /**
     * fetch a shipping zone
     *
     * @param [type] $id
     * @return void
     */
    public function get_zone($id);
}