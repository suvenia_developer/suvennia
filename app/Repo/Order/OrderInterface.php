<?php
namespace App\Repo\Order;

interface OrderInterface{

    /**
     * saves inline order
     *
     * @param int $total_amount
     * @return App\User
     */
    public function quick_order($total_amount, $order_id);
}