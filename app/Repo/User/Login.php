<?php
namespace App\Repo\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User as User;

class Login
{

    public $request;

    protected $user_instance;

    protected $guard;

    public $status = false;

    public $message = '';

    protected $messages = [
        'INVALID_MAIL'=>'Your email has not been verified, please click on the re-verify email address link to verify your email address.',
        'INVALID_CREDENTIALS'=> 'Your Login credentials are incorrect!',
        'LOGIN_SUCCESS'=> 'You have logged in successfully!'
    ];

    public function __construct($user_instance, $guard){
        $this->request = request();
        $this->user_instance = $user_instance;
        $this->guard = $guard;
    }

    public function enter(){
        
            $user = $this->user_instance::where('email', $this->request->email)->first();
            if($user){
                $remember_me = $this->request->remember_me ? true : false;
                if (Hash::check($this->request->password, $user->password) and $user->email_verified == true) {

                    if (Hash::needsRehash($user->password)) {
                        $new_hashed = Hash::make($this->request->password);
                        $user->password = $new_hashed;
                        $user->save();
                    }
        
                    if (Auth::guard($this->guard)->attempt(['email' => $this->request->email, 'password' => $this->request->password], $remember_me)) {
                        $this->status = true;
                        $this->message = $this->messages['LOGIN_SUCCESS'];
                        return $this->status;
                       //return redirect()->route($this->redirectUrl)->with('success', 'You have logged in successfully!');
                        //return back()->with('success', 'You have logged in successfully!');
                    }
        
                }elseif(!$user->email_verified) {

                    $this->status = false;
                    $this->message = $this->messages['INVALID_MAIL'];
                    return $this->status;
                  

                } else {

                    $this->status = false;
                    $this->message = $this->messages['INVALID_CREDENTIALS'];
                    return $this->status;
                }
            }else{
                $this->status = false;
                $this->message = $this->messages['INVALID_CREDENTIALS'];
                return $this->status;
            }
    }

}
