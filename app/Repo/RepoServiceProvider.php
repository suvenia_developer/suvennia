<?php

namespace App\Repo;

use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(
            'App\Repo\Order\OrderInterface',
            'App\Repo\Order\OrderRepo'
        );

        $this->app->bind(
            'App\Repo\Cart\CartInterface',
            'App\Repo\Cart\CartRepo'
        );

        $this->app->bind(
            'App\Repo\Shipping\ShippingInterface',
            'App\Repo\Shipping\ShippingRepo'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
