<?php
namespace App\Repo\Cart;

interface CartInterface{

    /**
     * Adds a product to cart
     *
     * @param int $id
     * @return void
     * 
     */
    public function add_to_cart($id);

    /**
     * deletes a product from cart
     *
     * @param mixed $id
     * @return void
     */
    public function delete_from_cart($hash);

    /**
     * update cart item
     *
     * @param mixed $hash
     * @return void
     */
    public function update_cart($hash);

    /**
     * counts cart items
     *
     * @return void
     */
    public function count_cart_items();

}