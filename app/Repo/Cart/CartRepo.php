<?php
namespace App\Repo\Cart;

use App\Repo\Cart\CartInterface;
use LaraCart;
use App\Product;

class CartRepo implements CartInterface{ 

    /**
     * adds a product to cart
     *
     * @param int $id
     * @return void
     */
    public function add_to_cart($id){
        $__data = json_decode(base64_decode(request()->item), true);

            $product = Product::with(['photos' => function ($q) {
                $q->first();
            }, 'user', 'brandable', 'design'])->where('id', $id)->first();

            LaraCart::addLine(
                $product->id,
                $product->name,
                $__data['qty'],
                $product->brandable->price + $product->design->total_earning,
                [
                    "size" => $__data['size'],
                    "color" => $__data['color'],
                ],
                $taxable = false
            );

            $message = "$product->name added to Cart";

            return [    
                'message'=> $message,
                'product'=> $product
            ];
    }

    /**
     * deletes a product from cart
     *
     * @param mixed $id
     * @return void
     */
    public function delete_from_cart($hash){
        LaraCart::removeItem($hash);
        /*if(LaraCart::count() < 1){
            LaraCart::removeCoupons();
        }*/
    }

    /**
     * update cart item
     *
     * @param mixed $hash
     * @return void
     */
    public function update_cart($hash){
        if(request()->qty){
            LaraCart::updateItem($hash, 'qty', request()->qty);
        }
    }

    /**
     * counts cart items
     *
     * @return void
     */
    public function count_cart_items(){

    }
}