<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerSocial extends Model
{
    protected $table = 'influencer_social';

    public function influencer(){
        return $this->belongsTo("App\Influencer");
    }

    public function network(){
        return $this->hasOne("App\InfluencerNetwork");
    }
}
