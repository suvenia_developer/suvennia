<?php
namespace App;

use App\Events\ProductCategoryCreated;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    protected $table = 'eco_categories';

    protected $dispatchesEvents = [
        'saving' => \App\Events\ProductCategoryCreated::class,
    ];

    public function products(){
        return $this->belongsToMany("App\Product", 'eco_category_product', 'category_id', 'product_id');
    }

    public function children(){
        return $this->hasMany("App\ProductCategory", 'parent_id');
    }

    public function parent(){
        return $this->belongsTo("App\ProductCategory", 'parent_id');
    }
}
