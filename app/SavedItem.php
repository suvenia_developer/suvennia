<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedItem extends Model
{

    protected $table = 'eco_saved_items';

    public function user(){
        return $this->belongsTo("App\User");
    }
    public function product(){
        return $this->belongsTo("App\Product");
    }

}
