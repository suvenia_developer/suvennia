<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderShipping extends Model
{

    protected $table = 'eco_order_shipping';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function order(){
        return $this->belongsTo("App\Order");
    }
public function address(){
        return $this->hasOne("App\ShippingAddress");
    }
}
