<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'app_chat';

    public function sender(){
        $this->belongsTo('App\User', 'sender');
    }

    public function reciever(){
        $this->belongsTo('App\User', 'reciever');
    }
}
