<?php
namespace App\Helpers\Social;

class Twitter{

	static public function Init(){
		$provider = new \League\OAuth1\Client\Server\Twitter([
			'identifier'          => config('user_config.twitter.client_id'),
			'secret'      => config('user_config.twitter.client_secret'),
			'callback_uri'       => config('user_config.twitter.redirect')
		]);

		return $provider;
	}

	static public function driver($request){
		$server = self::Init();
		if(is_null($request->oauth_token) AND is_null($request->oauth_verifier)){
            $temporaryCredentials = $server->getTemporaryCredentials();
			$request->session()->put('TEMP_TWITTER_CRED', serialize($temporaryCredentials));
			$authorizationUrl = $server->getAuthorizationUrl($temporaryCredentials);
			return redirect($authorizationUrl);
		}
		
	}

	static public function authenticate($request){
		$server = self::Init();
		if(!is_null($request->oauth_token) AND !is_null($request->oauth_verifier)){
			try{
				
				$temporaryCredentials = unserialize($request->session()->pull('TEMP_TWITTER_CRED'));

				$tokenCredentials = $server->getTokenCredentials(
					$temporaryCredentials,
					$request->oauth_token,
					$request->oauth_verifier
				);

				$user = $server->getUserDetails($tokenCredentials);
				
				$userData = [
					'id'=> $user->uid,
					'email'=> $user->email,
					'username'=> str_slug($user->firstName . ' ' . $user->lastName),
					'firstname'=> $user->firstName,
					'lastname'=> $user->lastName,
					'profile_url'=> 'https://twitter.com/' . $user->nickname,
					'photo_url'=> $user->imageUrl,
				  ];
				  return json_decode(json_encode($userData));

			} catch(\League\OAuth1\Client\Exceptions\Exception $e){
				exit($e->getMessage());
			}
		}
	}


}