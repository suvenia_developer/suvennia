<?php

namespace App\Listeners\Shop;

use App\Events\Shop\ProductOrderCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SellerOrder;
use App\Seller;

class ConfirmSellerOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductOrderCompleted  $event
     * @return void
     */
    public function handle(ProductOrderCompleted $event)
    {
         $order = $event->order;
         $seller_order = SellerOrder::where('order_id', $order->id)->first();
         $seller_order->is_paid = 2;
         $seller_order->save();
         $seller = Seller::where('id', $seller_order->user_id)->increment('balance', $seller_order->user_profit);
    }
}
