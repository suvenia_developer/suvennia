<?php

namespace App\Listeners;

use App\Events\DesignerAttachmentDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DeleteImages
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DesignerAttachmentDeleted  $event
     * @return void
     */
    public function handle(DesignerAttachmentDeleted $event)
    {
        $file = $event->attachment->path;
        if(Storage::exists($file)){
            Storage::delete($file);
        }
    }
}
