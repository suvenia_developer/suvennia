<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $table = 'eco_order_items';

    public function user(){
        return $this->belongsTo("App\User");
    }
public function order(){
        return $this->belongsTo("App\Order");
    }
    public function product(){
        return $this->belongsTo("App\Product");
    }
}
