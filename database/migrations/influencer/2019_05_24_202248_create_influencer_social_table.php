<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencerSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_social', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('influencer_id');
            $table->integer('social_id');
            $table->longText('link');
            $table->boolean('is_default');
            $table->integer('followers')->nullable();
            $table->integer('post_count')->nullable();
            $table->integer('engagements')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_social');
    }
}
