<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref');
            $table->string('username')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('email_token')->nullable();
            $table->timestamp('email_token_expiry')->nullable();
            $table->boolean('has_password')->default(false);
            $table->boolean('email_verified')->default(false);
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->longText('photo_url')->nullable();
            $table->longText('cover_photo_url')->nullable();
            $table->string('phone')->nullable();
            $table->decimal('balance', 8, 2)->default(0);
            $table->decimal('earnings', 8, 2)->default(0);
            $table->string('bank_sort')->nullable();
            $table->string('bank_bvn')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_name')->nullable();
            $table->longText('bio')->nullable();
            $table->string('accepted_payment')->nullable();
            $table->string('minimum_amount')->nullable();
            $table->boolean('is_approved')->nullable();
            $table->string('invite_code')->nullable();
            $table->longText('temp_socials')->nullable();
            $table->timestamps(); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencers');
    }
}
