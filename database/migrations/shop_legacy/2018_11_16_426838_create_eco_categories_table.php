<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_categories", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("parent_id");
            $table->string("name");
            $table->string("slug")->nullable();
            $table->string("ref")->nullable();
            $table->boolean("is_parent")->default(false);
            $table->boolean("is_sub")->default(false);
            $table->longText("page_banner")->nullable();
            $table->longText("home_page_grid_banner")->nullable();
            $table->longText("home_page_grid_color")->nullable();
            $table->longText("show_case_product")->nullable();
            $table->timestamps();

            $table->foreign('parent_id')
            ->references('id')->on('eco_categories')
            ->onDelete('cascade');

        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_categories");
    }
}

