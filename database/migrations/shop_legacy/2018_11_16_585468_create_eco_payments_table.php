<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_payments", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("order_id");
            $table->string("reference");
            $table->string("gateway");
            $table->float("amount", 8, 2);
            $table->string("status")->default(false);
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');
            
        });
             
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_payments");
    }
}

