<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoShippingLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_shipping_locations", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string("name")->nullable();
            $table->bigInteger("zone_id");
            $table->longText("data");
            $table->timestamps();
            
            $table->foreign('zone_id')
            ->references('id')->on('eco_shipping_zones')
            ->onDelete('cascade');
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_shipping_locations");
    }
}

