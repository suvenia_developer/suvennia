<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eco_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('initiator_id')->nullable();
            $table->bigInteger('reciever_id')->nullable();
            $table->string('title')->nullable();
            $table->longText('message_body')->nullable();
            $table->boolean('is_read')->default(false);
            $table->timestamp('read_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eco_notifications');
    }
}
