<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoBuyerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_buyer_addresses", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("location_id");
            $table->bigInteger("zone_id");
            $table->string("price");
            $table->string("firstname");
            $table->string("lastname");
            $table->string("email");
            $table->longText("address");
            $table->string("phone_number");
            $table->boolean("is_default")->default(false);
            $table->string("country");
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('location_id')
            ->references('id')->on('eco_shipping_locations')
            ->onDelete('cascade');

            $table->foreign('zone_id')
            ->references('id')->on('eco_shipping_zones')
            ->onDelete('cascade');
             
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_buyer_addresses");
    }
}

