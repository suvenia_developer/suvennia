<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrderMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_order_metrics", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("product_id");
            $table->bigInteger("user_id");
            $table->bigInteger("order_id");
            $table->bigInteger("order_item_id");
            $table->bigInteger("payment_id");
            $table->string("user_profit");
            $table->string("site_profit");
            $table->string("expenses"); 
            $table->timestamps();
            
            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('order_item_id')
            ->references('id')->on('eco_order_items')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');
        }); 
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_order_metrics");
    }
}

