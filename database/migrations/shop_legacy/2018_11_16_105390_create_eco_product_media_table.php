<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoProductMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_product_media", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("product_id");
            $table->longText("path");
            $table->longText("public_url");
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
            
        }); 
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_product_media");
    }
}

