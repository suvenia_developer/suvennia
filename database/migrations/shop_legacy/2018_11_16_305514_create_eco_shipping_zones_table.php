<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoShippingZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_shipping_zones", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string("name")->nullable();
            $table->string("payment_method")->nullable();
            $table->boolean("is_default")->default(false);
            $table->boolean("has_option")->default(false);
            $table->longText("description")->nullable();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_shipping_zones");
    }
}

