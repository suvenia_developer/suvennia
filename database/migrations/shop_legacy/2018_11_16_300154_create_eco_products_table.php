<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_products", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("brandable_id")->nullable();
            $table->string("name")->nullable();
            $table->string("slug")->nullable();
            $table->string("ref")->nullable(); 
            $table->longText("description")->nullable();
            $table->bigInteger("price")->nullable();
            $table->bigInteger("sku")->nullable();
            $table->bigInteger("quantity")->nullable();
            $table->longText("temp_data_design")->nullable();
            $table->longText("temp_data_photos")->nullable();
            $table->boolean("is_archived")->default(false);
            $table->boolean("is_published")->default(true);
            $table->boolean("is_approved")->default(false);
            $table->timestamp("approval_date")->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('brandable_id')
            ->references('id')->on('eco_brandables')
            ->onDelete('cascade');

            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_products");
    }
}

