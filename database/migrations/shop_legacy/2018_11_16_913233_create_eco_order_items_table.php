<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_order_items", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("order_id");
            $table->bigInteger("product_id");
            $table->string("order_ref");
            $table->float('price', 8, 2);
            $table->bigInteger("quantity");
            $table->longText("data")->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
            
        }); 
             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_order_items");
    }
}

