<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoCategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_category_product", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("product_id");
            $table->bigInteger("category_id");
            $table->timestamps();

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('category_id')
            ->references('id')->on('eco_categories')
            ->onDelete('cascade');
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_category_product");
    }
}

