<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoSellerOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_seller_orders", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("order_id");
            $table->bigInteger("product_id");
            $table->float("user_profit", 8, 2);
            $table->boolean("is_paid")->default(false);
            $table->timestamps();
            
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');

        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_seller_orders");
    }
}

