<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create("eco_designs", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id");
            $table->bigInteger("product_id");
            $table->bigInteger("brandable_id");
            $table->string("name")->nullable();
            $table->string("earnings")->nullable();
            $table->string("target_quantity")->nullable();
            $table->string("total_earning")->nullable();
            $table->string("percentage")->nullable();
            $table->longText("raw_design")->nullable();
            $table->longText("colors")->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('brandable_id')
            ->references('id')->on('eco_brandables')
            ->onDelete('cascade');

        }); 
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_designs");
    }
}

