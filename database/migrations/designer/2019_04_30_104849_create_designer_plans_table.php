<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignerPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('designer_plans', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('gig_id');
            $table->bigInteger('user_id');
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->string('revisions')->nullable();
            $table->boolean('has_source_file')->default(false);
            $table->boolean('enables_commercial_use')->default(false);
            $table->string('design_concepts')->nullable();
            $table->float('price', 8, 2);
            $table->integer('duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designer_plans');
    }
}
