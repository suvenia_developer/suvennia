<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisplayBlockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('display_block', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('block_code');
            $table->longText('block_key')->nullable();
            $table->longText('block_value')->nullable();
            $table->longText('block_meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('display_block');
    }
}
