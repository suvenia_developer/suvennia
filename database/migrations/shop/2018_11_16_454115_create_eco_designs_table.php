<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create("eco_designs", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("product_id")->unsigned();
            $table->bigInteger("brandable_id")->unsigned();
            $table->string("name")->nullable();
            $table->string("earnings")->nullable();
            $table->string("target_quantity")->nullable();
            $table->string("total_earning")->nullable();
            $table->string("percentage")->nullable();
            $table->longText("raw_design")->nullable();
            $table->longText("colors")->nullable();
            $table->timestamps();

        }); 
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_designs");
    }
}

