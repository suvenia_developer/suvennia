<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoCategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_category_product", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("product_id")->unsigned();
            $table->bigInteger("category_id")->unsigned();
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_category_product");
    }
}

