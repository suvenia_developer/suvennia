<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrderMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_order_metrics", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("product_id")->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("order_id")->unsigned();
            $table->bigInteger("order_item_id")->unsigned();
            $table->bigInteger("payment_id")->unsigned();
            $table->string("user_profit");
            $table->string("site_profit");
            $table->string("expenses"); 
            $table->timestamps();
        }); 
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_order_metrics");
    }
}

