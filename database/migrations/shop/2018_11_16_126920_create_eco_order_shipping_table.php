<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoOrderShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_order_shipping", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("order_id")->unsigned();
            $table->bigInteger("zone_id")->unsigned();
            $table->bigInteger("address_id")->nullable()->unsigned();
            $table->string("price");
            $table->boolean("status");
            $table->timestamps();
            
        }); 
             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_order_shipping");
    }
}

