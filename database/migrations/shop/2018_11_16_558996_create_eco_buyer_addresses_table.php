<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoBuyerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_buyer_addresses", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("location_id")->unsigned();
            $table->bigInteger("zone_id")->unsigned();
            $table->string("price");
            $table->string("firstname");
            $table->string("lastname");
            $table->string("email");
            $table->longText("address");
            $table->string("phone_number");
            $table->boolean("is_default")->default(false);
            $table->string("country");
            $table->timestamps();
             
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_buyer_addresses");
    }
}

