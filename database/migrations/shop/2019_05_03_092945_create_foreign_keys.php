<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_product_media", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
            
        }); 

        Schema::create("eco_order_shipping", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('designer_orders')
            ->onDelete('cascade');
            
        }); 

        Schema::create("eco_order_metrics", function (Blueprint $table) {
            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('order_item_id')
            ->references('id')->on('eco_order_items')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');
        }); 

        Schema::create("eco_coupons", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_products", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('brandable_id')
            ->references('id')->on('eco_brandables')
            ->onDelete('cascade');

            
        });

        Schema::create("eco_categories", function (Blueprint $table) {
            $table->foreign('parent_id')
            ->references('id')->on('eco_categories')
            ->onDelete('cascade');

        });

        Schema::create("eco_designs", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('brandable_id')
            ->references('id')->on('eco_brandables')
            ->onDelete('cascade');

        }); 

        Schema::create("eco_buyer_addresses", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('location_id')
            ->references('id')->on('eco_shipping_locations')
            ->onDelete('cascade');

            $table->foreign('zone_id')
            ->references('id')->on('eco_shipping_zones')
            ->onDelete('cascade');
             
        });

        Schema::create("eco_payments", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_orders", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_faqs", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
        });

        Schema::create("eco_shipping_locations", function (Blueprint $table) {
            
            $table->foreign('zone_id')
            ->references('id')->on('eco_shipping_zones')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_customers", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_seller_orders", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');

        });

        Schema::create("eco_order_items", function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('order_id')
            ->references('id')->on('eco_orders')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
            
        });

        Schema::create("eco_category_product", function (Blueprint $table) {
            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

            $table->foreign('category_id')
            ->references('id')->on('eco_categories')
            ->onDelete('cascade');
            
        });

        Schema::create('eco_saved_items', function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');
        });

        Schema::create('eco_analytics', function (Blueprint $table) {
            $table->foreign('client_id')
            ->references('id')->on('users');

        });

        Schema::create('eco_reviews', function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')->on('eco_products')
            ->onDelete('cascade');

        });

        Schema::create('eco_withdrawals', function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });

        Schema::create('eco_user_coupon', function (Blueprint $table) {
            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');

            $table->foreign('coupon_id')
            ->references('id')->on('eco_coupons')
            ->onDelete('cascade');


        });

        Schema::create('eco_notifications', function (Blueprint $table) {
            $table->foreign('initiator_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
            
            $table->foreign('reciever_id')
            ->references('id')->on('users')
            ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create("eco_product_media", function (Blueprint $table) {
            $table->dropForeign('eco_product_media_user_id');
            $table->dropForeign('eco_product_media_product_id');
            
        }); 

        Schema::create("eco_order_shipping", function (Blueprint $table) {
            $table->dropForeign('eco_order_shipping_user_id');
            $table->dropForeign('eco_order_shipping_order_id');
        }); 

        Schema::create("eco_order_metrics", function (Blueprint $table) {
            $table->dropForeign('eco_order_metrics_product_id');

            $table->dropForeign('eco_order_metrics_order_item_id');

            $table->dropForeign('eco_order_metrics_user_id');

            $table->dropForeign('eco_order_metrics_order_id');
        }); 

        Schema::create("eco_coupons", function (Blueprint $table) {
            $table->dropForeign('eco_coupons_user_id');

            $table->dropForeign('eco_coupons_product_id');
            
        });

        Schema::create("eco_products", function (Blueprint $table) {
            $table->dropForeign('eco_products_user_id');

            $table->dropForeign('eco_products_brandable_id');

        });

        Schema::create("eco_categories", function (Blueprint $table) {
            $table->dropForeign('eco_categories_parent_id');

        });

        Schema::create("eco_designs", function (Blueprint $table) {
            $table->dropForeign('eco_designs_user_id');

            $table->dropForeign('eco_designs_product_id');

            $table->dropForeign('eco_designs_brandable_id');

        }); 

        Schema::create("eco_buyer_addresses", function (Blueprint $table) {
            $table->dropForeign('eco_buyer_addresses_user_id');
            $table->dropForeign('eco_buyer_addresses_location_id');

            $table->dropForeign('eco_buyer_addresses_zone_id');
             
        });

        Schema::create("eco_payments", function (Blueprint $table) {
            $table->dropForeign('eco_payments_user_id');
            $table->dropForeign('eco_payments_order_id');
            
        });

        Schema::create("eco_orders", function (Blueprint $table) {
            $table->dropForeign('eco_orders_user_id');
        });

        Schema::create("eco_faqs", function (Blueprint $table) {
            $table->dropForeign('eco_faqs_user_id');

            $table->dropForeign('eco_faqs_product_id');
        });

        Schema::create("eco_shipping_locations", function (Blueprint $table) {
            
            $table->dropForeign('eco_shipping_locations_zone_id');
            
        });

        Schema::create("eco_customers", function (Blueprint $table) {
            $table->dropForeign('eco_customers_user_id');
            
        });

        Schema::create("eco_seller_orders", function (Blueprint $table) {
            $table->dropForeign('eco_seller_orders_user_id');
            $table->dropForeign('eco_seller_orders_product_id');
            $table->dropForeign('eco_seller_orders_order_id');
        });

        Schema::create("eco_order_items", function (Blueprint $table) {
            $table->dropForeign('eco_order_items_user_id');
            
            $table->dropForeign('eco_order_items_order_id');
            
            $table->dropForeign('eco_order_items_product_id');
           
        });

        Schema::create("eco_category_product", function (Blueprint $table) {
            $table->dropForeign('eco_category_product_product_id');
            $table->dropForeign('eco_category_product_category_id');
        });

        Schema::create('eco_saved_items', function (Blueprint $table) {
            $table->dropForeign('eco_saved_items_user_id');

            $table->dropForeign('eco_saved_items_product_id');
        });

        Schema::create('eco_analytics', function (Blueprint $table) {
            $table->dropForeign('eco_analytics_client_id');

        });

        Schema::create('eco_reviews', function (Blueprint $table) {
            $table->dropForeign('eco_reviews_user_id');
            $table->dropForeign('eco_reviews_product_id');

        });

        Schema::create('eco_withdrawals', function (Blueprint $table) {
            $table->dropForeign('eco_withdrawals_user_id');
        });

        Schema::create('eco_user_coupon', function (Blueprint $table) {
            $table->dropForeign('eco_user_coupon_user_id');
            $table->dropForeign('eco_user_coupon_coupon_id');
  
        });

        Schema::create('eco_notifications', function (Blueprint $table) {
            $table->dropForeign('eco_notifications_initiator_id');
            $table->dropForeign('eco_notifications_reciever_id');
        });


    }
}
