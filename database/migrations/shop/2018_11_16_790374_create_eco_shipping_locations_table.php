<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoShippingLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_shipping_locations", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string("name")->nullable();
            $table->bigInteger("zone_id")->unsigned();
            $table->longText("data");
            $table->timestamps();
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_shipping_locations");
    }
}

