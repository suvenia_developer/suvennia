<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcoCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("eco_coupons", function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger("user_id")->unsigned();
            $table->bigInteger("product_id")->unsigned();
            $table->string("name");
            $table->string("percentage");
            $table->bigInteger("amount");
            $table->boolean("is_closed")->default(false);
            $table->timestamp("starts_on")->nullable();
            $table->timestamp("expires_on")->nullable();
            $table->timestamps();
            
        });
            
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("eco_coupons");
    }
}

