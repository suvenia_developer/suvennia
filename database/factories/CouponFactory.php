<?php

use Faker\Generator as Faker;

$factory->define(App\Coupon::class, function (Faker $faker) {
    return [
        'user_id'=> $faker->numberBetween($min = 1, $max = 10),
        'product_id'=> $faker->numberBetween($min = 1, $max = 100),
        'name'=> $faker->name,
        'percentage'=> $faker->numberBetween($min = 1, $max = 100),
        'amount'=> $faker->numberBetween($min = 100, $max = 1000),
        'is_closed'=> $faker->dateTime(),
        'starts_on'=> \Carbon\Carbon::now(),
        'expires_on'=> \Carbon\Carbon::now()->addDays(2)
    ];
});
 