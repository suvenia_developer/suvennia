<?php
 
use Faker\Generator as Faker;

$factory->define(App\Analytic::class, function (Faker $faker) {
    return [
         'client_id'=> $faker->ipv4,
         'model_id'=> $faker->numberBetween($min = 1, $max = 200),
         'category'=> $faker->randomElement($array = array ('PRODUCT','STORE')),
         'device'=> $faker->userAgent,
         'platform'=> $faker->userAgent,
         'browser'=> $faker->userAgent,
         'is_mobile'=> $faker->boolean,
         'lang'=> $faker->languageCode,
         'country'=> $faker->countryCode,
         'path'=> $faker->url,
         'page_name'=> $faker->name
    ];
});
 