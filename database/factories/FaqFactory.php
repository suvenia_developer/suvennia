<?php

use Faker\Generator as Faker;

$factory->define(App\Faq::class, function (Faker $faker) {
    return [
        'user_id'=> $faker->numberBetween($min = 1, $max = 10),
        'product_id'=> $faker->numberBetween($min = 1, $max = 100),
        'question'=> paragraph($nbSentences = 3, $variableNbSentences = true),
        'response'=> paragraph($nbSentences = 3, $variableNbSentences = true)
    ]; 
});
