<?php
Route::group(['prefix' => 'influencer', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/', 'InfluencerCtrl@landing_page')->name('app:influencer:landing_page');
    Route::match(['post', 'get'], '/sign-up', 'InfluencerCtrl@sign_up')->name('app:influencer:sign_up');
    Route::match(['post', 'get'], '/login', 'InfluencerCtrl@login')->name('app:influencer:login');
    Route::match(['post', 'get'], '/logout', 'InfluencerCtrl@logout')->name('app:influencer:logout');
});

Route::group(['prefix' => 'influencer/app', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/apply', 'InfluencerCtrl@apply')->name('app:influencer:apply');
    Route::match(['post', 'get'], '/apply-success', 'InfluencerCtrl@apply_success')->name('app:influencer:apply_success');
});  

Route::group(['prefix' => 'dashboard/influencer', 'middleware' => ['web', 'assign.guard:influencers,app:influencer:login']], function () {
    Route::match(['post', 'get'], '/overview', 'Dashboard\InfluencerCtrl@overview')->name('app:influencer:dashboard:overview');
    Route::match(['post', 'get'], '/jobs', 'Dashboard\InfluencerCtrl@gigs')->name('app:influencer:dashboard:gigs');
    Route::match(['post', 'get'], '/jobs/accept/{id}', 'Dashboard\InfluencerCtrl@accept_gig')->name('app:influencer:dashboard:accept_gig');
    Route::match(['post', 'get'], '/job/modal/{id}', 'Dashboard\InfluencerCtrl@gig_modal')->name('app:influencer:dashboard:gig_modal');
    Route::match(['post', 'get'], '/my-posts', 'Dashboard\InfluencerCtrl@my_posts')->name('app:influencer:dashboard:my_posts');
    Route::match(['post', 'get'], '/my-orders', 'Dashboard\InfluencerCtrl@my_orders')->name('app:influencer:dashboard:my_orders');
    Route::match(['post', 'get'], '/withdraw', 'Dashboard\InfluencerCtrl@withdraw')->name('app:influencer:dashboard:withdraw');
    Route::match(['post', 'get'], '/profile-settings', 'Dashboard\InfluencerCtrl@profile_settings')->name('app:influencer:dashboard:profile_settings');
    Route::match(['post', 'get'], '/change-password', 'Dashboard\InfluencerCtrl@change_password')->name('app:influencer:dashboard:change_password');
    Route::match(['post', 'get'], '/bank-details', 'Dashboard\InfluencerCtrl@bank_details')->name('app:influencer:dashboard:bank_details');
});  