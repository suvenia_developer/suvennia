<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;
use App\Helpers\Utils;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('app_reset', function () {
    DB::table('eco_products')->truncate();
    DB::table('eco_product_media')->truncate();
    DB::table('eco_seller_orders')->truncate();
    DB::table('eco_saved_items')->truncate();
    DB::table('eco_reviews')->truncate();
    DB::table('eco_order_shipping')->truncate();
    DB::table('eco_order_metrics')->truncate();
    DB::table('eco_orders')->truncate();
    DB::table('eco_order_items')->truncate();
    DB::table('eco_faqs')->truncate();
    DB::table('eco_designs')->truncate();
    DB::table('eco_customers')->truncate();
    DB::table('eco_coupons')->truncate();
    DB::table('eco_user_coupon')->truncate();
    DB::table('eco_category_product')->truncate();
    DB::table('eco_buyer_addresses')->truncate();
    DB::table('eco_payments')->truncate();
    App\Seller::whereNotNull('balance')->update(['balance'=> 0]);

    $util = new Utils();
    $util->deleteAllFiles(public_path('products'));
})->describe('Truncates table');
