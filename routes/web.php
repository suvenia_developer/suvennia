<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
 */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::match(['post', 'get'], '/eco-brandables/add', 'BrandableCtrl@add')->name('admin:brandable:add');
    Route::match(['post', 'get'], '/eco-brandables/{id}/edit', 'BrandableCtrl@edit_brandable')->name('admin:brandable:edit');
    Route::match(['post', 'get'], '/eco-brandables/{id}/delete', 'BrandableCtrl@delete_brandable')->name('admin:brandable:delete');

    Route::match(['post', 'get'], '/templates/add', 'TemplateCtrl@add')->name('admin:template:add');
    Route::match(['post', 'get'], '/templates/{id}/edit', 'TemplateCtrl@edit')->name('admin:template:edit');
    Route::match(['post', 'get'], '/templates/{id}/delete', 'TemplateCtrl@delete')->name('admin:template:delete');

    Voyager::routes();
}); 

include_once "guards/influencers.php";
include_once "guards/sellers.php";
include_once "guards/designers.php";

include_once "customizer.php";
include_once "shop.php";
include_once "users.php"; 

Route::group(['prefix' => 'app/chat', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/send/chat/{id}/{sender}', 'ChatCtrl@new_chat')->name('app:chat:new_chat');
    Route::match(['post', 'get'], '/all/{id}/{sender}', 'ChatCtrl@all_chats')->name('app:chat:all_chats');
    Route::match(['post', 'get'], '/update/chat/{id}/{sender}', 'ChatCtrl@update_chat')->name('app:chat:update_chat');
    Route::match(['post', 'get'], '/upload/chat-file/{id}/{sender}', 'ChatCtrl@upload_chat_file')->name('app:chat:upload_chat_file');
});

Route::group(['prefix' => '', 'middleware' => ['web']], function () {
    Route::match(['post', 'get'], '/product/search', 'ShopCtrl@product_search')->name('app:ecommerce:product_search');
    Route::match(['post', 'get'], '/store/{slug}', 'DashboardCtrl@store_view')->name('app:base:store_view');
    Route::match(['post', 'get'], '/{category}/{param}', 'ShopCtrl@single_product')->name('app:ecommerce:single_product');
    Route::match(['post', 'get'], '/{category}', 'ShopCtrl@product_catlogue')->name('app:ecommerce:product_catlogue');
    
    Route::match(['post', 'get'], '/', 'BaseCtrl@index')->name('app:base:index');
});

 