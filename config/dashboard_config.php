<?php
return [

    "fonts"=> [
        "Roboto" => [
            "css"=> "'Roboto', sans-serif",  
            "url" => "https://fonts.googleapis.com/css?family=Roboto",
        ],
    
        "Fahkwang" => [
                "css"=> "'Fahkwang', sans-serif",
                "url" => "https://fonts.googleapis.com/css?family=Fahkwang",
           ],
    
        "ZCOOL KuaiLe" => [
                "css"=> "'ZCOOL KuaiLe', cursive",
                "url" => "https://fonts.googleapis.com/css?family=ZCOOL+KuaiLe",
           ],
    
        "Staatliches" => [
                "css"=> "'Staatliches', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Staatliches",
           ],
    
        "Indie Flower" => [
                "css"=> "'Indie Flower', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Indie+Flower",
           ],
    
        "Anton" => [
                "css"=> "'Anton', sans-serif",
                "url" => "https://fonts.googleapis.com/css?family=Anton",
           ],
    
        "Lobster" => [
                "css"=> "'Lobster', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Lobster",
           ],
    
        "Pacifico" => [
                "css"=> "'Pacifico', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Pacifico",
           ],
    
        "Abril Fatface" => [
                "css"=> "'Abril Fatface', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Abril+Fatface",
           ],
    
        "Shadows Into Light" => [
                "css"=> "'Shadows Into Light', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Shadows+Into+Light",
           ],
    
        "Acme" => [
                "css"=> "'Acme', sans-serif",
                "url" => "https://fonts.googleapis.com/css?family=Acme",
           ],
    
        "Bree Serif" => [
                "css"=> "'Bree Serif', serif",
                "url" => "https://fonts.googleapis.com/css?family=Bree+Serif",
           ],
    
        "Righteous" => [
                "css"=> "'Righteous', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Righteous",
           ],
    
        "Permanent Marker" => [
                "css"=> "'Permanent Marker', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Permanent+Marker",
           ],
    
        "Kaushan Script" => [
                "css"=> "'Kaushan Script', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Kaushan+Script",
           ],
    
        "Great Vibes" => [
                "css"=> "'Great Vibes', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Great+Vibes",
           ],
    
        "Fredoka One" => [
                "css"=> "'Fredoka One', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Fredoka+One",
           ],
    
        "Luckiest Guy" => [
                "css"=> "'Luckiest Guy', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Luckiest+Guy",
           ],
    
        "Orbitron" => [
                "css"=> "'Orbitron', sans-serif",
                "url" => "https://fonts.googleapis.com/css?family=Orbitron",
           ],
    
        "Lobster Two" => [
                "css"=> "'Lobster Two', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Lobster+Two",
           ],
    
        "Sacramento" => [
                "css"=> "'Sacramento', cursive",
                "url" => "https://fonts.googleapis.com/css?family=Sacramento",
           ],
    
    ]

];