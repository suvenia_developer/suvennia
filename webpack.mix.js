const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/vendor.js', 'public/js')
mix.js('resources/assets/js/main.js', 'public/js')
mix.js('resources/assets/js/customizer.js', 'public/js')
mix.js('resources/assets/js/dashboard_vendor.js', 'public/js')
mix.js('resources/assets/js/dashboard_main.js', 'public/js')
mix.js('resources/assets/js/brandable.js', 'public/js')
mix.js('resources/assets/js/admin_templates.js', 'public/js')
mix.js('resources/assets/js/_store.js', 'public/js')
   .sass('resources/assets/sass/vendor.scss', 'public/css')
   .sass('resources/assets/sass/main.scss', 'public/css')
   .sass('resources/assets/sass/customizer.scss', 'public/css')
   .sass('resources/assets/sass/dashboard_vendor.scss', 'public/css')
   .sass('resources/assets/sass/dashboard_main.scss', 'public/css')
   .sass('resources/assets/sass/brandable.scss', 'public/css')
   .sass('resources/assets/sass/admin_templates.scss', 'public/css')
   .sass('resources/assets/sass/_store.scss', 'public/css')
   .webpackConfig({
      module: {
        rules: [ 
          {
            test: /\.tsx?$/,
            loader: "ts-loader",
            exclude: /node_modules/
          },
          {
            test: require.resolve('jquery'),
            use: [
                {
                    loader: 'expose-loader',
                    options: '$'
                },
                {
                    loader: 'expose-loader',
                    options: 'jQuery'
                }
            ]
        },
        {
          test: require.resolve('@ckeditor/ckeditor5-build-classic'),
          use: [
              {
                  loader: 'expose-loader',
                  options: 'ClassicEditor'
              }
          ]
        },
        {
          test: require.resolve('uikit'),
          use: [
              {
                  loader: 'expose-loader',
                  options: 'UIkit'
              }
          ]
        },
        {
            test: require.resolve('toastr'),
            use: [
                {
                    loader: 'expose-loader',
                    options: 'toastr'
                }
            ]
        },
        {
          test: /\.modernizrrc.js$/,
          use: [ 'modernizr-loader' ]
        },
        {
          test: /\.modernizrrc(\.json)?$/,
          use: [ 'modernizr-loader', 'json-loader' ]
        },

        ]
      },
      resolve: {
         extensions: [".ts", ".tsx", ".js"],
         alias: {
          modernizr$: path.resolve(__dirname, ".modernizrrc")
        }
       }
    });

    if (mix.inProduction()) {
      mix.version();
  }